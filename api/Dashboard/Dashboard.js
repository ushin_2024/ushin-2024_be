const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.get("/getDashboardDetails", async (req, res) => {
    let result = await models.TargetUsage.findAll({
        include: [
            {
                model: models.ActualUsage,
                required: true,
                raw: true,
                include: [
                    {
                        model: models.MasterUsageStatus,
                        required: true,
                        raw: true,
                    },
                ],
            },
            {
                model: models.DO,
                required: true,
                raw: true,
                include: [
                    {
                        model: models.MO,
                        required: true,
                        raw: true,
                    },
                ],
            },
        ],
        raw: true,
    });
    // console.log(result)
    let uniqueList = [];
    let uniqueTable = [];

    for (let i = 0; i < result.length; i++) {
        let getPo = await models.PO.findAll({
            where: { DOId: result[i]["DOId"] },
            raw: true,
        });
        for (let j = 0; j < getPo.length; j++) {
            if (!uniqueList.includes(getPo[j]["POId"])) {
                uniqueList.push(getPo[j]["POId"]);

                uniqueTable.push(Object.assign(getPo[j], result[i]));
            }
        }

    }

    for (let i = 0; i < uniqueTable.length; i++) {

        let getProductionData = await models.ProductionData.findOne({
            where: { POId: uniqueTable[i]["POId"] },
            order: [["createdAt", "DESC"]],
            raw: true,
        });
        uniqueTable[i]["ProductionData.ProductionDataId"] =
            getProductionData["ProductionDataId"];
        uniqueTable[i]["ProductionData.POId"] = getProductionData["POId"];
        uniqueTable[i]["ProductionData.ChildPartListId"] =
            getProductionData["ChildPartListId"];
        uniqueTable[i]["ProductionData.EmployeeId"] =
            getProductionData["EmployeeId"];
        uniqueTable[i]["ProductionData.createdAt"] = getProductionData["createdAt"];
        uniqueTable[i]["ProductionData.updatedAt"] = getProductionData["updatedAt"];
        uniqueTable[i]["ProductionData.MachineId"] = getProductionData["MachineId"];

        let getMachine = await models.Machine.findOne({
            where: { MachineId: getProductionData["MachineId"] },
            raw: true,
        });
        uniqueTable[i]["Machine.MachineId"] = getMachine["MachineId"];
        uniqueTable[i]["Machine.MachineName"] = getMachine["MachineName"];
        uniqueTable[i]["Machine.MachineType"] = getMachine["MachineType"];
        uniqueTable[i]["Machine.MachineNo"] = getMachine["MachineNo"];
        uniqueTable[i]["Machine.createdAt"] = getMachine["createdAt"];
        uniqueTable[i]["Machine.updatedAt"] = getMachine["updatedAt"];
        // console.log(getMachine)
    }

    console.log(uniqueTable);
    // console.log(uniqueTable)
    res.send(result);
});

// router.get("/getProductionResult", async (req, res) => {
//     let data = await models.TargetUsage.findAll({
//         include: [
//             {
//                 model: models.ActualUsage,
//                 required: true,
//                 raw: true,
//             },
//             {
//                 model: models.DO,
//                 required: true,
//                 raw: true,
//                 where: { DOStatusId: 1 },
//                 include: [
//                     {
//                         model: models.MO,
//                         required: true,
//                         raw: true,
//                     },
//                 ],
//             },
//             {
//                 model: models.PartListUsage,
//                 required: true,
//                 raw: true,
//                 include: [
//                     {
//                         model: models.ChildPartList,
//                         required: true,
//                         raw: true,
//                         include: [
//                             {
//                                 model: models.ProductionData,
//                                 required: true,
//                                 raw: true,
//                                 include: [
//                                     {
//                                         model: models.Machine,
//                                         required: true,
//                                         raw: true,
//                                     },
//                                 ],
//                             },
//                         ],
//                     },
//                 ],
//             },
//         ],
//         raw: true,
//     });

//     data.sort(function (a, b) {
//         return a[
//             "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
//         ].localeCompare(
//             b["PartListUsage.ChildPartList.ProductionData.Machine.MachineName"]
//         );
//     });
//     let previousMachine;
//     let machineDetail = [];

//     let result = [];
//     for (let i = 0; i < data.length; i++) {
//         if (
//             previousMachine !=
//             data[i]["PartListUsage.ChildPartList.ProductionData.Machine.MachineName"]
//         ) {
//             previousMachine =
//                 data[i][
//                 "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
//                 ];
//             machineDetail.push({
//                 MachineName:
//                     data[i][
//                     "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
//                     ],
//                 MONo: data[i]["DO.MO.MONo"],
//             });
//         }

//         // console.log(data[i]["PartListUsage.ChildPartList.ProductionData.Machine.MachineName"])
//     }
//     let sum = 0;

//     for (let i = 0; i < machineDetail.length; i++) {
//         for (let j = 0; j < data.length; j++) {
//             if (
//                 machineDetail[i]["MachineName"] ===
//                 data[j][
//                 "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
//                 ]
//             ) {
//                 sum += data[j]["ActualUsage.ActualUsage"];
//             }
//         }
//         machineDetail[i]["Sum"] = sum;
//         // result.push({MONo: data[]})
//         // console.log(sum)
//         sum = 0;
//     }

//     // console.log(machineDetail)

//     res.send(machineDetail);
// });

router.get("/getProductionResultNew", async (req, res) => {
    let findAllPOStatus = await models.POStatus.findAll({ raw: true, include: [{ model: models.MasterPOStatus, required: true, raw: true }] })
    let arr = []

    for (let i = 0; i < findAllPOStatus.length; i++) {

        if (String(findAllPOStatus[i]['MasterPOStatus.POStatus']).includes("_open")) {
            let findLatestOpenedPO = await models.POStatus.findOne({
                where: [{ POId: findAllPOStatus[i]['POId'] }, { MasterPOStatusId: findAllPOStatus[i]['MasterPOStatusId'] }],
                order: [['createdAt', 'DESC']],
                raw: true,
                include: [{
                    model: models.MasterPOStatus,
                    required: true,
                    raw: true
                }]
            })

            let findMasterPOStatusClose = await models.MasterPOStatus.findOne({
                where: { POStatus: String(findLatestOpenedPO['MasterPOStatus.POStatus']).replace(/_\w+/g, "") + "_close" },
                raw: true
            })


            // console.log(findLatestOpenedPO)
            // console.log(findMasterPOStatusClose)
            let findLatestClosedPO = await models.POStatus.findOne({
                where: [{ MasterPOStatusId: findMasterPOStatusClose["MasterPOStatusId"] }, { POId: findLatestOpenedPO["POId"] }],
                order: [['createdAt', 'DESC']],
                raw: true,
                include: [{ model: models.MasterPOStatus, required: true, raw: true }]
            })
            if (findLatestClosedPO !== null) {
                let findPO = await models.PO.findOne({
                    where: { POId: findLatestClosedPO["POId"] },
                    raw: true
                })
                let findDO = await models.DO.findOne({
                    where: { DOId: findPO["DOId"] },
                    raw: true
                })

                let findMO = await models.MO.findOne({
                    where: { MOId: findDO["MOId"] },
                    raw: true
                })

                // console.log(findMO)

                let getDOId = await models.PO.findOne({
                    where: { POId: findLatestClosedPO["POId"] },
                    raw: true
                })
                let getDOBalance = await models.DO.findOne({
                    where: { DOId: getDOId["DOId"] },
                    raw: true
                })
                // console.log(getDOBalance["Balance"])
                // total += getDOBalance["Balance"]
                // console.log(findLatestClosedPO)
                arr.push({ MONo: findMO["MONo"], MasterPOStatusId: findLatestClosedPO["MasterPOStatusId"], POStatus: findLatestClosedPO['MasterPOStatus.POStatus'], Balance: getDOBalance["Balance"] })

            }

        }
    }

    arr.sort(function (a, b) {
        return a.MasterPOStatusId - b.MasterPOStatusId
    })

    let prevMasterPOId = -1
    let total = 0
    let sum = []
    for (let i = 0; i < arr.length; i++) {

        if (prevMasterPOId !== arr[i]["MasterPOStatusId"]) {
            if (i === 0) {
                total += arr[i]["Balance"]
            }
            sum.push({ MONo: arr[i]["MONo"], POStatus: String(arr[i]["POStatus"]).replace(/_\w+/g, ""), totalSum: total })
            total = 0
            prevMasterPOId = arr[i]["MasterPOStatusId"]
            total += arr[i]["Balance"]

        } else {
            total += arr[i]["Balance"]
        }

    }

    console.log(sum)

    res.send(sum)

})

router.get("/getProductionResultRefined", async (req, res) => {
    let findTotalCards = await models.Card_Log.findAll({
        raw: true,
        order: [['createdAt', 'DESC']],
    })

    let cardUniqueList = []
    let cardUniqueListArr = []//filtered array
    for (let i = 0; i < findTotalCards.length; i++) {//filter latest card
        if (!cardUniqueList.includes(findTotalCards[i]["DOId"] + "," + findTotalCards[i]["CardNo"])) {
            cardUniqueList.push(findTotalCards[i]["DOId"] + "," + findTotalCards[i]["CardNo"])
            cardUniqueListArr.push(findTotalCards[i])
        }
    }

    cardUniqueListArr.sort(function (a, b) {
        return a.DOId - b.DOId
    })
    console.log(cardUniqueListArr)

    for (let i = 0; i < cardUniqueListArr.length; i++) {
        let getPO = await models.PO.findOne({
            where: [{ DOId: cardUniqueListArr[i]["DOId"] }, { CardNo: cardUniqueListArr[i]["CardNo"] }],
            raw: true
        })
        cardUniqueListArr[i]["POId"] = getPO["POId"]
        cardUniqueListArr[i]["PONo"] = getPO["PONo"]

        let getLatestPOStatus = await models.POStatus.findOne({
            where: { POId: cardUniqueListArr[i]["POId"] },
            raw: true,
            order: [['createdAt', 'DESC']],
            include: [{
                model: models.MasterPOStatus,
                required: true
            }]
        })
        if (getLatestPOStatus !== null) {
            cardUniqueListArr[i]['MasterPOStatus.MasterPOStatusId'] = getLatestPOStatus['MasterPOStatus.MasterPOStatusId']
            cardUniqueListArr[i]['MasterPOStatus.POStatus'] = getLatestPOStatus['MasterPOStatus.POStatus']
            cardUniqueListArr[i]['MasterPOStatus.createdAt'] = getLatestPOStatus['MasterPOStatus.createdAt']
            cardUniqueListArr[i]['MasterPOStatus.updatedAt'] = getLatestPOStatus['MasterPOStatus.updatedAt']
        }

    }
    // console.log(cardUniqueListArr)
    let cleanedData = []
    for (let i = 0; i < cardUniqueListArr.length; i++) {
        if (cardUniqueListArr[i]['MasterPOStatus.MasterPOStatusId'] !== undefined) {
            cleanedData.push(cardUniqueListArr[i])
        }

    }

    for (let i = 0; i < cleanedData.length; i++) {
        let getDO = await models.DO.findOne({
            where: { DOId: cleanedData[i]["DOId"] },
            raw: true
        })
        cleanedData[i]["DO.Balance"] = getDO["Balance"]
    }


    let list = []
    let count = 0

    let prevCleanData = ""

    for (let i = 0; i < cleanedData.length; i++) {

        if (prevCleanData['MasterPOStatus.POStatus'] !== cleanedData[i]['MasterPOStatus.POStatus']) {

            if (i === 0)
                count += cleanedData[i]['DO.Balance']
            else {

                list.push({ POStatus: cleanedData[i]['MasterPOStatus.POStatus'], totalSum: count })
                count = 0
                count += cleanedData[i]['DO.Balance']
            }

            prevCleanData = cleanedData[i]

        } else {
            count += cleanedData[i]['DO.Balance']
        }
        if (i === cleanedData.length - 1) {
            list.push({ Process: cleanedData[i]['MasterPOStatus.POStatus'], totalSum: count })
        }
    }

    console.log(list)
    // console.log(findTotalCards)
    res.send(cardUniqueListArr)
})

router.get("/getMaterialInlineStock", async (req, res) => {
    let getAllQtyLeft = await models.QtyLeft.findAll({
        raw: true,
    })
    let latestCheck = []
    let latest = []
    for (let i = 0; i < getAllQtyLeft.length; i++) {
        let findLatestQtyLeft = await models.QtyLeft.findOne({
            where: [{ ChildPartListId: getAllQtyLeft[i]["ChildPartListId"] }],
            order: [['createdAt', 'DESC']],
            raw: true,
        })

        if (!latestCheck.includes(findLatestQtyLeft["ChildPartListId"])) {
            latestCheck.push(findLatestQtyLeft["ChildPartListId"])
            latest.push(findLatestQtyLeft)
        }
        // console.log(findLatestQtyLeft)
    }
    let getAllMO = await models.MO.findAll({
        include: [{
            model: models.DO,
            required: true,
            include: [{
                model: models.PO,
                required: true,
                include: [{
                    model: models.ProductionData,
                    required: true,
                    include: [{
                        model: models.ChildPartList,
                        required: true,

                    }]
                }]
            }]
        }],
        raw: true
    })
    let qtyList = []

    for (let i = 0; i < getAllMO.length; i++) {
        let getChildPartListNo = await models.ChildPartList.findOne({
            where: { ChildPartListId: getAllMO[i]['DOs.POs.ProductionData.ChildPartListId'] },
            raw: true
        })
        // console.log(getChildPartListNo["ChildPartName"])
        let findChildPart = await models.ChildPartList.findOne({
            where: { ChildPartNo: getChildPartListNo["ChildPartNo"] },
            raw: true
        })

        if (findChildPart !== null) {
            let getLatestQtyLeft = await models.QtyLeft.findOne({
                where: { ChildPartListId: findChildPart["ChildPartListId"] },
                raw: true,
                order: [["createdAt", "DESC"]],
            })
            qtyList.push(getLatestQtyLeft)
        }
    }
    let uniqueListArr = []
    let uniqueList = []
    for (let i = 0; i < qtyList.length; i++) {
        if (!uniqueList.includes(qtyList[i]["QtyLeftId"] + "," + qtyList[i]["ChildPartListId"])) {
            uniqueList.push(qtyList[i]["QtyLeftId"] + "," + qtyList[i]["ChildPartListId"])
            uniqueListArr.push(qtyList[i])
        }
    }
    for (let i = 0; i < uniqueListArr.length; i++) {
        let getChild = await models.ChildPartList.findOne({
            where: { ChildPartListId: uniqueListArr[i]["ChildPartListId"] },
            raw: true
        })
        uniqueListArr[i]["ChildPartNo"] = getChild["ChildPartNo"]
        uniqueListArr[i]["ChildPartName"] = getChild["ChildPartName"]
        uniqueListArr[i]["ThresholdValue"] = getChild["UsagePerBox"] * 0.2
        if (uniqueListArr[i]["QtyLeft"] > uniqueListArr[i]["ThresholdValue"]) {
            uniqueListArr[i]["OutOfStock"] = false
        } else {
            uniqueListArr[i]["OutOfStock"] = true
        }
    }
    console.log(uniqueListArr)
    // for (let i = 0; i < latest.length; i++) {
    //     let getMONo = await models.MO.findOne({
    //         where: { MOId: latest[i]["MOId"] },
    //         raw: true
    //     })

    //     let getChildPartNo = await models.ChildPartList.findOne({
    //         where: { ChildPartListId: latest[i]["ChildPartListId"] },
    //         raw: true
    //     })

    //     latest[i]["MONo"] = getMONo["MONo"]
    //     latest[i]["ChildPartNo"] = getChildPartNo["ChildPartNo"]
    // }

    // console.log(latest)
    console.log(uniqueListArr)
    res.send(uniqueListArr)
})

router.get("/getSummarizeOutput", async (req, res) => {

    let getAllDO = await models.DO.findAll({
        where: { DOStatusId: 2 },
        raw: true
    })

    getAllDO.sort((a, b) => a.CustomersPN >= b.CustomersPN)
    // console.log(getAllDO)
    let list = []
    let count = 0

    let prevCustomersPN = ""

    for (let i = 0; i < getAllDO.length; i++) {

        if (prevCustomersPN["CustomersPN"] !== getAllDO[i]["CustomersPN"] && prevCustomersPN["createdAt"] !== getAllDO[i]["createdAt"]) {

            if (i === 0)
                count += getAllDO[i]["TotalQty"]
            else {

                list.push({ CustomersPN: prevCustomersPN["CustomersPN"], totalSum: count, DueDate: prevCustomersPN["DueDate"] })
                count = 0
                count += getAllDO[i]["TotalQty"]
            }
            prevCustomersPN = getAllDO[i]

        } else {
            count += getAllDO[i]["TotalQty"]
        }
        if (i === getAllDO.length - 1) {
            list.push({ CustomersPN: prevCustomersPN["CustomersPN"], totalSum: count, DueDate: prevCustomersPN["DueDate"] })
        }
    }

    console.log(list)
    // console.log(getAllDO)


    // console.log(uniqueList)


    // console.log(list)
    res.send(list)
})
router.post("/getSummarizeOutput", async (req, res) => {
    try {
        let sumData = await sequelize.query(` 
        select  DueDate,iif(sum(TotalQty)=null,0,sum(TotalQty))as totalSum
		,[CustomersPN] ,PartName
        from [ushinnew].[dbo].[view_SummarizeOutput]
        group by DueDate,[CustomersPN] ,PartName
        --having DueDate = '${req.body.DueDate}'
            `)
        // console.log(sumData)
        res.send(sumData[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }

})

router.get("/getProductionResultRenew", async (req, res) => {
    let getDO = await models.DO.findAll({
        include: [{
            model: models.PO,
            required: true,
        }],
        raw: true
    })

    let foundUsageArr = []

    for (let i = 0; i < getDO.length; i++) {
        let findOneProduction = await models.ProductionData.findOne({
            where: { POId: getDO[i]['POs.POId'] },
            raw: true,
            order: [['createdAt', 'DESC']],
            include: [{
                model: models.PO,
                required: true,
                include: [{
                    model: models.DO,
                    required: true,
                    include: [{
                        model: models.PartList,
                        required: true
                    }]
                }]
            }, {
                model: models.Machine,
                required: true
            }]
        })



        if (findOneProduction !== null) {
            let findInCardLog = await models.Card_Log.findOne({
                where: [{ DOId: findOneProduction['PO.DOId'] }, { CardNo: findOneProduction['PO.CardNo'] }],
                raw: true,
                order: [['createdAt', 'DESC']],
            })
            if (findInCardLog !== null || findInCardLog !== undefined) {
                // console.log(findInCardLog)
                foundUsageArr.push({ ...findOneProduction, ...findInCardLog })
            }
        }



    }

    // foundUsageArr.sort((a, b) => a['Machine.MachineType'].localeCompare(b['Machine.MachineType']))

    let uniqueDOId = []
    let uniqueDOIdArr = []
    for (let i = 0; i < foundUsageArr.length; i++) {
        if (!uniqueDOId.includes(foundUsageArr[i]['PO.DOId'] + "," + foundUsageArr[i]['Machine.MachineType'])) {
            uniqueDOId.push(foundUsageArr[i]['PO.DOId'] + "," + foundUsageArr[i]['Machine.MachineType'])
            uniqueDOIdArr.push({ DONo: foundUsageArr[i]['PO.DO.DONo'], DOId: foundUsageArr[i]['PO.DOId'], SupName: foundUsageArr[i]['PO.DO.SuppName'], MachineType: foundUsageArr[i]['Machine.MachineType'], MachineName: foundUsageArr[i]['Machine.Machine'] + "-" + foundUsageArr[i]['Machine.MachineType'] })
        }
    }
    // console.log(foundUsageArr)
    // console.log(uniqueDOIdArr)

    let sum = []
    for (let i = 0; i < uniqueDOIdArr.length; i++) {
        let sumCount = 0

        for (let j = 0; j < foundUsageArr.length; j++) {
            if (foundUsageArr[j]['PO.DOId'] === uniqueDOIdArr[i]["DOId"] && foundUsageArr[j]['Machine.MachineType'] === uniqueDOIdArr[i]["MachineType"]) {
                sumCount += foundUsageArr[j]['PO.DO.Balance']

            }
        }
        uniqueDOIdArr[i]["sum"] = sumCount
        // console.log(sumCount)
    }
    console.log(uniqueDOIdArr)

    res.send("OK")
})

router.post("/productionResult", async (req, res) => {
    try {
        let prodData = await sequelize.query(`
    SELECT * FROM  [view_productionResult] 
    where DueDate = '${req.body.Prod_date}'`)
        console.log(prodData)
        res.send(prodData[0])
        res.status(200)
    } catch (error) {
        console.log(error)
        res.json(error)
    }
})

router.get("/MO_print", async (req, res) => {
    try {
        let data = await sequelize.query(
            `SELECT a.[MOId],a.[MONo] 
            ,b.PartName ,b.DueDate , b.DueQty ,b.[TotalCard]
            ,b.[TotalQty],b.[CustomersPN]
            ,b.[PartNoId]
            ,b.DOStatusId  
            ,MO_Print.[MONo] as MO_NoPrint
            ,MO_Print.[DueDate]  as MO_Print_DueDate
            ,MO_Print.[Date] as MO_Print_Date
            ,MO_Print.[Time] as MO_Print_Time
            ,MO_Print.[msg] as MO_Print_msg
			,MO_Print.[PartNoId] as MO_Print_PartNoId
			 ,MO_Print.[CustomersPN] as  MO_Print_CustomersPN 
           FROM [MO] a  left join [DO] b
           on  a.MOId = b.MOId  
		   left join MO_Print on b.MOId = MO_Print.MOId
		   where MO_Print.[MONo] is null and b.DOStatusId = 1 `
        )
        console.log(data)
        console.log(data)
        res.send(data[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }
})
router.post("/insert_MoDataPrint", async (req, res) => {
    try {
        const { MOId, MONo, DueDate, Date, Time, msg, createdAt, updatedAt, PartNoId, CustomersPN, DOId } = req.body
        let data = await sequelize.query(
            `
            INSERT INTO [dbo].[MO_Print] 
                        ([MOId]
                       ,[MONo]
                       ,[DueDate]
                       ,[Date]
                       ,[Time]
                       ,[msg]
                       ,[PartNoId]
                       ,[CustomersPN]
                       ,[createdAt]
                       ,[updatedAt] )
                 VALUES ( ${MOId}, '${MONo}', '${DueDate}', '${Date}', '${Time}', ${msg},${PartNoId},'${CustomersPN}', '${createdAt}', '${updatedAt}') 
                `
        )
        console.log(data)
        res.send(data[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }
})

router.get("/getMoDataPrint", async (req, res) => {
    try {
        let result_print = await sequelize.query(
            `  SELECT a.[MOId],a.[MONo]
            ,b.PartName ,b.DueDate , b.DueQty ,b.[TotalCard]
            ,b.[TotalQty],b.[CustomersPN]
            ,b.[PartNoId] 
            ,b.DOStatusId 
            ,MO_Print.[MONo] as MO_NoPrint
            ,MO_Print.[DueDate]  as MO_Print_DueDate
            ,MO_Print.[Date] as MO_Print_Date
            ,convert(varchar,  MO_Print.[Time], 24)as MO_Print_Time
            ,MO_Print.[msg] as MO_Print_msg
			,MO_Print.[PartNoId] as MO_Print_PartNoId
			 ,MO_Print.[CustomersPN] as  MO_Print_CustomersPN 
           FROM [MO] a  left join [DO] b
           on  a.MOId = b.MOId  
		  inner join [MO_Print] 
		   on b.MOId  =  [MO_Print].MOId
           where  b.DOStatusId = 1 and MO_Print.[msg] = 0 `
        )
        console.log(result_print, "*********************")
        res.send(result_print[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }
})

router.post("/insertMoDataLog", async (req, res) => {
    try {
        let result_Log = await sequelize.query(
            `INSERT INTO [ushinnewerest].[dbo].[MO_PrintLog]
            ([MO_No],[ChildPartID],[ChildPartName],[ChildPartNo],[TotalQty]
                ,[MatRemain],[MatQty],[Package],[Short],[Usage],[UsagePerBox])
      VALUES
            ('${req.body.MO_No}',${req.body.ChildPartListId},'${req.body.ChildPartName}','${req.body.ChildPartNo}',${req.body.TotalQty}
            ,${req.body.RemainingStock},${req.body.TotalQtyYield},${req.body.Package},${req.body.Short},${req.body.Usage},${req.body.UsagePerBox})
            `
        )
        console.log(result_Log, "*********************")
        res.send(result_Log[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }
})

router.get("/getMoDataRePrint", async (req, res) => {
    try {
        let result_print = await sequelize.query(
            ` SELECT a.[MOId],a.[MONo]
            ,b.PartName ,b.DueDate , b.DueQty ,b.[TotalCard]
            ,b.[TotalQty],b.[CustomersPN]
            ,b.[PartNoId] 
            ,b.DOStatusId 
            ,MO_Print.[MONo] as MO_NoPrint
            ,MO_Print.[DueDate]  as MO_Print_DueDate
            ,MO_Print.[Date] as MO_Print_Date
            ,convert(varchar,  MO_Print.[Time], 24)as MO_Print_Time
            ,MO_Print.[msg] as MO_Print_msg
			,MO_Print.[PartNoId] as MO_Print_PartNoId
			 ,MO_Print.[CustomersPN] as  MO_Print_CustomersPN  
             ,MO_Print.[updatedAt] 
           FROM [MO] a  left join [DO] b
           on  a.MOId = b.MOId  
		  inner join [MO_Print] 
		   on b.MOId  =  [MO_Print].MOId 
		  where MO_Print.[msg] <>  0   and b.DOStatusId = 1  `
        )
        console.log(result_print, "*********************")
        res.send(result_print[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }
}) 

router.post("/selectState_Reprint", async (req, res) => {
    try {
        let result_print = await sequelize.query(
            ` SELECT [MO_PrintLog]
            ,[MO_No]
            ,[ChildPartID]
            ,[ChildPartName]
            ,[ChildPartNo]
            ,[TotalQty]
            ,[MatRemain]
            ,[MatQty]
            ,[Package]
            ,[Short]
            ,[Usage]
            ,[UsagePerBox]
        FROM [ushinnewerest].[dbo].[MO_PrintLog] 
        where MO_No = '${req.body.scannedMO}' `
        )
        console.log(result_print, "**55555*****")
        res.send(result_print[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }
}) 

router.put("/msg_update", async (req, res) => {
    try {
        const { scannedCustomersPN, scannedMO, msg_new, updatedAt } = req.body
        let msg_update = await sequelize.query(
            ` UPDATE [dbo].[MO_Print]
        SET [msg] = [msg] + '${msg_new}'  , [updatedAt]  =  '${updatedAt}'
        WHERE  MONo = '${scannedMO}' and CustomersPN =  '${scannedCustomersPN}'
     ` )
        res.send(msg_update[0])
    } catch (error) {
        console.log(error)
        res.json(error)
    }
})

router.post("/getChildPartPrint", async (req, res) => {
    // console.log(req.body)
    console.log(req.body)
    let mo = req.body["scannedMO"]
    let customersPN = req.body["scannedCustomersPN"]
    console.log(mo + " " + customersPN)

    let getPrintDetail = await models.MO.findAll({
        where: { MONo: mo },
        include: [{
            model: models.DO,
            required: true,
            where: { CustomersPN: customersPN },
            include: [{
                model: models.PartList,
                required: true,
                include: [{
                    model: models.PartListUsage,
                    required: true,
                    include: [{
                        model: models.ChildPartList,
                        required: true
                    }]
                }]
            }]
        }],
        raw: true
    })

    let findMO = await models.MO.findOne({
        where: { MONo: mo },
        raw: true
    })
    let findDO = await models.DO.findAll({
        where: { MOId: findMO["MOId"] },
        raw: true
    })

    let totalQtySum = 0
    for (let i = 0; i < findDO.length; i++) {
        totalQtySum += findDO[i]["TotalQty"]
    }
    console.log(getPrintDetail)
    let printDetail = []
    for (let i = 0; i < getPrintDetail.length; i++) {
        printDetail.push(
            {
                ChildPartListId: getPrintDetail[i]['DOs.PartList.PartListUsages.ChildPartListId'],
                ChildPartNo: getPrintDetail[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartNo'],
                TotalQtyYield: totalQtySum * getPrintDetail[i]['DOs.PartList.PartListUsages.Usage'] + (Math.ceil(getPrintDetail[i]['DOs.PartList.PartListUsages.ChildPartList.UsagePerBox'] * 0.01 * getPrintDetail[i]['DOs.PartList.PartListUsages.ChildPartList.MaterialYieldPercent'])),
                UsagePerBox: getPrintDetail[i]['DOs.PartList.PartListUsages.ChildPartList.UsagePerBox'],
                ChildPartName: getPrintDetail[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartName'],
                Usage: getPrintDetail[i]['DOs.PartList.PartListUsages.Usage'],
                TotalQty: getPrintDetail[i]['DOs.TotalQty'],
                Package: Math.ceil(getPrintDetail[i]['DOs.TotalQty'] / getPrintDetail[i]['DOs.PartList.PartListUsages.ChildPartList.UsagePerBox']),
            })
    }
    let getAllQtyCheck = await models.QtyCheck.findAll({ raw: true, order: [["createdAt", "DESC"]], })


    for (let i = 0; i < printDetail.length; i++) {
        for (let j = 0; j < getAllQtyCheck.length; j++) {
            if (printDetail[i]["ChildPartListId"] === getAllQtyCheck[j]["ChildPartListId"]) {
                printDetail[i]["RemainingStock"] = getAllQtyCheck[j]["QtyLeft"]
                printDetail[i]["Short"] = Math.max(printDetail[i]["TotalQtyYield"] - getAllQtyCheck[j]["QtyLeft"], 0)
                j = getAllQtyCheck.length
            }
        }

    }
    for (let i = 0; i < printDetail.length; i++) {
        if (printDetail[i]["RemainingStock"] === undefined || printDetail[i]["RemainingStock"] === 0) {
            printDetail[i]["RemainingStock"] = 0
            printDetail[i]["Short"] = printDetail[i]["TotalQtyYield"]
        }
    }


    console.log(printDetail)
    res.send(printDetail)
})

module.exports = router;