const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../models");
const moment = require("moment");
const tools = require("../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../models");


//////////////////////////////////////////////////////////////////////////Bulk Insertion///////////////////////////////////////////
router.post("/insertBulkMO", async (req, res) => {

    await models.MO.bulkCreate(req.body)
    res.send("OK")

})

router.post("/insertBulkDO", async (req, res) => {

    for (let i = 0; i < req.body.length; i++) {
        const {
            DONo,
            Supp,
            SuppName,
            CMSP,
            Prcs,
            PartName,
            DueDate,
            DueQty,
            PrdtQty,
            Balance,
            Unit,
            ODDays,
            TotalCard,
            // TotalQty,
            DeliveryDueDate,
            CustomersPN,
            PartNoId,
            DOStatusId,
            MOId,
        } = req.body[i];

        let insertion = await models.DO.create({
            DONo: DONo,
            Supp: Supp,
            SuppName: SuppName,
            CMSP: Number(CMSP),
            Prcs: Number(Prcs),
            PartName: PartName,
            DueDate: tools.formatDate(DueDate),
            DueQty: Number(DueQty),
            PrdtQty: Number(PrdtQty),
            Balance: Number(Balance),
            Unit: Unit,
            ODDays: Number(ODDays),
            TotalCard: Number(TotalCard),
            TotalQty: Number(DueQty) * Number(TotalCard),
            DeliveryDueDate: tools.formatDate(DeliveryDueDate),
            CustomersPN: CustomersPN,
            PartNoId: Number(PartNoId),
            DOStatusId: Number(DOStatusId),
            MOId: MOId,
        });
    }

    res.json({
        api_result: "OK",
        message: "Success.",
    });

});


router.post("/insertBulkGroupPartList", async (req, res) => {
    await models.GroupPartList.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkPartList", async (req, res) => {
    try {
        for (let i = 0; i < req.body.length; i++) {
            let str = ""
            str = req.body[i].PartNo.replace(/(-DOM|-EXP)/g, "")
            let groupPartNoId = await models.GroupPartList.findOne({
                where: { PartNo: str },
                raw: true
            })
            req.body[i]["GroupPartNoId"] = groupPartNoId.GroupPartNoId
            await models.PartList.create(req.body[i])

        }
    } catch (e) {
        console.log(e)
    }

    res.send("OK")
})

router.post("/insertBulkMasterDOStatus", async (req, res) => {
    await models.MasterDOStatus.bulkCreate(req.body)
    res.send("OK")
})
router.post("/insertBulkPO", async (req, res) => {
    await models.PO.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkChildPartList", async (req, res) => {
    await models.ChildPartList.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkEmployee", async (req, res) => {
    await models.Employee.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkEmployeeRole", async (req, res) => {
    await models.EmployeeRole.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkMasterUsageStatus", async (req, res) => {
    await models.MasterUsageStatus.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkMachine", async (req, res) => {
    await models.Machine.bulkCreate(req.body)
    res.send("OK")
})
router.post("/insertBulkPartListUsage", async (req, res) => {
    await models.PartListUsage.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkMasterPOStatus", async (req, res) => {
    await models.MasterPOStatus.bulkCreate(req.body)
    res.send("OK")
})



//////////////////////////////////////////////////////////////Receive Material Part/////////////////////////////////////////////////////////

var state = {
    savedMOValues: [],
    MOValue: "",
    scanText: "",
    storedValidMat: [
        // { id: 1, values: [{ partNo: "testpart" }, { lotNo: "lot" }] },
        // { id: 2, values: [{ partNo: "testpart2" }, { lotNo: "lot" }] },
    ],
    row: 0,
};

var sumReceiveMaterialPartList = [];

router.get("/getMatchingIdState", async (req, res) => {
    res.send(state);
});

router.post("/checkMatchingId", async (req, res) => {
    state = req.body;

    let partNoPattern = /\w{0,6}-\D{3}|\w{0,6}-(.+)-\D{3}/g;
    const tag = req.body.scanText;
    const arr = [...tag.matchAll(partNoPattern)];

    let partNo = "";
    try {
        partNo = arr[0][0];
    } catch (error) { }

    let lotNo = "";
    let qty;
    let polotlabel = "";

    let qtyPatternUshinJP = /\d{20}\s\d+\s\d+/g;
    let check = [...tag.matchAll(qtyPatternUshinJP)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is now ushinJP
        let deleteLines = [...data.matchAll(/\d{12}\s/g)];
        let deletedLine = String(deleteLines[0][0]).trim();
        let subqty = deletedLine.substring(0, 7);
        let pureQty = subqty.replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        lotNo = poNo;
        polotlabel = lotNo;
    } catch (error) {
        //console.log(check);
    }
    let qtyWebEdiPattern = /(\w\d-)\d{17}|\w+\d\s\s\d{15}/g;
    check = [...tag.matchAll(qtyWebEdiPattern)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is web edi
        let pureQty = data.substring(5, 12).replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        polotlabel = poNo;
    } catch (error) {
    }
    let qtyPatternReprint = /(\w\d-)\d{21}|\s\s[A-Za-z]\d{21}/g;
    check = [...tag.matchAll(qtyPatternReprint)];
    try {
        let data = String(check[0][0]);
        //tag type is now reprint
        let subqty = data.substring(5, 12);
        console.log(subqty);
        let pureQty = subqty.replace(/^0+/, "");
        let lotNoPattern = /\s\s\s\w+\s/g;
        let match = [...tag.matchAll(lotNoPattern)];
        let lotNo = match[0][0].trim().substring(4, 15);
        qty = pureQty;
        polotlabel = lotNo;
    } catch (error) {
    }
    let qtyProductionInHousePattern = /\s{9}\w{18}/g;
    check = [...tag.matchAll(qtyProductionInHousePattern)];
    try {
        let data = String(check[0][0]).trim();
        let subqty = data.substring(3, 10).replace(/^0+/, "");
        let labelNo = data.substring(10, 17);
        qty = subqty;
        polotlabel = labelNo;
    } catch (error) { }

    // console.log(partNo + " " + lotNo + " " + qty + " " + polotlabel)
    try {
        let getChildPartListId = await models.ChildPartList.findAll({
            where: { ChildPartNo: partNo },
            raw: true
        });

        let childPartListId = getChildPartListId[0]["ChildPartListId"];

        let mo = await models.MO.findAll({
            where: { MONo: state.MOValue },
            raw: true
        });


        let getAllDO = await models.DO.findAll({
            where: { MOId: mo[0]["MOId"] },
            raw: true
        });


        let allDO = getAllDO;
        for (let i = 0; i < allDO.length; i++) {
            sumReceiveMaterialPartList.push({
                MOId: mo[0]["MOId"],
                ChildPartListId: childPartListId,
                ChildPartLot: polotlabel,
            });
        }

    } catch (error) {
        state.scanText = "";
        res.send(state);
        return;
    }

    state.row++;
    state.storedValidMat.push({
        // id: state.row,
        values: [
            { no: state.row },
            { partNo: partNo },
            { polotlabel: polotlabel },
            { qty: qty },
        ],
    });
    state.scanText = "";
    res.send(state);
});

router.post("/submitAllData", async (req, res) => {
    //insert all store
    try {
        if (sumReceiveMaterialPartList.length === 0) {
            res.send("No Data");
            return;
        }
        await models.ReceiveMaterialPart.bulkCreate(sumReceiveMaterialPartList);

        let uniqueSumReceiveMaterialPartList = []
        let uniqueSumReceiveMaterialPartListArr = []
        for (let i = 0; i < sumReceiveMaterialPartList.length; i++) {//กรองเอาตัวที่ไม่ซ้ำ
            if (!uniqueSumReceiveMaterialPartList.includes(sumReceiveMaterialPartList[i]["ChildPartListId"])) {
                uniqueSumReceiveMaterialPartList.push(sumReceiveMaterialPartList[i]["ChildPartListId"])
                uniqueSumReceiveMaterialPartListArr.push(sumReceiveMaterialPartList[i])
            }
        }

        console.log(uniqueSumReceiveMaterialPartListArr)

        for (let i = 0; i < uniqueSumReceiveMaterialPartListArr.length; i++) {
            let childPart = await models.ChildPartList.findOne({
                where: { ChildPartListId: uniqueSumReceiveMaterialPartListArr[i]["ChildPartListId"] },
                raw: true
            })

            let mo = await models.MO.findOne({
                where: { MONo: state.MOValue },
                raw: true
            })

            let latestQtyLeft = await models.QtyLeft.findAll({
                where: [{ MOId: mo["MOId"] }, { ChildPartListId: uniqueSumReceiveMaterialPartListArr[i]["ChildPartListId"] }],
                order: [['createdAt', 'DESC']],
                raw: true
            })


            if (latestQtyLeft.length !== 0) {
                await models.QtyLeft.create({
                    MOId: mo["MOId"],
                    QtyLeft: Number(childPart["UsagePerBox"]) + Number(latestQtyLeft[0]["QtyLeft"]),
                    ChildPartListId: Number(uniqueSumReceiveMaterialPartListArr[i]["ChildPartListId"])
                })
            } else {

                await models.QtyLeft.create({
                    MOId: mo["MOId"],
                    QtyLeft: Number(childPart["UsagePerBox"]),
                    ChildPartListId: Number(uniqueSumReceiveMaterialPartListArr[i]["ChildPartListId"])
                })
            }
        }

        sumReceiveMaterialPartList = [];

        const index = state.savedMOValues.indexOf(state.MOValue)
        if (index > -1) {
            state.savedMOValues.splice(index, 1)
        }

        state = {
            savedMOValues: state.savedMOValues,
            MOValue: "",
            scanText: "",
            storedValidMat: [],
            row: 0,
        };
        // console.log(state)
        res.send(state);
    } catch (error) {
        console.log(error)
        res.send("No Data");
    }
});

router.get("/clearData", async (req, res) => {
    sumReceiveMaterialPartList = [];
    state = {
        savedMOValues: [],
        MOValue: "",
        scanText: "",
        storedValidMat: [],
        row: 0,
    };
    res.send(state);
});

router.post("/populateMOSplit", async (req, res) => {

    try {
        const splitArr = String(req.body.data).split(",")
        state.savedMOValues = splitArr
        // console.log(state)
    } catch (e) {
        console.log(e)
    }
    res.send(state)

})
////////////////////////////////////////////////////////Input Machine Line //////////////////////////////////////////

var postate = {
    PO: "",
    EMP: "",
    MC: "",
    PartNo: "",
    row: 0,
    storedValidMat: [
    ],
    storedMatOfEachPO: [
    ],
    scanMat: "",
    storedMat: [
    ],
};


router.get("/getPoState", async (req, res) => {
    postate.PartNo = "";
    res.send(postate);
});

router.post("/checkMatchingPO", async (req, res) => {
    postate = req.body;

    let poPattern = /D.*/g;
    const tag = postate.PO;
    const arr = [...tag.matchAll(poPattern)];
    try {

        let foundPOId = arr[0][0];

        postate.row++;
        postate.storedValidMat.push({
            PONo: foundPOId,
            Emp: postate.EMP,
            MC: postate.MC,
            PartNo: postate.PartNo,
            row: postate.row,
        });
        res.send(postate);

    } catch (e) {
        console.log(e)
    }


});

router.post("/checkMatchingPOMatNew", async (req, res) => {
    postate = req.body.poState;

    let partNoPattern = /\w{0,6}-\D{3}|\w{0,6}-(.+)-\D{3}/g;
    const tag = req.body.scannedVal;
    const arr = [...tag.matchAll(partNoPattern)];

    let partNo = "";
    try {
        partNo = arr[0][0];
    } catch (error) {
        postate.scanMat = "";
        res.send(postate);
        return;
    }

    postate.storedMat.push({ matScan: partNo });
    postate.scanMat = "";
    res.send(postate);
});

router.post("/checkMatchingPOMat", async (req, res) => {

    let partNoPattern = /\w{0,6}-\D{3}|\w{0,6}-(.+)-\D{3}/g;
    const tag = req.body.enterVal;
    const arr = [...tag.matchAll(partNoPattern)];

    let partNo = "";
    try {
        partNo = arr[0][0];
    } catch (error) { }

    postate.storedMatOfEachPO.push([
        { PORow: req.body.rowId, scannedMat: partNo },
    ]);
    res.send(postate);
});


router.get("/insertAllPOPage", async (req, res) => {

    let poPattern = /D.*/g;
    const tag = postate.PO;
    const arr = [...tag.matchAll(poPattern)];
    let foundPOId = ""
    try {
        foundPOId = arr[0][0];
    } catch (e) {
        console.log(e)
        res.send("No Data");
        return
    }



    //insert all shopper
    try {
        if (postate.storedValidMat.length === 0 || postate.storedMat.length === 0) {
            res.send("No Data");
            return;
        }
        let getMachineId = await models.Machine.findAll({
            where: { MachineName: postate.MC },
        });

        let foundMachineId = JSON.parse(JSON.stringify(getMachineId))[0][
            "MachineId"
        ];
        let foundMachine = JSON.parse(JSON.stringify(getMachineId))[0][
            "Machine"
        ];
        let foundMachineType = JSON.parse(JSON.stringify(getMachineId))[0][
            "MachineType"
        ]

        let POStatusName = foundMachine + "-" + foundMachineType

        let findMachinePOStatusWithMachineName =
            await models.MasterPOStatus.findAll({
                where: { POStatus: POStatusName + "_open" },
            });
        let foundMasterPOStatusMasterPOStatusId = JSON.parse(
            JSON.stringify(findMachinePOStatusWithMachineName)
        )[0]["MasterPOStatusId"];


        for (let i = 0; i < postate.storedValidMat.length; i++) {
            // let foundPOId = postate.PO.substring(96, 106).trim();
            let getPOId = await models.PO.findAll({
                where: { PONo: foundPOId },
                raw: true,
            });

            for (let j = 0; j < postate.storedMat.length; j++) {
                let getChildPartListId = await models.ChildPartList.findAll({
                    where: { ChildPartNo: postate.storedMat[j]["matScan"] },
                });
                let foundChildPartListId = JSON.parse(JSON.stringify(getChildPartListId))[0]["ChildPartListId"];
                // console.log(foundPOId + " " + foundChildPartListId + " " + foundMachineId + " " + postate.EMP);
                await models.ProductionData.create({
                    POId: getPOId[0]["POId"],
                    ChildPartListId: foundChildPartListId,
                    MachineId: foundMachineId,
                    EmployeeId: postate.EMP,
                });

                // let getDO = await models.DO.findOne({
                //     where: { DOId: getPOId[0]["DOId"] },
                //     raw: true
                // })
                //  console.log(getDO)

                //  console.log(JSON.parse(JSON.stringify(getChildPartListId))[0]["ChildPartListId"])
                // let checkQtyLeft = await models.QtyLeft.findAll({
                //     where: { ChildPartListId: JSON.parse(JSON.stringify(getChildPartListId))[0]["ChildPartListId"] },
                //     raw: true,
                // })

                // console.log(checkQtyLeft.length)

                // if (checkQtyLeft.length !== 0) {
                //     for (let k = 0; k < checkQtyLeft.length; k++) {
                //         // console.log(checkQtyLeft[k])
                //         console.log(checkQtyLeft[k]["QtyLeft"] + JSON.parse(JSON.stringify(getChildPartListId))[0]["UsagePerBox"])
                //         await models.QtyLeft.create({
                //             MOId: getDO["MOId"],
                //             QtyLeft: Number(JSON.parse(JSON.stringify(getChildPartListId))[0]["UsagePerBox"]) + Number(checkQtyLeft[k]["QtyLeft"]),
                //             ChildPartListId: JSON.parse(JSON.stringify(getChildPartListId))[0]["ChildPartListId"]
                //         })
                //     }
                //     // for (let i = 0; i < checkQtyLeft.length; i++) {
                //     //     await models.QtyLeft.create({
                //     //         where:{
                //     //             MOId: checkQtyLeft[i]["MOId"],
                //     //             QtyLeft: checkQtyLeft[i]["QtyLeft"] + JSON.parse(JSON.stringify(getChildPartListId))[0]["UsagePerBox"],
                //     //             ChildPartListId: JSON.parse(JSON.stringify(getChildPartListId))[0]["ChildPartListId"]
                //     //         }
                //     //     })
                //     // }

                // } else {
                //     await models.QtyLeft.create({
                //         MOId: getDO["MOId"],
                //         QtyLeft: JSON.parse(JSON.stringify(getChildPartListId))[0]["UsagePerBox"],
                //         ChildPartListId: JSON.parse(JSON.stringify(getChildPartListId))[0]["ChildPartListId"]
                //     })
                // }


            }

            await models.POStatus.create({
                MasterPOStatusId: foundMasterPOStatusMasterPOStatusId,
                POId: getPOId[0]["POId"],
                MfgDate: new Date(Date.now()).toISOString(),
            });
        }



    } catch (error) {

        res.send("No Data");
        return;
    }

    postate = {
        PO: "",
        EMP: "",
        MC: "",
        PartNo: "",
        row: 0,
        storedValidMat: [],
        storedMatOfEachPO: [],
        scanMat: "",
        storedMat: [],
    };
    res.send(postate);
});

router.get("/clearDataPOPage", async (req, res) => {
    postate = {
        PO: "",
        EMP: "",
        MC: "",
        PartNo: "",
        row: 0,
        storedValidMat: [],
        storedMatOfEachPO: [],
        scanMat: "",
        storedMat: [],
    };
    res.send(postate);
});

/////////////////////////////////////////////////////////////WIP output//////////////////////////////////////////////////////////
router.post("/popageclose", async (req, res) => {
    try {

        // console.log(req.body.data);

        let poPattern = /D.*/g;
        const tag = req.body.data[0]['scannedPONo'];
        const arr = [...tag.matchAll(poPattern)];
        let foundPO = ""
        try {
            foundPO = arr[0][0]
        } catch (e) {
            res.send("No Data");
            return;
        }
        // console.log(foundPO)

        let scannedPONo = foundPO;
        if (scannedPONo.length === 0) {
            res.send("No Data");
            return;
        }

        let findPO = await models.PO.findOne({
            where: { PONo: scannedPONo },
            raw: true
        })

        // console.log(findPO)
        await models.Card_Log.create({
            DOId: findPO["DOId"],
            CardNo: findPO["CardNo"]
        })

        let findAllDoCountInCardLog = await models.Card_Log.findAll({
            where: { DOId: findPO["DOId"] }
        })

        findAllDoCountInCardLog = findAllDoCountInCardLog.length
        // console.log(findAllDoCountInCardLog)

        let getDOId = await models.DO.findOne({
            where: { DOId: findPO["DOId"] },
            raw: true
        })

        let findPOStatus = await models.POStatus.findOne({
            where: { POId: findPO["POId"] },
            raw: true
        })

        console.log(findPOStatus)
        let getMasterPOStatus = await models.MasterPOStatus.findOne({
            where: { MasterPOStatusId: findPOStatus["MasterPOStatusId"] },
            raw: true
        })

        let machine = String(getMasterPOStatus["POStatus"]).replace(/-\d+/g, "")
        let substringMachine = machine.split("_")

        // console.log(substringMachine[0])

        let getMasterPOStatusClose = await models.MasterPOStatus.findOne({
            where: { POStatus: substringMachine[0] + "_close" },
            raw: true
        })

        console.log(getMasterPOStatusClose)

        await models.POStatus.create({
            MasterPOStatusId: getMasterPOStatusClose["MasterPOStatusId"],
            POId: findPO["POId"],
            MfgDate: new Date(Date.now()).toISOString(),
        })


        if (findAllDoCountInCardLog === getDOId["TotalCard"]) {
            console.log("close that do")

            let getDOId = await models.DO.findOne({
                where: { DOId: findPO["DOId"] },
                raw: true
            })

            await models.DO.update(
                {
                    DOStatusId: 2,
                },
                {
                    where: {
                        DOId: findPO["DOId"]
                    }
                }).then(async () => {
                    let finishedDOStatus = {
                        DOId: getDOId["DOId"],
                        DONo: getDOId["DONo"],
                        Supp: getDOId["Supp"],
                        SuppName: getDOId["SuppName"],
                        CMSP: getDOId["CMSP"],
                        Prcs: getDOId["Prcs"],
                        PartName: getDOId["PartName"],
                        DueDate: getDOId["DueDate"],
                        DueQty: getDOId["DueQty"],
                        PrdtQty: getDOId["PrdtQty"],
                        Balance: getDOId["Balance"],
                        Unit: getDOId["Unit"],
                        ODDays: getDOId["ODDays"],
                        TotalCard: getDOId["TotalCard"],
                        TotalQty: getDOId["TotalQty"],
                        DeliveryDueDate: getDOId["DeliveryDueDate"],
                        CustomersPN: getDOId["CustomersPN"],
                        PartNoId: getDOId["PartNoId"],
                        DOStatusId: 2,
                        createdAt: getDOId["createdAt"],
                        updatedAt: getDOId["updatedAt"],
                        MOId: getDOId["MOId"]
                    }
                    let jsonArr = [getDOId, finishedDOStatus]
                    await models.DO_Log.bulkCreate(jsonArr)

                })

            // let findAllDO = await models.DO.findAll({
            //     where: { MOId: getDOId["MOId"] },
            //     raw: true,
            // })

            // let finishedDOCount = 0

            // for (let i = 0; i < findAllDO.length; i++) {

            //     if (findAllDO[i]["DOStatusId"] === 2) {
            //         finishedDOCount++
            //     }
            // }
            // if(finishedDOCount === findAllDO.length){

            // }


        }


    } catch (error) {
        res.send("No Data");
        return;
    }

    res.send("OK");
});

//////////////////////////////////////////////////////////////////Traceability////////////////////////////////////////////////

router.post("/checkDo", async (req, res) => {
    let DO_PO_TABLE = [];
    let MC_TABLE = [];
    let LOT_TABLE = [];
    try {
        if (req.body.data["scannedDo"].length === 0) {
            res.send("No Data");
            return;
        }
        let getJoinedDOPO = await models.DO.findAll({
            // attributes: ['DONo'],
            where: { DONo: req.body.data["scannedDo"] },
            include: [
                {
                    model: models.PO,
                    required: true,
                    // attributes: ['PONo'],
                    raw: true,
                },
                {
                    model: models.PartList,
                    required: true,
                },
                {
                    model: models.MO,
                    required: true,
                },
            ],
            raw: true,
        });

        let getAvailableChildParts = await models.DO.findAll({
            where: { DONo: req.body.data["scannedDo"] },
            include: [
                {
                    model: models.PO,
                    required: true,
                    include: [
                        {
                            model: models.ProductionData,
                            required: true,
                            include: [
                                {
                                    model: models.ChildPartList,
                                    required: true,
                                    include: [{
                                        model: models.ReceiveMaterialPart,
                                        required: true
                                    }]
                                },
                                {
                                    model: models.Machine,
                                    required: true
                                }
                            ]
                        }]
                }, {
                    model: models.MO,
                    required: true
                }
            ],
            raw: true
        })


        //console.log(getJoinedDOPO)
        //console.log(getAvailableChildParts)

        let distinctChildList = []
        let distinctChildListArr = []

        for (let i = 0; i < getAvailableChildParts.length; i++) {
            if (!distinctChildList.includes(getAvailableChildParts[i]['POs.ProductionData.ChildPartList.ChildPartNo'])) {
                distinctChildList.push(getAvailableChildParts[i]['POs.ProductionData.ChildPartList.ChildPartNo'])
                distinctChildListArr.push(getAvailableChildParts[i])
            }
        }
        console.log(distinctChildListArr)

        DO_PO_TABLE = getJoinedDOPO;
        MC_TABLE = distinctChildListArr;
        LOT_TABLE = getAvailableChildParts;

    } catch (error) {
        console.log(error);
        res.send({ DO_PO_TABLE: [], MC_TABLE: [], LOT_TABLE: [] });
        return;
    }

    res.send({
        DO_PO_TABLE: DO_PO_TABLE,
        MC_TABLE: MC_TABLE,
        LOT_TABLE: LOT_TABLE,
    });
});

///////////////////////////////////////////////////////////////////////Delivery Order Detail///////////////////////////////////
router.get("/get_DO", async (req, res) => {
    let DOdata = await models.DO.findAll({
        raw: true,
        include: [
            {
                model: models.PartList,
                required: true,
                raw: true,
            },
            {
                model: models.MasterDOStatus,
                required: true,
                raw: true,
            },
        ],
    });
    console.log(DOdata);
    res.send(DOdata);
});

/////////////////////////////////////////////////////////////////////////MOSS////////////////////////////////////////////////////
//insert childpartlist, partlist, machine, masterpostatus,employee
router.post("/insertChildPartList", async (req, res) => {
    let data = req.body;

    try {
        await models.ChildPartList.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertPartList", async (req, res) => {
    let data = req.body;
    try {
        await models.PartList.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});
router.post("/insertGroupPartList", async (req, res) => {
    let data = req.body;
    try {
        await models.GroupPartList.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertMachine", async (req, res) => {
    let data = req.body;
    try {
        await models.Machine.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertMasterPOStatus", async (req, res) => {
    let data = req.body;
    try {
        await models.MasterPOStatus.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertEmployee", async (req, res) => {
    let data = req.body;

    try {
        let getEmployeeRoleName = await models.EmployeeRole.findOne({
            where: { RoleName: data.RoleName },
            raw: true,
        });

        await models.Employee.create({
            EmployeeId: data.EmployeeId,
            EmployeeName: data.EmployeeName,
            RoleId: getEmployeeRoleName["RoleId"],
        });
    } catch (error) {
        console.log(error);
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertEmployeeRole", async (req, res) => {
    let data = req.body;
    try {
        await models.EmployeeRole.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.get("/getRole", async (req, res) => {
    let data = await models.EmployeeRole.findAll({ raw: true });
    console.log(data);
    res.send(data);
});

// router.get("/getEmployee", async (req, res) => {
//     let data = await models.Employee.findAll({
//         raw: true,
//         include: [
//             {
//                 model: models.EmployeeRole,
//                 required: true,
//                 raw: true,
//             },
//         ],
//     });
//     console.log(data);
//     res.send(data);
// });

router.get("/ChildPartList", async (req, res) => {
    let data = await models.ChildPartList.findAll({ raw: true });
    console.log(data);
    res.send(data);
});

//////////////////////////////////////////////////////////////Dashboard///////////////////////////////////////////////

router.get("/getDashboardDetails", async (req, res) => {
    let result = await models.TargetUsage.findAll({
        include: [
            {
                model: models.ActualUsage,
                required: true,
                raw: true,
                include: [
                    {
                        model: models.MasterUsageStatus,
                        required: true,
                        raw: true,
                    },
                ],
            },
            {
                model: models.DO,
                required: true,
                raw: true,
                include: [
                    {
                        model: models.MO,
                        required: true,
                        raw: true,
                    },
                ],
            },
        ],
        raw: true,
    });
    // console.log(result)
    let uniqueList = [];
    let uniqueTable = [];

    for (let i = 0; i < result.length; i++) {
        let getPo = await models.PO.findAll({
            where: { DOId: result[i]["DOId"] },
            raw: true,
        });
        for (let j = 0; j < getPo.length; j++) {
            if (!uniqueList.includes(getPo[j]["POId"])) {
                uniqueList.push(getPo[j]["POId"]);

                uniqueTable.push(Object.assign(getPo[j], result[i]));
            }
        }

    }

    for (let i = 0; i < uniqueTable.length; i++) {

        let getProductionData = await models.ProductionData.findOne({
            where: { POId: uniqueTable[i]["POId"] },
            order: [["createdAt", "DESC"]],
            raw: true,
        });
        uniqueTable[i]["ProductionData.ProductionDataId"] =
            getProductionData["ProductionDataId"];
        uniqueTable[i]["ProductionData.POId"] = getProductionData["POId"];
        uniqueTable[i]["ProductionData.ChildPartListId"] =
            getProductionData["ChildPartListId"];
        uniqueTable[i]["ProductionData.EmployeeId"] =
            getProductionData["EmployeeId"];
        uniqueTable[i]["ProductionData.createdAt"] = getProductionData["createdAt"];
        uniqueTable[i]["ProductionData.updatedAt"] = getProductionData["updatedAt"];
        uniqueTable[i]["ProductionData.MachineId"] = getProductionData["MachineId"];

        let getMachine = await models.Machine.findOne({
            where: { MachineId: getProductionData["MachineId"] },
            raw: true,
        });
        uniqueTable[i]["Machine.MachineId"] = getMachine["MachineId"];
        uniqueTable[i]["Machine.MachineName"] = getMachine["MachineName"];
        uniqueTable[i]["Machine.MachineType"] = getMachine["MachineType"];
        uniqueTable[i]["Machine.MachineNo"] = getMachine["MachineNo"];
        uniqueTable[i]["Machine.createdAt"] = getMachine["createdAt"];
        uniqueTable[i]["Machine.updatedAt"] = getMachine["updatedAt"];
        // console.log(getMachine)
    }

    console.log(uniqueTable);
    // console.log(uniqueTable)
    res.send(result);
});

router.get("/getProductionResult", async (req, res) => {
    let data = await models.TargetUsage.findAll({
        include: [
            {
                model: models.ActualUsage,
                required: true,
                raw: true,
            },
            {
                model: models.DO,
                required: true,
                raw: true,
                where: { DOStatusId: 1 },
                include: [
                    {
                        model: models.MO,
                        required: true,
                        raw: true,
                    },
                ],
            },
            {
                model: models.PartListUsage,
                required: true,
                raw: true,
                include: [
                    {
                        model: models.ChildPartList,
                        required: true,
                        raw: true,
                        include: [
                            {
                                model: models.ProductionData,
                                required: true,
                                raw: true,
                                include: [
                                    {
                                        model: models.Machine,
                                        required: true,
                                        raw: true,
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        ],
        raw: true,
    });

    data.sort(function (a, b) {
        return a[
            "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
        ].localeCompare(
            b["PartListUsage.ChildPartList.ProductionData.Machine.MachineName"]
        );
    });
    let previousMachine;
    let machineDetail = [];

    let result = [];
    for (let i = 0; i < data.length; i++) {
        if (
            previousMachine !=
            data[i]["PartListUsage.ChildPartList.ProductionData.Machine.MachineName"]
        ) {
            previousMachine =
                data[i][
                "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
                ];
            machineDetail.push({
                MachineName:
                    data[i][
                    "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
                    ],
                MONo: data[i]["DO.MO.MONo"],
            });
        }

        // console.log(data[i]["PartListUsage.ChildPartList.ProductionData.Machine.MachineName"])
    }
    let sum = 0;

    for (let i = 0; i < machineDetail.length; i++) {
        for (let j = 0; j < data.length; j++) {
            if (
                machineDetail[i]["MachineName"] ===
                data[j][
                "PartListUsage.ChildPartList.ProductionData.Machine.MachineName"
                ]
            ) {
                sum += data[j]["ActualUsage.ActualUsage"];
            }
        }
        machineDetail[i]["Sum"] = sum;
        // result.push({MONo: data[]})
        // console.log(sum)
        sum = 0;
    }

    // console.log(machineDetail)

    res.send(machineDetail);
});

router.get("/getProductionResultNew", async (req, res) => {
    let findAllPOStatus = await models.POStatus.findAll({ raw: true, include: [{ model: models.MasterPOStatus, required: true, raw: true }] })
    let arr = []

    for (let i = 0; i < findAllPOStatus.length; i++) {

        if (String(findAllPOStatus[i]['MasterPOStatus.POStatus']).includes("_open")) {
            let findLatestOpenedPO = await models.POStatus.findOne({
                where: [{ POId: findAllPOStatus[i]['POId'] }, { MasterPOStatusId: findAllPOStatus[i]['MasterPOStatusId'] }],
                order: [['createdAt', 'DESC']],
                raw: true,
                include: [{
                    model: models.MasterPOStatus,
                    required: true,
                    raw: true
                }]
            })

            let findMasterPOStatusClose = await models.MasterPOStatus.findOne({
                where: { POStatus: String(findLatestOpenedPO['MasterPOStatus.POStatus']).replace(/_\w+/g, "") + "_close" },
                raw: true
            })


            // console.log(findLatestOpenedPO)
            // console.log(findMasterPOStatusClose)
            let findLatestClosedPO = await models.POStatus.findOne({
                where: [{ MasterPOStatusId: findMasterPOStatusClose["MasterPOStatusId"] }, { POId: findLatestOpenedPO["POId"] }],
                order: [['createdAt', 'DESC']],
                raw: true,
                include: [{ model: models.MasterPOStatus, required: true, raw: true }]
            })
            if (findLatestClosedPO !== null) {
                let findPO = await models.PO.findOne({
                    where: { POId: findLatestClosedPO["POId"] },
                    raw: true
                })
                let findDO = await models.DO.findOne({
                    where: { DOId: findPO["DOId"] },
                    raw: true
                })

                let findMO = await models.MO.findOne({
                    where: { MOId: findDO["MOId"] },
                    raw: true
                })

                // console.log(findMO)

                let getDOId = await models.PO.findOne({
                    where: { POId: findLatestClosedPO["POId"] },
                    raw: true
                })
                let getDOBalance = await models.DO.findOne({
                    where: { DOId: getDOId["DOId"] },
                    raw: true
                })
                // console.log(getDOBalance["Balance"])
                // total += getDOBalance["Balance"]
                // console.log(findLatestClosedPO)
                arr.push({ MONo: findMO["MONo"], MasterPOStatusId: findLatestClosedPO["MasterPOStatusId"], POStatus: findLatestClosedPO['MasterPOStatus.POStatus'], Balance: getDOBalance["Balance"] })

            }

        }
    }

    arr.sort(function (a, b) {
        return a.MasterPOStatusId - b.MasterPOStatusId
    })

    let prevMasterPOId = -1
    let total = 0
    let sum = []
    for (let i = 0; i < arr.length; i++) {

        if (prevMasterPOId !== arr[i]["MasterPOStatusId"]) {
            if (i === 0) {
                total += arr[i]["Balance"]
            }
            sum.push({ MONo: arr[i]["MONo"], POStatus: String(arr[i]["POStatus"]).replace(/_\w+/g, ""), totalSum: total })
            total = 0
            prevMasterPOId = arr[i]["MasterPOStatusId"]
            total += arr[i]["Balance"]

        } else {
            total += arr[i]["Balance"]
        }

    }
    console.log(sum)

    res.send(sum)

})

router.get("/getMaterialInlineStock", async (req, res) => {
    let getAllQtyLeft = await models.QtyLeft.findAll({
        raw: true,
    })
    let latestCheck = []
    let latest = []
    for (let i = 0; i < getAllQtyLeft.length; i++) {
        let findLatestQtyLeft = await models.QtyLeft.findOne({
            where: [{ MOId: getAllQtyLeft[i]["MOId"] }, { ChildPartListId: getAllQtyLeft[i]["ChildPartListId"] }],
            order: [['createdAt', 'DESC']],
            raw: true,
        })

        if (!latestCheck.includes(findLatestQtyLeft["MOId"] + "," + findLatestQtyLeft["ChildPartListId"])) {
            latestCheck.push(findLatestQtyLeft["MOId"] + "," + findLatestQtyLeft["ChildPartListId"])
            latest.push(findLatestQtyLeft)
        }
        // console.log(findLatestQtyLeft)
    }

    for (let i = 0; i < latest.length; i++) {
        let getMONo = await models.MO.findOne({
            where: { MOId: latest[i]["MOId"] },
            raw: true
        })

        let getChildPartNo = await models.ChildPartList.findOne({
            where: { ChildPartListId: latest[i]["ChildPartListId"] },
            raw: true
        })

        latest[i]["MONo"] = getMONo["MONo"]
        latest[i]["ChildPartNo"] = getChildPartNo["ChildPartNo"]
    }

    console.log(latest)
    // console.log(getAllQtyLeft)
    res.send(latest)
})

router.get("/getSummarizeOutput", async (req, res) => {

    let getAllDO = await models.DO.findAll({
        where: { DOStatusId: 2 },
        raw: true
    })

    getAllDO.sort((a, b) => a.CustomersPN >= b.CustomersPN)

    // console.log(getAllDO)

    let list = []
    let count = 0

    for (let i = 0; i < getAllDO.length; i++) {

        for (let j = i; j < getAllDO.length; j++) {

            if (getAllDO[i]["CustomersPN"] == getAllDO[j]["CustomersPN"]) {
                if (i == getAllDO.length - 1 && j == getAllDO.length - 1) {
                    list.push({ CustomersPN: getAllDO[i]["CustomersPN"], totalSum: count })
                    break
                }
                // console.log(i + " " + j)
                // console.log(getAllDO[i]["CustomersPN"])
                count += getAllDO[i]["TotalQty"]
            } else {
                list.push({ CustomersPN: getAllDO[i]["CustomersPN"], totalSum: count })//
                // console.log(count)
                i = j - 1
                j = getAllDO.length
                count = 0
            }
        }


    }
    console.log(list)
    // console.log(getAllDO)


    // console.log(uniqueList)


    // console.log(list)
    res.send(list)
})

//////////////////////////////DEBUG////////////////////////////////
router.post("/submitTest", async (req, res) => {
    //insert all store
    state = req.body
    // console.log(state)

    const index = state.savedMOValues.indexOf(state.MOValue)
    if (index > -1) {
        state.savedMOValues.splice(index, 1)
    }
    state = {
        savedMOValues: state.savedMOValues,
        MOValue: "",
        scanText: "",
        storedValidMat: [],
        row: 0,
    };

    res.send(state);

});
router.post("/setMOValue", async (req, res) => {
    //    console.log(req.body.data)
    state.MOValue = req.body.data
    res.send(state)
})

router.get("/updateTargetUsage", async (req, res) => {//update target usage and actual usage depends on DO
    let fetchDo = await models.MO.findAll({
        raw: true,
        include: [{
            model: models.DO,
            required: true,
            raw: true,
            include: [{
                model: models.PartList,
                required: true,
                raw: true,
                include: [{
                    model: models.PartListUsage,
                    required: true,
                    raw: true,
                    include: [{
                        model: models.ChildPartList,
                        required: true,
                        raw: true,
                    }]
                }]
            }]
        }]
    });

    //insert actual usage first 
    //actual usage, actual per box will be 0 
    //and status will be 1 at first then we will update later
    let targetUsageInsertList = []

    for (let i = 0; i < fetchDo.length; i++) {

        await models.ActualUsage.create({
            ActualUsage: 0,
            ActualPerBox: 0,
            UsageStatusId: 1,
        }).then(async (actual) => {
            let sum = 0
            let findDO = await models.DO.findAll({
                where: { MOId: fetchDo[i]["MOId"] },
                raw: true,
            })
            for (let j = 0; j < findDO.length; j++) {
                sum += findDO[j]["TotalQty"]
            }

            let targetUsage = sum * fetchDo[i]['DOs.PartList.PartListUsages.Usage'] + Math.ceil(fetchDo[i]['DOs.PartList.PartListUsages.ChildPartList.MaterialYieldPercent'] * 0.01 * fetchDo[i]['DOs.PartList.PartListUsages.ChildPartList.UsagePerBox'])

            //insert target usage
            let actualUsageNum = i
            actualUsageNum++
            targetUsageInsertList.push({
                MOId: fetchDo[i]["MOId"],
                PartListUsageId: fetchDo[i]['DOs.PartList.PartListUsages.PartListUsageId'],
                TargetUsage: targetUsage/*find a way to get target usage here 1 is dummy*/,
                TargetPerBox: Math.ceil(targetUsage / fetchDo[i]['DOs.PartList.PartListUsages.ChildPartList.UsagePerBox']),
                ActualUsageId: actual.ActualUsageId/*get actual usage but before getting it insert actual first*/
            })
        })
    }
    targetUsageInsertList.sort(function (a, b) {
        return a.MOId - b.MOId;
    })
    await models.TargetUsage.bulkCreate(targetUsageInsertList)
    res.send("OK")

});

router.get("/fetchDOTest", async (req, res) => {
    let fetchDo = await models.MO.findAll({//ตัดยอด
        where: { MOId: 1 },//change MOId later
        raw: true,
        include: [{
            model: models.DO,
            required: true,
            raw: true,
            include: [{
                model: models.PartList,
                required: true,
                raw: true,
                include: [{
                    model: models.PartListUsage,
                    required: true,
                    raw: true,
                    include: [{
                        model: models.ChildPartList,
                        required: true,
                        raw: true,
                        include: [{
                            model: models.ProductionData,
                            required: true,
                            raw: true,
                            include: [{
                                model: models.Machine,
                                required: true,
                                raw: true
                            }]
                        }]
                    }]
                }]
            }]
        }]
    });
    // console.log(fetchDo)
    let uniqueFetchDo = []
    let uniqueFetchDoArr = []
    for (let i = 0; i < fetchDo.length; i++) {

        if (!uniqueFetchDo.includes(fetchDo[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartNo'])) {
            uniqueFetchDo.push(fetchDo[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartNo'])
            // console.log(fetchDo[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartNo'])
            uniqueFetchDoArr.push(fetchDo[i])
        }
    }
    // console.log(uniqueFetchDoArr)
    for (let i = 0; i < uniqueFetchDoArr.length; i++) {
        // console.log(uniqueFetchDoArr[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartNo'] + " " + uniqueFetchDoArr[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartListId'])
        let findQtyLeft = await models.QtyLeft.findOne({
            where: [{ ChildPartListId: uniqueFetchDoArr[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartListId'] }, { MOId: 1 }],//change MOId later
            order: [['createdAt', 'DESC']],
            raw: true
        })

        // console.log(findQtyLeft)//use findQtyLeft to evaluateResult
        let getTotalQtyFromDO = await models.DO.findAll({
            where: { MOId: uniqueFetchDoArr[i]['MOId'] },
            raw: true
        })
        let totalQtyFromDO = 0
        for (let j = 0; j < getTotalQtyFromDO.length; j++) {
            totalQtyFromDO += getTotalQtyFromDO[j]["TotalQty"]
        }
        console.log(totalQtyFromDO)
        let findPartListUsage = await models.PartListUsage.findOne({
            where: { ChildPartListId: uniqueFetchDoArr[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartListId'] },
            raw: true
        })
        let usage = findPartListUsage["Usage"]

        let targetUsage = totalQtyFromDO * usage + Math.ceil(uniqueFetchDoArr[i]['DOs.PartList.PartListUsages.ChildPartList.UsagePerBox'] * 0.01 * uniqueFetchDoArr[i]['DOs.PartList.PartListUsages.ChildPartList.MaterialYieldPercent'])

        await models.QtyLeft.create({
            MOId: 1,//change MOId later
            QtyLeft: findQtyLeft["QtyLeft"] - targetUsage,
            ChildPartListId: uniqueFetchDoArr[i]['DOs.PartList.PartListUsages.ChildPartList.ChildPartListId'],
        })
    }

    // console.log(uniqueFetchDoArr)
    console.log(fetchDo.length)
    res.send(uniqueFetchDoArr)
})

router.get("/evaluateResult", async (req, res) => {
    let find
})

router.post("/fetchDOSingle", async (req, res) => {

    let foundPO = req.body['scannedPONo']

    let scannedPONo = foundPO;

    let findPO = await models.PO.findOne({
        where: { PONo: scannedPONo },
        raw: true
    })

    let getDOId = await models.DO.findOne({
        where: { DOId: findPO["DOId"] },
        raw: true
    })

    let findChildPartListId = await models.ProductionData.findAll({
        where: [{ POId: findPO["POId"] }],
        order: [['createdAt', 'DESC']],
        raw: true
    })
    let getUniqueChildPartLists = []
    let getUniqueChildPartListsArr = []
    for (let i = 0; i < findChildPartListId.length; i++) {

        if (!getUniqueChildPartLists.includes(findChildPartListId[i]["ChildPartListId"])) {
            getUniqueChildPartLists.push(findChildPartListId[i]["ChildPartListId"])
            getUniqueChildPartListsArr.push(findChildPartListId[i])
        }
    }

    for (let i = 0; i < getUniqueChildPartListsArr.length; i++) {
        let findQtyLeft = await models.QtyLeft.findOne({
            where: [{ MOId: getDOId["MOId"] }, { ChildPartListId: getUniqueChildPartListsArr[i]["ChildPartListId"] }],
            order: [['createdAt', 'DESC']],
            raw: true
        })

        let findPartListUsageUsage = await models.PartListUsage.findOne({
            where: { ChildPartListId: findQtyLeft["ChildPartListId"] },
            raw: true
        })

        await models.QtyLeft.create({
            MOId: getDOId["MOId"],//change MOId later
            QtyLeft: findQtyLeft["QtyLeft"] - (getDOId["Balance"] * findPartListUsageUsage["Usage"]),
            ChildPartListId: getUniqueChildPartListsArr[i]["ChildPartListId"],
        })

    }

    res.send("OK")
})
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


module.exports = router;
