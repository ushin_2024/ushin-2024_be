const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.get("/getRole", async (req, res) => {
    let data = await models.EmployeeRole.findAll({ raw: true });
    console.log(data);
    res.send(data);
});

router.get("/getEmployee", async (req, res) => {
    let data = await models.Employee.findAll({
        raw: true,
        include: [
            {
                model: models.EmployeeRole,
                required: true,
                raw: true,
            },
        ],
    });
    console.log(data);
    res.send(data);
});

router.get("/ChildPartList", async (req, res) => {
    let data = await models.ChildPartList.findAll({ raw: true });
    console.log(data);
    res.send(data);
}); 

router.get("/getPartList", async (req, res) => {
    try {
        let data = await models.PartList.findAll({ raw: true })
        console.log(data)
        res.send(data);
    } catch (error) {
        res.send(error)
    }

})

router.get("/getMasterPOStatus", async (req, res) => {
    try {
        let data = await models.MasterPOStatus.findAll({ raw: true })
        console.log(data)
        res.send(data);
    } catch (error) {
        res.send(error)
    }

})
router.get("/getMasterMachine", async (req, res) => {
    try {
        let data = await models.Machine.findAll({ raw: true })
        console.log(data)
        res.send(data);
    } catch (error) {
        res.send(error)
    }
})

module.exports = router;