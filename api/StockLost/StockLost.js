const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.post("/inputReasonForStockLost", async (req, res) => {

    let partNoPattern = /\w{0,6}-\D{3}|\w{0,6}-(.+)-\D{3}/g;
    const tag = req.body.scannedMat;
    const arr = [...tag.matchAll(partNoPattern)];

    let partNo = "";
    try {
        partNo = arr[0][0];
    } catch (error) { }

    let lotNo = "";
    let qty;
    let polotlabel = "";

    let qtyPatternUshinJP = /\d{20}\s\d+\s\d+/g;
    let check = [...tag.matchAll(qtyPatternUshinJP)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is now ushinJP
        let deleteLines = [...data.matchAll(/\d{12}\s/g)];
        let deletedLine = String(deleteLines[0][0]).trim();
        let subqty = deletedLine.substring(0, 7);
        let pureQty = subqty.replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        lotNo = poNo;
        polotlabel = lotNo;
    } catch (error) {
        //console.log(check);
    }
    let qtyWebEdiPattern = /(\w\d-)\d{17}|\w+\d\s\s\d{15}/g;
    check = [...tag.matchAll(qtyWebEdiPattern)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is web edi
        let pureQty = data.substring(5, 12).replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        polotlabel = poNo;
    } catch (error) {
    }
    let qtyPatternReprint = /(\w\d-)\d{21}|\s\s[A-Za-z]\d{21}/g;
    check = [...tag.matchAll(qtyPatternReprint)];
    try {
        let data = String(check[0][0]);
        //tag type is now reprint
        let subqty = data.substring(5, 12);
        console.log(subqty);
        let pureQty = subqty.replace(/^0+/, "");
        let lotNoPattern = /\s\s\s\w+\s/g;
        let match = [...tag.matchAll(lotNoPattern)];
        let lotNo = match[0][0].trim().substring(4, 15);
        qty = pureQty;
        polotlabel = lotNo;
    } catch (error) {
    }
    let qtyProductionInHousePattern = /\s{9}\w{18}/g;
    check = [...tag.matchAll(qtyProductionInHousePattern)];
    try {
        let data = String(check[0][0]).trim();
        let subqty = data.substring(3, 10).replace(/^0+/, "");
        let labelNo = data.substring(10, 17);
        qty = subqty;
        polotlabel = labelNo;
    } catch (error) { }

    console.log(partNo + " " + lotNo)
    try {
        let getChild = await models.ChildPartList.findOne({
            where: { ChildPartNo: partNo },
            raw: true
        })
        let findReceive = await models.ReceiveMaterialPart.findOne({
            where: [{ ChildPartListId: getChild["ChildPartListId"] }, { ChildPartLot: lotNo }],

        })
        let getLatestQtyLeft = await models.QtyLeft.findOne({
            where: { ChildPartListId: getChild["ChildPartListId"] },
            order: [['createdAt', 'DESC']],
        })

        let getLatestQtyLeftVal = JSON.parse(JSON.stringify(getLatestQtyLeft))

        await models.QtyLeft.create({
            QtyLeft: getLatestQtyLeftVal["QtyLeft"] - qty,
            ChildPartListId: getChild["ChildPartListId"]
        })

        await getLatestQtyLeft.destroy()

        let findReceiveVal = JSON.parse(JSON.stringify(findReceive))
        let findMOInLog = await models.MO_Log.findOne({
            where: { MOId: findReceiveVal["MOId"] },
        })
        if (findMOInLog !== null)
            await findMOInLog.destroy()
        if (findReceive !== null)
            await findReceive.destroy()

        await models.StockLostReason.create({
            ChildPartNo: partNo,
            ChildPartLot: lotNo,
            Reason: req.body.reason,
        })


    } catch (e) {
        console.log(e)
        res.send("Not OK")
        return
    }

    res.send("OK")
})


module.exports = router;