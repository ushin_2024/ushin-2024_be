const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

var postate = {
  DO: "",
  EMP: "",
  MC: "",
  scanMat: "",
  row: 0,
  storedMachine: [],
  storedMat: [],
};

router.get("/getPoState", async (req, res) => {
  res.send(postate);
});

router.post("/addMat", async (req, res) => {
  let stateFromFrontend = req.body;
  postate = stateFromFrontend;
  let scannedMat = stateFromFrontend.scanMat;
  let childPartNoPattern = /\w{0,6}-\D{3}|\w{0,6}-(.+)-\D{3}/g;
  const tag = scannedMat;
  const arr = [...tag.matchAll(childPartNoPattern)];

  let childPartNo = "";
  try {
    childPartNo = arr[0][0];
    postate.storedMat.push(childPartNo);
  } catch (error) {
    let currentState = postate;
    currentState["status"] = "Qrcode is invalid";
    res.send(postate);
    return;
  }

  res.send(postate);
});

router.post("/addMachine", async (req, res) => {
  let stateFromFrontend = req.body;
  postate = stateFromFrontend;
  let scannedMC = stateFromFrontend.MC;

  postate.storedMachine.push(scannedMC);

  res.send(postate);
});

router.post("/insertAllPOPage", async (req, res) => {
  // CheckMatchingDO()
  let stateFromFrontend = req.body;
  postate = stateFromFrontend;
  let checkMatchingDOPO = CheckMatchingDOPO(stateFromFrontend.DO);

  if (checkMatchingDOPO === "INVALID_SCAN_DO_PO") {
    ClearData();
    let currentState = postate;
    currentState["status"] = "INVALID_SCAN_DO_PO";
    res.send(currentState);
    return;
  }

  if (postate.storedMachine.length === 0 || postate.storedMat.length === 0) {
    ClearData();
    let currentState = postate;
    currentState["status"] = "NO_MACHINE_OR_MAT_IS_SELECTED";
    res.send(currentState);
    return;
  }

  let childPart = [];
  let container = [];
  let getAllPO
  try {
    for (let i = 0; i < postate.storedMachine.length; i++) {
      console.log(postate.storedMachine[i])
      let getMachine = await models.Machine.findOne({
        where: { MachineName: postate.storedMachine[i] },
        raw: true,
      });
      if (getMachine === null) {
        res.send("CannotFindMachine")
        return
      }
      let foundMachineId = getMachine["MachineId"];
      let foundMachine = getMachine["Machine"];
      let foundMachineType = getMachine["MachineType"];

      let POStatusName = foundMachine + "-" + foundMachineType;

      let findMachinePOStatusWithMachineName =
        await models.MasterPOStatus.findOne({
          where: { POStatus: POStatusName + "_open" },
          raw: true,
        });

      let getPO = await models.PO.findOne({
        where: { PONo: checkMatchingDOPO["PONo"] },
        raw: true,
      });

      let getDO = await models.DO.findOne({
        where: { DOId: getPO["DOId"] },
        raw: true,
      });

      getAllPO = await models.PO.findAll({
        where: { DOId: getDO["DOId"] },
        raw: true
      });

      let foundMasterPOStatusMasterPOStatusId =
        findMachinePOStatusWithMachineName["MasterPOStatusId"];

      let dupCheck = [];

      for (let j = 0; j < postate.storedMat.length; j++) {

        let findChildPart = await models.ChildPartList.findAll({
          where: { ChildPartNo: postate.storedMat[j] },
          raw: true,
        });

        let findPartListUsage = await models.PartListUsage.findAll({
          where: { PartNoId: getDO["PartNoId"] },
          include: [{
            model: models.ChildPartList,
            required: true
          }],
          raw: true,
        });
        // console.log(findPartListUsage)

        //checking if scanned mat childpartlistid matches partlistusage childpartlistid


        for (let k = 0; k < findChildPart.length; k++) {
          for (let l = 0; l < findPartListUsage.length; l++) {
            if (findChildPart[i]["ChildPartNo"] === findPartListUsage[l]['ChildPartList.ChildPartNo']) {
              for (let m = 0; m < getAllPO.length; m++) {
                if (!dupCheck.includes(getAllPO[m]["POId"] + "," + findPartListUsage[l]['ChildPartList.ChildPartListId'])) {
                  container.push({
                    POId: getAllPO[m]["POId"],
                    ChildPartListId: findPartListUsage[l]['ChildPartList.ChildPartListId'],
                    MachineId: foundMachineId,
                    EmployeeId: postate.EMP,
                  });
                  dupCheck.push(getAllPO[m]["POId"] + "," + findPartListUsage[l]['ChildPartList.ChildPartListId'])
                }

              }
            }
          }
        }
      }

      for (let j = 0; j < getAllPO.length; j++) {
        await models.POStatus.create({
          MasterPOStatusId: foundMasterPOStatusMasterPOStatusId,
          POId: getAllPO[j]["POId"],
          MfgDate: new Date(Date.now()).toISOString(),
        });
      }
    }
  } catch (e) {
    console.log(e);
  }

  let productionData = await models.ProductionData.findAll({ raw: true })

  console.log(container)
  console.log(postate.storedMat)
  console.log(getAllPO)

  let insertFromFront = []

  let sameCount = 0

  for (let i = 0; i < postate.storedMat.length; i++) {

    for (let j = 0; j < container.length; j++) {

      let getSimiliarChild = await models.ChildPartList.findOne({
        where: { ChildPartListId: container[j]["ChildPartListId"] },
        raw: true
      })

      for (let k = 0; k < getAllPO.length; k++) {
        if (getAllPO[k]["POId"] === container[j]["POId"]) {
          sameCount++
          k = getAllPO.length
        }

      }
      if (sameCount === getAllPO.length) {

        if (getSimiliarChild["ChildPartNo"] === postate.storedMat[i]) {
          let contain = container[j]
          contain["ChildPartNo"] = getSimiliarChild["ChildPartNo"]
          insertFromFront.push(contain)
          console.log("work")
        }
        sameCount = 0
      }



    }

  }

  // console.log("test")
  let insertList = []

  let sumCount = 0

  insertFromFront.sort(function (a, b) {
    return a.ChildPartListId - b.ChildPartListId;
  });

  // console.log(insertFromFront)
  let uniqueChild = []
  let uniqueChildArr = []
  for (let i = 0; i < insertFromFront.length; i++) {
    if (!uniqueChild.includes(insertFromFront[i]["ChildPartListId"])) {
      uniqueChild.push(insertFromFront[i]["ChildPartListId"])
      uniqueChildArr.push(insertFromFront[i])
    }
  }

  for (let i = 0; i < uniqueChildArr.length; i++) {
    let sum = 0
    for (let j = 0; j < insertFromFront.length; j++) {
      if (uniqueChildArr[i]["ChildPartListId"] === insertFromFront[j]["ChildPartListId"]) {
        sum++
      }
    }
    uniqueChildArr[i]["Sum"] = sum
  }

  uniqueChildArr.sort(function (a, b) {
    return a.ChildPartNo.localeCompare(b.ChildPartNo)
  });

  // filter data 
  let uniqueChildNo = []

  for (let i = 0; i < uniqueChildArr.length; i++) {
    if (!uniqueChildNo.includes(uniqueChildArr[i]["ChildPartNo"])) {
      uniqueChildNo.push(uniqueChildArr[i]["ChildPartNo"])
    }
  }
  console.log(uniqueChildArr)

  let filteredArr = []
  for (let i = 0; i < uniqueChildNo.length; i++) {
    let sameCount = 0
    for (let j = 0; j < uniqueChildArr.length; j++) {
      let getProduction = await models.ProductionData.findOne({
        where: { POId: uniqueChildArr[j]["POId"], ChildPartListId: uniqueChildArr[j]["ChildPartListId"] },
        raw: true
      })
      if (uniqueChildNo[i] === uniqueChildArr[j]["ChildPartNo"]) {
        if (sameCount < uniqueChildArr[j]["Sum"] && getProduction === null) {
          filteredArr.push(uniqueChildArr[j])
          sameCount++
        }
      }
    }
  }
  console.log(filteredArr)

  let connectPOData = []
  for (let i = 0; i < filteredArr.length; i++) {
    for (let j = 0; j < getAllPO.length; j++) {
      connectPOData.push({
        POId: getAllPO[j]["POId"],
        ChildPartListId: filteredArr[i]["ChildPartListId"],
        MachineId: filteredArr[i]["MachineId"],
        EmployeeId: filteredArr[i]["EmployeeId"],
      })
    }
  }
  console.log(connectPOData)

  await models.ProductionData.bulkCreate(connectPOData);

  ClearData();
  res.send(postate);
});

router.get("/clearDataPOPage", async (req, res) => {
  ClearData();
  res.send(postate);
});

router.get("/deletePOLastScannedMaterial", async (req, res) => {
  let tempList = [];
  if (postate.storedMat.length !== 0) {
    for (let i = 0; i < postate.storedMat.length - 1; i++) {
      tempList.push(postate.storedMat[i]);
    }
  }
  postate.storedMat = tempList;
  res.send(postate);
});

function ClearData() {
  postate = {
    DO: "",
    EMP: "",
    MC: "",
    scanMat: "",
    row: 0,
    storedMachine: [],
    storedMat: [],
  };
}

function CheckMatchingDOPO(scannedDOPO) {
  // console.log(split)
  if (scannedDOPO.length > 112 || scannedDOPO.length === 0) {
    return "INVALID_SCAN_DO_PO";
  }

  let DOPO;
  try {
    let DO_PATTERN = /N\w+-\w+-\w+|N\w+-\w+|N\w+/g;
    const tag = scannedDOPO;
    const arr = [...tag.matchAll(DO_PATTERN)];
    let DO = arr[0][0]
    let PO_PATTERN = /D.*/g;
    const arrPO = [...tag.matchAll(PO_PATTERN)];
    let PO = arrPO[0][0]
    DOPO = { DONo: String(DO).trim(), PONo: String(PO).trim() }
  } catch (e) {
    console.log(e);
    return "INVALID_SCAN_DO_PO";
  }
  return DOPO;
}

module.exports = router;
