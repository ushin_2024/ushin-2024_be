const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.post("/saveEmployee", async (req, res) => {
    // console.log(req.body.data)
    let machineCheck = await models.Machine.findOne({
        where: { MachineName: req.body.data.scannedMC },
        raw: true
    })
    if (machineCheck === null){
        res.send("MACHINE_NOT_FOUND_IN_DATABASE")
        return
    }

    let employeeCheck = await models.Employee.findOne({
        where: { EmployeeId: req.body.data.scannedEmp },
        raw: true
    })
    if(employeeCheck === null){
        res.send("EMPLOYEE_NOT_FOUND_IN_DATABASE")
        return
    }

    await models.EmployeeLineMatch.create(
        {
            EmployeeId: req.body.data.scannedEmp,
            Machine: req.body.data.scannedMC
        }
    )

    res.send("OK")
})

module.exports = router;