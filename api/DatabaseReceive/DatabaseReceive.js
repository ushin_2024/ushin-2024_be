const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.post("/receiveOpucas", async (req, res) => {
    // console.log(req.body);

    try {
        // for (let i = 0; i < req.body.length; i++) {//check if PO is duplicated
        //     let findPO = await models.PO.findOne({
        //         where: { PONo: req.body[i]["PO_NO"] },
        //         raw: true,
        //     });
        //     // console.log(findPO)
        //     if (findPO !== null) {
        //         res.send("Duplicated Data Found");
        //         return;
        //     }
        // }

        let DO = [];
        let PO = [];
        let MO = [];
        let uniqueMO = [];
        let uniquePartList = [];
        //let partList = []
        //insert MO first
        for (let i = 0; i < req.body.length; i++) {
            let mo =
                String(req.body[i]["P_Name"]).replaceAll(" ", "_") +
                "_" +
                String(req.body[i]["Due_Date"]).replace("/20", "").replaceAll("/", "");
            if (!uniqueMO.includes(mo)) {
                uniqueMO.push(mo);
                MO.push({ MONo: mo });
            }

            //find partlist if exist do nothing if not insert new partlist
            let partList = req.body[i]["Part_Number"];
            if (!uniquePartList.includes(partList)) {
                uniquePartList.push(partList);
            }
        }
        let nonExistingMO = [];
        //check each MO is exist if not don't push into new array
        for (let i = 0; i < MO.length; i++) {
            let findMO = await models.MO.findOne({
                where: { MONo: MO[i]["MONo"] },
                raw: true,
            });
            if (findMO === null) {
                nonExistingMO.push({ MONo: MO[i]["MONo"] });
            }
        }

        await models.MO.bulkCreate(nonExistingMO);

        //find and insert partlist if not exist
        for (let i = 0; i < uniquePartList.length; i++) {
            let findPartList = await models.PartList.findOne({
                where: { PartNo: uniquePartList[i] },
                raw: true,
            });
            if (findPartList === null) {
                await models.PartList.create({
                    PartNo: uniquePartList[i],
                });
            }
        }

        for (let i = 0; i < req.body.length; i++) {
            let mo =
                String(req.body[i]["P_Name"]).replaceAll(" ", "_").toUpperCase() + "_"
                + String(req.body[i]["Due_Date"]).replace("/20", "").replaceAll("/", "");
            let findMO = await models.MO.findOne({
                where: { MONo: mo },
                raw: true,
            });

            let findPartNoId = await models.PartList.findOne({
                where: { PartNo: req.body[i]["Part_Number"] },
                raw: true,
            });

            if (findPartNoId && findMO) {
                DO.push({
                    DONo: req.body[i]["DO_Number"],
                    Supp: req.body[i]["Supp"],
                    SuppName: req.body[i]["Supp_Name"],
                    CMSP: parseInt(req.body[i]["CMSP"]),
                    Prcs: parseInt(req.body[i]["Prcs"]),
                    PartName: req.body[i]["P_Name"],
                    DueDate: tools.formatDate(req.body[i]["Due_Date"]),
                    DueQty: req.body[i]["Due_Qty"],
                    PrdtQty: req.body[i]["Prdt_Qty"],
                    Balance: req.body[i]["Due_Qty"],
                    Unit: req.body[i]["Unit"],
                    ODDays: req.body[i]["OD_Days"],
                    TotalCard: req.body[i]["Total"],
                    TotalQty: req.body[i]["Due_Qty"] * req.body[i]["Total"],
                    DeliveryDueDate: tools.formatDate(req.body[i]["Delivery_Due"]),
                    CustomersPN: req.body[i]["Customer_PN"],
                    PartNoId: findPartNoId["PartNoId"],
                    DOStatusId: 1,
                    MOId: findMO["MOId"],
                });
            } else {
                console.log("PartNoId not found");
                DO = [];
                break;
            }
        }

        let uniqueDO = [];
        let uniqueDOArr = [];
        for (let i = 0; i < DO.length; i++) {
            if (!uniqueDO.includes(DO[i]["DONo"])) {
                uniqueDO.push(DO[i]["DONo"]);
                uniqueDOArr.push(DO[i]);
            }
        }

        //check if DO is already existed
        let nonExistedDO = []
        for (let i = 0; i < uniqueDOArr.length; i++) {
            let findExistedDO = await models.DO.findOne({
                where: { DONo: uniqueDOArr[i]["DONo"] },
                raw: true
            })
            if (findExistedDO === null) {
                nonExistedDO.push(uniqueDOArr[i])
            }
        }

        await models.DO.bulkCreate(nonExistedDO);

        for (let i = 0; i < req.body.length; i++) {
            let findDO = await models.DO.findOne({
                where: { DONo: req.body[i]["DO_Number"] },
                raw: true,
            });
            if (findDO) {
                PO.push({
                    PONo: req.body[i]["PO_NO"],
                    CardNo: req.body[i]["CardNo"],
                    DOId: findDO["DOId"],
                });
            }
        }
        //find existed PO if not insert if it is do nothing.
        let nonExistedPO = []
        for (let i = 0; i < PO.length; i++) {
            let findExistedPO = await models.PO.findOne({
                where: { PONo: PO[i]["PONo"] },
                raw: true
            })
            if (findExistedPO === null) {
                nonExistedPO.push(PO[i])
            }
        }
        await models.PO.bulkCreate(nonExistedPO);

        // console.log(MO)
        res.send("OK");
    } catch (e) {
        console.log(e);
        res.send("Error");
    }
});

router.post("/receiveBOM", async (req, res) => {
    console.log(req.body)
    for (let i = 0; i < req.body.length; i++) {
        let findPartNo = await models.PartList.findOne({
            where: { PartNo: req.body[i]["Part Number"] },
            raw: true
        })
        if (findPartNo === null) {
            await models.PartList.create({
                PartNo: req.body[i]["Part Number"]
            })
        }
    }


    for (let i = 0; i < req.body.length; i++) {

        await models.ChildPartList.create({
            ChildPartNo: req.body[i]["Child Part Number"],
            ChildPartName: req.body[i]["Child Part Name"],
            ProcessName: req.body[i]["Process Name"],
            UsagePerBox: req.body[i]["Q'ty/Box"],
            MaterialYieldPercent: 1,
        }).then(async (childPart) => {
            let findPartList = await models.PartList.findOne({
                where: { PartNo: req.body[i]["Part Number"] },
                raw: true
            })

            await models.PartListUsage.create({
                PartNoId: findPartList["PartNoId"],
                ChildPartListId: childPart.dataValues.ChildPartListId,
                Usage: req.body[i]["Qty."]
            })

        })

    }



    res.send("OK")
})
//do after populate mo,po,do and partlist usage (receiveopucas and bom)
router.get("/populatePartListRealUsage", async (req, res) => {
    let findAllDO = await models.DO.findAll({
        raw: true
    })
    
    for (let i = 0; i < findAllDO.length; i++) {
        let findPartListUsage = await models.PartListUsage.findAll({
            where: { PartNoId: findAllDO[i]["PartNoId"] },
            raw: true
        })
        for (let j = 0; j < findPartListUsage.length; j++) {
            let order = 0
            let getChild = await models.ChildPartList.findOne({
                where: { ChildPartListId: findPartListUsage[j]["ChildPartListId"] },
                raw: true
            })
            
            await models.PartListRealUsage.create({
                PartNoId: findPartListUsage[j]["PartNoId"],
                ChildPartListId: findPartListUsage[j]["ChildPartListId"],
                Usage: findPartListUsage[j]["Usage"],
                DOId: findAllDO[i]["DOId"],
                Order: ++order
            })
    
            let totalChildAppend = Math.max(Math.ceil(findAllDO[i]["TotalQty"] / getChild["UsagePerBox"]) - 1, 0)
       
            for (let k = 0; k < totalChildAppend; k++) {
                await models.PartListRealUsage.create({
                    PartNoId: findPartListUsage[j]["PartNoId"],
                    ChildPartListId: findPartListUsage[j]["ChildPartListId"],
                    Usage: findPartListUsage[j]["Usage"],
                    DOId: findAllDO[i]["DOId"],
                    Order: ++order
                })
            }

        }

    }
    res.send("OK")

})


module.exports = router;
