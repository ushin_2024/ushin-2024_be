const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

var state = {
    savedMOValues: [],
    MOValue: "",
    scannedMO: "",
    scanText: "",
    storedValidMat: [],
    row: 0,
    DO: "",
    EMP: "",
    MC: "",
    status: "",
    BOM_LEFTOVER: [],
    storedMachine: [],
    selectedMOPO: "",
    storedMachineGuide: [],
    savedDO: ""
};

var sumReceiveMaterialPartList = [];

router.get("/getMatchingIdStateShopperStore", async (req, res) => {
    res.send(state);
});


router.post("/checkMatchingIdShopperStore", async (req, res) => {
    state = req.body;

    let partNoPattern = /\w{0,6}-\D{3}|\w{0,6}-(.+)-\D{3}|N.+-\w+|\s.+-\w+[A-Z,a-z]\s\s\s|\s\w.+-.{3}\s{3}/g;
    const tag = req.body.scanText;
    const arr = [...tag.matchAll(partNoPattern)];

    let partNo = "";
    try {
        partNo = arr[0][0];
    } catch (error) { }

    let lotNo = "";
    let qty;
    let polotlabel = "";

    let qtyPatternUshinJP = /\d{20}\s\d+\s\d+/g;
    let check = [...tag.matchAll(qtyPatternUshinJP)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is now ushinJP
        let deleteLines = [...data.matchAll(/\d{12}\s/g)];
        let deletedLine = String(deleteLines[0][0]).trim();
        let subqty = deletedLine.substring(0, 7);
        let pureQty = subqty.replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        lotNo = poNo;
        polotlabel = lotNo;
    } catch (error) {
        //console.log(check);
    }
    let qtyWebEdiPattern = /(\w\d-)\d{17}|\w+\d\s\s\d{15}/g;
    check = [...tag.matchAll(qtyWebEdiPattern)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is web edi
        let pureQty = data.substring(5, 12).replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        polotlabel = poNo;
    } catch (error) {
    }
    let qtyPatternReprint = /(\w\d-)\d{21}|\s\s[A-Za-z]\d{21}/g;
    check = [...tag.matchAll(qtyPatternReprint)];
    try {
        let data = String(check[0][0]);
        //tag type is now reprint
        let subqty = data.substring(5, 12);
        console.log(subqty);
        let pureQty = subqty.replace(/^0+/, "");
        let lotNoPattern = /\s\s\s\w+\s/g;
        let match = [...tag.matchAll(lotNoPattern)];
        let lotNo = match[0][0].trim().substring(4, 15);
        qty = pureQty;
        polotlabel = lotNo;
    } catch (error) {
    }
    let qtyProductionInHousePattern = /\s{9}\w{18}/g;
    check = [...tag.matchAll(qtyProductionInHousePattern)];
    try {
        let data = String(check[0][0]).trim();
        let subqty = data.substring(3, 10).replace(/^0+/, "");
        let labelNo = data.substring(10, 17);
        qty = subqty;
        polotlabel = labelNo;
    } catch (error) { }

    let qtyUshinNewPattern = /\s\d{15}\s+\w+\s/g
    check = [...tag.matchAll(qtyUshinNewPattern)];
    try {
        let data = String(check[0][0]).trim();
        let subqty = data.substring(8, 15).replace(/^0+/, "");
        let labelNo = data.substring(20, 28);
        qty = subqty;
        polotlabel = labelNo;
    } catch (e) { }
    let qtyUshinNewerPattern = /\s\d+\w\d+\s+\w+\s{8}.+/g
    check = [...tag.matchAll(qtyUshinNewerPattern)];
    try {
        let data = String(check[0][0]).trim();
        let subqty = data.substring(33, 40).replace(/^0+/, "");
        let labelNo = data.substring(4, 15);
        qty = subqty;
        polotlabel = labelNo;
    } catch (e) { }


    if (qty === '')
        qty = 0

    try {

        let getChildPartListId = await models.ChildPartList.findAll({
            where: { ChildPartNo: partNo.trim() },
            raw: true
        });

        let childPartListId = getChildPartListId[0]["ChildPartListId"];

        let mo = await models.MO.findAll({
            where: { MONo: state.MOValue },
            raw: true
        });


        let getAllDO = await models.DO.findAll({
            where: { MOId: mo[0]["MOId"] },
            raw: true
        });


        let allDO = getAllDO;
        for (let i = 0; i < allDO.length; i++) {
            sumReceiveMaterialPartList.push({
                MOId: mo[0]["MOId"],
                ChildPartListId: childPartListId,
                ChildPartLot: polotlabel,
                row: state.row
            });
        }

    } catch (error) {
        console.log(error)
        state.scanText = "";
        res.send(state);
        return;
    }


    state.row++;
    state.storedValidMat.push({
        no: state.row,
        partNo: partNo,
        polotlabel: polotlabel,
        qty: qty

    });
    state.scanText = "";

    res.send(state);
});


router.get("/clearDataShopperStore", async (req, res) => {
    sumReceiveMaterialPartList = [];
    ClearReceiveData()
    res.send(state);
});

function ClearReceiveData() {
    sumReceiveMaterialPartList = [];
    state.savedMOValues = []
    state.MOValue = ""
    state.scanText = ""
    state.storedValidMat = []
    state.row = 0
}

router.post("/populateMOSplitShopperStore", async (req, res) => {

    try {
        const splitArr = String(req.body.data).split(",")

        let moNotUsedList = []

        for (let i = 0; i < splitArr.length; i++) {
            let findUsedMO = await models.MO_Log.findOne({
                where: { MONo: String(splitArr[i]).trim() },
                raw: true
            })
            if (findUsedMO === null) {

                moNotUsedList.push(String(splitArr[i]).trim())

            }
        }

        state.savedMOValues = moNotUsedList

    } catch (e) {
        console.log(e)
    }
    res.send(state)

})

router.post("/submitAllDataNewShopperStore", async (req, res) => {

    try {

        let findMO = await models.MO.findOne({
            where: { MONo: state.MOValue },
            raw: true
        })

        let findDO = await models.DO.findAll({
            where: { MOId: findMO["MOId"] },
            raw: true
        })



        for (let i = 0; i < findDO.length; i++) {
            let getPartListUsage = await models.PartListRealUsage.findAll({
                where: { PartNoId: findDO[i]["PartNoId"], DOId: findDO[i]["DOId"] },
                raw: true
            })
            if (getPartListUsage.length === 0) {
                ClearReceiveData()
                state.status = "PartListUsageNotFoundInDatabase"
                // console.log(state)
                res.send(state);
                return
            }
        }

        let totalQtySum = 0
        for (let i = 0; i < findDO.length; i++) {
            totalQtySum += findDO[i]["TotalQty"]
        }
        // console.log(totalQtySum)

        let uniqueChildPartNo = []
        for (let i = 0; i < state.storedValidMat.length; i++) {
            if (!uniqueChildPartNo.includes(state.storedValidMat[i]["partNo"])) {
                uniqueChildPartNo.push(state.storedValidMat[i]["partNo"].trim())
            }
        }

        let sumChildPartQty = []//sum of each child part
        for (let i = 0; i < uniqueChildPartNo.length; i++) {
            let sum = 0
            for (let j = 0; j < state.storedValidMat.length; j++) {
                if (state.storedValidMat[j]["partNo"] === uniqueChildPartNo[i]) {
                    // console.log(state.storedValidMat[j]["qty"] + " " + state.storedValidMat[j])
                    sum += parseInt(state.storedValidMat[j]["qty"])
                }
            }
            sumChildPartQty.push({ partNo: uniqueChildPartNo[i], sumQty: sum })
        }

        let storedModelQtyLeft = []
        let storedReceiveMaterialPart = []
        let storedModelQtyCheck = []
        let stockNotEnough = false
        let reason = ""
        let allDO = []
        for (let i = 0; i < findDO.length; i++) {
            allDO.push(findDO[i]["DOId"])
        }
        let allPartListRealUsage = [];
        for (let i = 0; i < allDO.length; i++) {
            let getAllPartListUsage = await models.PartListRealUsage.findAll({
                where: { DOId: allDO[i] },
                include: [{ model: models.ChildPartList }],
                raw: true
            })
            allPartListRealUsage.push(getAllPartListUsage)
        }
        // console.log(allPartListRealUsage)
        //concat
        let concatArr = []
        for (let i = 0; i < allPartListRealUsage.length; i++) {
            concatArr = concatArr.concat(allPartListRealUsage[i])

        }
        // let concateQtyCheck = []
        // for (let i = 0; i < allDO.length; i++) {
        //     let getLatestQtyCheck = await models.QtyCheck.findAll({
        //         where: { DOId: allDO[i] },
        //         order: [['createdAt', 'DESC']],
        //         raw: true
        //     })
        //     concateQtyCheck = concateQtyCheck.concat(getLatestQtyCheck)


        // }
        // concateQtyCheck.sort((a, b) => new Date(b.createdAt) - new Date(a.createdAt))

        // let uniqueQtyCheck = []
        // let uniqueQtyCheckArr = []
        // for (let i = 0; i < concateQtyCheck.length; i++) {
        //     if(!uniqueQtyCheck.includes(concateQtyCheck[i]["ChildPartListId"])){
        //         uniqueQtyCheck.push(concateQtyCheck[i]["ChildPartListId"])
        //         uniqueQtyCheckArr.push(concateQtyCheck[i])
        //     }
        // }
        // console.log(uniqueQtyCheckArr)

        for (let i = 0; i < state.storedValidMat.length; i++) {
            let fetchedQtyCheck = []

            for (let j = 0; j < concatArr.length; j++) {

                if (state.storedValidMat[i]["partNo"].trim() === concatArr[j]['ChildPartList.ChildPartNo']) {

                    let getQtyCheck = await models.QtyCheck.findOne({
                        where: {
                            DOId: concatArr[j].DOId,
                            ChildPartListId: concatArr[j].ChildPartListId,
                            Order: concatArr[j].Order
                        },
                        include: [{
                            model: models.ChildPartList,
                            required: true,
                            include: [{
                                model: models.PartListUsage,
                                required: true,
                            }]
                        }],
                        order: [['createdAt', 'DESC']],
                        raw: true
                    })
                    
                    if (getQtyCheck === null) {
                        fetchedQtyCheck.push({

                            QtyLeft: parseInt(state.storedValidMat[i]["qty"]),
                            ChildPartListId: concatArr[j].ChildPartListId,
                            DOId: concatArr[j].DOId,
                            Order: concatArr[j].Order,
                            ChildPartNo: concatArr[j]['ChildPartList.ChildPartNo'],
                            ProcessName: concatArr[j]['ChildPartList.ProcessName']
                        })
                        j = concatArr.length
                    } else {
                        let totalQtyYield = totalQtySum * getQtyCheck['ChildPartList.PartListUsages.Usage'] + (Math.ceil(getQtyCheck['ChildPartList.UsagePerBox'] * 0.01 * getQtyCheck['ChildPartList.MaterialYieldPercent']))
                        if (getQtyCheck["QtyLeft"] < totalQtyYield) {
                            fetchedQtyCheck.push({

                                QtyLeft: parseInt(state.storedValidMat[i]["qty"]),
                                ChildPartListId: concatArr[j].ChildPartListId,
                                DOId: concatArr[j].DOId,
                                Order: concatArr[j].Order,
                                ChildPartNo: concatArr[j]['ChildPartList.ChildPartNo'],
                                ProcessName: concatArr[j]['ChildPartList.ProcessName']
                            })
                            j = concatArr.length
                        }

                    }
                }
            }
            // console.log(fetchedQtyCheck)

            if (fetchedQtyCheck.length !== 0) {
                let getCommonChild = await models.ChildPartList.findOne({
                    where: { ChildPartNo: fetchedQtyCheck[0].ChildPartNo },
                    raw: true
                })

                let getLatestQtyCheck = await models.QtyCheck.findOne({
                    where: {
                        ChildPartNo: fetchedQtyCheck[0].ChildPartNo,
                        ProcessName: fetchedQtyCheck[0].ProcessName
                    },
                    raw: true,
                    order: [['createdAt', 'DESC']],
                    include: [{ model: models.ChildPartList }]
                })


                if (getLatestQtyCheck === null) {

                    await models.QtyCheck.create({
                        QtyLeft: parseInt(state.storedValidMat[i]["qty"]),
                        ChildPartListId: fetchedQtyCheck[0].ChildPartListId,
                        DOId: fetchedQtyCheck[0].DOId,
                        Order: fetchedQtyCheck[0].Order,
                        ChildPartNo: fetchedQtyCheck[0].ChildPartNo,
                        ProcessName: fetchedQtyCheck[0].ProcessName
                    }).then((qty) => {
                        storedModelQtyCheck.push(qty)
                    })
                    let getLatestQtyLeft = await models.QtyLeft.findOne({
                        where: { ChildPartListId: getCommonChild.ChildPartListId },
                        raw: true,
                        order: [['createdAt', 'DESC']],
                    })

                    if (getLatestQtyLeft !== null) {
                        await models.QtyLeft.create({
                            QtyLeft: parseInt(state.storedValidMat[i]["qty"]) + getLatestQtyLeft["QtyLeft"],
                            ChildPartListId: getCommonChild.ChildPartListId,
                        }).then((qtyLeft) => {
                            storedModelQtyLeft.push(qtyLeft)
                        })
                    } else {
                        await models.QtyLeft.create({
                            QtyLeft: parseInt(state.storedValidMat[i]["qty"]),
                            ChildPartListId: getCommonChild.ChildPartListId,
                        }).then((qtyLeft) => {
                            storedModelQtyLeft.push(qtyLeft)
                        })
                    }

                    await models.ReceiveMaterialPart.create({
                        ChildPartListId: fetchedQtyCheck[0].ChildPartListId,
                        ChildPartLot: state.storedValidMat[i]["polotlabel"],
                        MOId: findMO["MOId"],
                        Qty: parseInt(state.storedValidMat[i]["qty"]),
                    }).then((receiveMat) => {
                        storedReceiveMaterialPart.push(receiveMat)
                    })

                } else {

                    await models.QtyCheck.create({
                        QtyLeft: parseInt(state.storedValidMat[i]["qty"]) + getLatestQtyCheck["QtyLeft"],
                        ChildPartListId: fetchedQtyCheck[0].ChildPartListId,
                        DOId: fetchedQtyCheck[0].DOId,
                        Order: fetchedQtyCheck[0].Order,
                        ChildPartNo: fetchedQtyCheck[0].ChildPartNo,
                        ProcessName: fetchedQtyCheck[0].ProcessName
                    }).then(async (qty) => {
                        storedModelQtyCheck.push(qty)
                    })
                    await models.QtyLeft.create({
                        QtyLeft: parseInt(state.storedValidMat[i]["qty"]) + getLatestQtyCheck["QtyLeft"],
                        ChildPartListId: getCommonChild.ChildPartListId,
                    }).then((qtyLeft) => {
                        storedModelQtyLeft.push(qtyLeft)
                    })
                    await models.ReceiveMaterialPart.create({
                        ChildPartListId: fetchedQtyCheck[0].ChildPartListId,
                        ChildPartLot: state.storedValidMat[i]["polotlabel"],
                        MOId: findMO["MOId"],
                        Qty: parseInt(state.storedValidMat[i]["qty"]),
                    }).then((receiveMat) => {
                        storedReceiveMaterialPart.push(receiveMat)
                    })
                }

            } else {
                stockNotEnough = true
                reason = "Out of order"
                console.log("Out of order")
            }
        }

        if (storedModelQtyCheck !== 0) {
            for (let i = 0; i < storedModelQtyCheck.length; i++) {
                let getAllQtyCheck = await models.QtyCheck.findAll({
                    where: { ChildPartListId: storedModelQtyCheck[i].dataValues.ChildPartListId },
                    raw: true
                })
                let exceedCount = 0
                for (let j = 0; j < getAllQtyCheck.length; j++) {
                    if (getAllQtyCheck[j]["QtyLeft"] >= totalQtySum) {
                        exceedCount++
                    }
                    if (exceedCount > 1) {
                        stockNotEnough = true
                        reason = "Stock Is Not Enough"
                        break;
                    }
                }
            }

            // if (stockNotEnough) {
            //     for (let i = 0; i < storedModelQtyCheck.length; i++) {
            //         await storedModelQtyCheck[i].destroy()
            //     }
            //     for (let i = 0; i < storedModelQtyLeft.length; i++) {
            //         await storedModelQtyLeft[i].destroy()
            //     }
            //     for (let i = 0; i < storedReceiveMaterialPart.length; i++) {
            //         await storedReceiveMaterialPart[i].destroy()
            //     }
            //     ClearReceiveData()
            //     state.status = reason
            //     res.send(state);
            //     return
            // }

        }

        let partList = []
        for (let i = 0; i < findDO.length; i++) {
            let getPartListUsage = await models.PartListUsage.findAll({
                where: { PartNoId: findDO[i]["PartNoId"] },
                include: [{
                    model: models.ChildPartList,
                    required: true
                }],
                raw: true
            })
            partList.push(getPartListUsage)

        }
        let allPartList = []
        for (let i = 0; i < partList.length; i++) {

            allPartList = allPartList.concat(partList[i])

            // Object.assign(allPartList, partList[i])
        }
        let uniqueChild = []
        let uniqueChildArr = []
        for (let i = 0; i < allPartList.length; i++) {
            if (!uniqueChild.includes(allPartList[i]["ChildPartListId"])) {
                uniqueChild.push(allPartList[i]["ChildPartListId"])
                uniqueChildArr.push(Object.assign(allPartList[i], { TotalQtyYield: totalQtySum * allPartList[i]['Usage'] + (Math.ceil(allPartList[i]['ChildPartList.UsagePerBox'] * 0.01 * allPartList[i]['ChildPartList.MaterialYieldPercent'])) }))

            }
        }
        let allQtyCheck = []
        for (let i = 0; i < findDO.length; i++) {
            let getAllLatestQtyCheck = await models.QtyCheck.findAll({
                where: { DOId: findDO[i]["DOId"] },
                raw: true,
                order: [['createdAt', 'DESC']],
            })
            let uniqueChild = []
            let uniqueChildArr = []
            for (let j = 0; j < getAllLatestQtyCheck.length; j++) {
                if (!uniqueChild.includes(getAllLatestQtyCheck[j]["ChildPartListId"])) {
                    uniqueChild.push(getAllLatestQtyCheck[j]["ChildPartListId"])
                    uniqueChildArr.push(getAllLatestQtyCheck[j])
                }
            }
            allQtyCheck.push(uniqueChildArr)

        }

        let concateObj = []
        for (let i = 0; i < allQtyCheck.length; i++) {
            concateObj = concateObj.concat(allQtyCheck[i])
        }
        // console.log(uniqueChildArr)
        // console.log(concateObj)
        let result = []
        for (let i = 0; i < uniqueChildArr.length; i++) {
            for (let j = 0; j < concateObj.length; j++) {
                if (uniqueChildArr[i]['ChildPartList.ChildPartListId'] === concateObj[j]["ChildPartListId"]) {
                    result.push(Object.assign(uniqueChildArr[i], concateObj[j]))
                    j = concateObj.length
                }
            }
        }
        // console.log(result)
        let receiveFinish = false
        if (result.length >= uniqueChildArr.length) {
            receiveFinish = true
            for (let i = 0; i < result.length; i++) {
                if (result[i]["QtyLeft"] < result[i]["TotalQtyYield"]) {
                    receiveFinish = false
                    break
                }
            }
        }


        if (receiveFinish) {

            //find mo then prep for saving to log
            const index = state.savedMOValues.indexOf(state.MOValue)
            if (index > -1) {
                state.savedMOValues.splice(index, 1)
            }
            //save mo to log
            await models.MO_Log.create({
                MOId: findMO["MOId"],
                MONo: findMO["MONo"]
            })

        }




    } catch (e) {
        console.log(e)
        ClearReceiveData()
        state.status = "Error"
        res.send(state);
        return
    }
    ClearReceiveData()
    state.status = "OK"

    res.send(state);

})

router.post("/shopperPOSubmitAll", async (req, res) => {

    try {
        let storedMachineArray = req.body.storedMachineGuide
        let stateFromFrontend = req.body;


        let checkMatchingDOPO = CheckMatchingDOPO(stateFromFrontend.savedDO);

        if (checkMatchingDOPO === "INVALID_SCAN_DO_PO") {
            ClearDataPO();
            let currentState = state;
            currentState["status"] = "INVALID_SCAN_DO_PO";
            res.send(currentState);
            return;
        }

        /*if (state.storedMachine.length === 0) {
            ClearDataPO();
            let currentState = state;
            currentState["status"] = "NO_MACHINE_OR_MAT_IS_SELECTED";
            res.send(currentState);
            return;
        }*/



        let machineCount = 0
        for (let i = 0; i < storedMachineArray.length; i++) {
            if (storedMachineArray[i]["Process"]["value"].includes(storedMachineArray[i]["Process"]["ProcessName"])) {
                machineCount++
            }
        }
        // console.log(state.storedMachineGuide)

        console.log(machineCount + " " + storedMachineArray.length)
        if (machineCount !== storedMachineArray.length) {
            ClearDataPO()
            let currentState = state;
            currentState["status"] = "MACHINE_IS_INCORRECTLY_SCANNED";
            res.send(state)
            return
        }



        let machineList = []
        for (let i = 0; i < storedMachineArray.length; i++) {

            let getMachine = await models.Machine.findOne({
                where: { MachineName: storedMachineArray[i]["Process"]["value"] },
                raw: true,
            });

            if (getMachine === null) {
                ClearDataPO();
                let currentState = state;
                currentState["status"] = "NO_MACHINE_IS_FOUND_IN_DATABASE";
                res.send(currentState);
                return
            }

            machineList.push(getMachine)
        }

        let getPO = await models.PO.findOne({
            where: { PONo: checkMatchingDOPO["PONo"] },
            raw: true,
        });

        let getDO = await models.DO.findOne({
            where: { DOId: getPO["DOId"] },
            raw: true,
        });

        let getAllPO = await models.PO.findAll({
            where: { DOId: getDO["DOId"] },
            raw: true
        });


        // console.log(machineList)
        let filterList = []
        for (let i = 0; i < machineList.length; i++) {
            let findEmployee = await models.EmployeeLineMatch.findOne({
                where: { Machine: machineList[i]["MachineName"] },
                raw: true,
                order: [['createdAt', 'DESC']],
            })
            if (findEmployee !== null) {
                let obj = Object.assign(machineList[i], findEmployee)
                filterList.push(obj)
            }
        }

        // console.log(container)
        // console.log(getAllPO)
        // console.log(findReceive)
        let findAllQtyCheck = await models.QtyCheck.findAll({
            where: { DOId: getPO["DOId"] },
            raw: true,
            order: [['createdAt', 'DESC']],
        })

        let uniqueChildPartList = []
        let uniqueChildPartListArr = []
        for (let i = 0; i < findAllQtyCheck.length; i++) {
            if (!uniqueChildPartList.includes(findAllQtyCheck[i]["ChildPartListId"])) {
                uniqueChildPartList.push(findAllQtyCheck[i]["ChildPartListId"])
                uniqueChildPartListArr.push(findAllQtyCheck[i])
            }
        }

        let makeProductionData = []

        let fragmentOne = []

        for (let i = 0; i < uniqueChildPartListArr.length; i++) {
            for (let j = 0; j < getAllPO.length; j++) {
                fragmentOne.push(
                    {
                        POId: getAllPO[j]["POId"],
                        ChildPartListId: uniqueChildPartListArr[i]["ChildPartListId"],
                    }
                )
            }
        }

        for (let i = 0; i < filterList.length; i++) {
            for (let j = 0; j < fragmentOne.length; j++) {
                makeProductionData.push(
                    {
                        POId: fragmentOne[j]["POId"],
                        ChildPartListId: fragmentOne[j]["ChildPartListId"],
                        EmployeeId: filterList[i]["EmployeeId"],
                        MachineId: filterList[i]["MachineId"]
                    }
                )
            }
        }

        await models.ProductionData.bulkCreate(makeProductionData);

    } catch (e) {
        console.log(e)

    }


    ClearDataPO()
    let currentState = state;
    currentState["status"] = "OK";
    res.send(currentState)
})

router.get("/getMOFromReceive", async (req, res) => {
    let receive = await models.ReceiveMaterialPart.findAll({ raw: true })
    let uniqueMO = []
    for (let i = 0; i < receive.length; i++) {
        if (!uniqueMO.includes(receive[i]["MOId"])) {
            uniqueMO.push(receive[i]["MOId"])
        }
    }
    let mo = []
    for (let i = 0; i < uniqueMO.length; i++) {
        let getMO = await models.MO.findOne({
            where: { MOId: uniqueMO[i] },
            raw: true
        })
        mo.push(getMO["MONo"])
    }

    // let moNotUsedList = []

    // for (let i = 0; i < mo.length; i++) {
    //     let findUsedMO = await models.MO_Log.findOne({
    //         where: { MONo: mo[i] },
    //         raw: true
    //     })
    //     if (findUsedMO === null) {
    //         moNotUsedList.push(mo[i])
    //     }
    // }

    // console.log(moNotUsedList)

    res.send(mo)
})

router.post("/addMachineShopperStore", async (req, res) => {
    let stateFromFrontend = req.body;
    state = stateFromFrontend;
    let scannedMC = stateFromFrontend.MC;

    state.storedMachine.push(scannedMC);
    res.send(state);
});

router.get("/clearDataPOPageShopperStore", async (req, res) => {
    ClearDataPO();
    res.send(state);
});


function ClearDataPO() {
    state.DO = ""
    state.EMP = ""
    state.MC = ""
    state.scanMat = ""
    state.row = 0
    state.storedMachine = []
    state.storedMat = []
    state.selectedMOPO = ""
    state.storedMachineGuide = []
    state.savedDO = ""
}

function CheckMatchingDOPO(scannedDOPO) {
    // console.log(split)
    if (scannedDOPO.length > 112 || scannedDOPO.length === 0) {
        return "INVALID_SCAN_DO_PO";
    }

    let DOPO;
    try {
        let DO_PATTERN = /N\w+-\w+-\w+|N\w+-\w+|N\w+/g;
        const tag = scannedDOPO;
        const arr = [...tag.matchAll(DO_PATTERN)];
        let DO = arr[0][0]
        let PO_PATTERN = /D.*/g;
        const arrPO = [...tag.matchAll(PO_PATTERN)];
        let PO = arrPO[0][0]
        DOPO = { DONo: String(DO).trim(), PONo: String(PO).trim() }
    } catch (e) {
        console.log(e);
        return "INVALID_SCAN_DO_PO";
    }
    return DOPO;
}


router.post("/submitDO", async (req, res) => {

    let DOPO = CheckMatchingDOPO(req.body.DO)
    let getDO = await models.DO.findOne({
        where: { DONo: DOPO["DONo"] },
        raw: true
    })
    let getPartListUsage = await models.PartListUsage.findAll({
        where: { PartNoId: getDO["PartNoId"] },
        include: {
            model: models.ChildPartList,
            required: true
        },
        raw: true
    })
    let getUniqueProcessName = []
    for (let i = 0; i < getPartListUsage.length; i++) {
        if (!getUniqueProcessName.includes(getPartListUsage[i]['ChildPartList.ProcessName'])) {
            getUniqueProcessName.push(getPartListUsage[i]['ChildPartList.ProcessName'])
            state.storedMachineGuide.push(
                {
                    Process: { ProcessName: getPartListUsage[i]['ChildPartList.ProcessName'], value: "" }
                }
            )
        }
    }
    state.savedDO = req.body.DO


    res.send(state)
})

module.exports = router;
