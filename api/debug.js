const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../models");
const moment = require("moment");
const tools = require("../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../models");
const { getValue } = require("cli");


router.get("/testCheckStock", async (req, res) => {

    let findDO = [{ PartNoId: 8, DOId: 10 }, { PartNoId: 8, DOId: 11 }]

    totalQtySum = 200

    let partList = []
    for (let i = 0; i < findDO.length; i++) {
        let getPartListUsage = await models.PartListUsage.findAll({
            where: { PartNoId: findDO[i]["PartNoId"] },
            include: [{
                model: models.ChildPartList,
                required: true
            }],
            raw: true
        })
        partList.push(getPartListUsage)

    }
    let allPartList = []
    for (let i = 0; i < partList.length; i++) {

        allPartList = allPartList.concat(partList[i])

        // Object.assign(allPartList, partList[i])
    }
    let uniqueChild = []
    let uniqueChildArr = []
    for (let i = 0; i < allPartList.length; i++) {
        if (!uniqueChild.includes(allPartList[i]["ChildPartListId"])) {
            uniqueChild.push(allPartList[i]["ChildPartListId"])
            uniqueChildArr.push(Object.assign(allPartList[i], { TotalQtyYield: totalQtySum * allPartList[i]['Usage'] + (Math.ceil(allPartList[i]['ChildPartList.UsagePerBox'] * 0.01 * allPartList[i]['ChildPartList.MaterialYieldPercent'])) }))

        }
    }
    let allQtyCheck = []
    for (let i = 0; i < findDO.length; i++) {
        let getAllLatestQtyCheck = await models.QtyCheck.findAll({
            where: { DOId: findDO[i]["DOId"] },
            raw: true,
            order: [['createdAt', 'DESC']],
        })
        let uniqueChild = []
        let uniqueChildArr = []

        for (let j = 0; j < getAllLatestQtyCheck.length; j++) {
            if (!uniqueChild.includes(getAllLatestQtyCheck[j]["ChildPartListId"])) {
                uniqueChild.push(getAllLatestQtyCheck[j]["ChildPartListId"])
                uniqueChildArr.push(getAllLatestQtyCheck[j])
            }
        }
        allQtyCheck.push(uniqueChildArr)

    }
    let concateObj = []
    for (let i = 0; i < allQtyCheck.length; i++) {
        concateObj = concateObj.concat(allQtyCheck[i])
    }
    // console.log(uniqueChildArr)
    // console.log(concateObj)
    let result = []
    for (let i = 0; i < uniqueChildArr.length; i++) {
        for (let j = 0; j < concateObj.length; j++) {
            if (uniqueChildArr[i]['ChildPartList.ChildPartListId'] === concateObj[j]["ChildPartListId"]) {
                result.push(Object.assign(uniqueChildArr[i], concateObj[j]))
                j = concateObj.length
            }
        }
    }
    let receiveFinish = true
    for (let i = 0; i < result.length; i++) {
        if (result[i]["QtyLeft"] <= result[i]["TotalQtyYield"]) {
            receiveFinish = false
        }
    }
    // console.log(result)

    // console.log(receiveFinish)
    // console.log(uniqueChildArr)

    for (let i = 0; i < findDO.length; i++) {
        let getReceieveMat = await models.ReceiveMaterialPart.findAll({
            where: { MOId: 7 },
            raw: true
        })
        console.log(getReceieveMat)
    }

    res.send("Blast Off!")



    // if (getReceieveMat.length === sumPartListRealUsage) {
    //     //find mo then prep for saving to log
    //     const index = state.savedMOValues.indexOf(state.MOValue)
    //     if (index > -1) {
    //         state.savedMOValues.splice(index, 1)
    //     }
    //     //save mo to log
    //     await models.MO_Log.create({
    //         MOId: findMO["MOId"],
    //         MONo: findMO["MONo"]
    //     })
    // }
    // }
})

router.get("/testInsertShort", async (req, res) => {


    let findPO = await models.PO.findOne({
        where: { PONo: "D407802170" },
        raw: true
    })

    let getDOId = await models.DO.findOne({
        where: { DOId: findPO["DOId"] },
        raw: true
    })

    let latestQty = await models.QtyCheck.findAll({
        where: { DOId: getDOId["DOId"] },
        order: [['createdAt', 'DESC']],
        include: [{
            model: models.ChildPartList,
            required: true
        }],
        raw: true
    })

    let uniqueQty = []
    let uniqueQtyArr = []
    for (let i = 0; i < latestQty.length; i++) {
        if (!uniqueQty.includes(latestQty[i]['ChildPartListId'])) {
            uniqueQty.push(latestQty[i]['ChildPartListId'])
            uniqueQtyArr.push(latestQty[i])
        }
    }

    let getMO = await models.MO.findOne({
        where: { MOId: getDOId["MOId"] },
        raw: true
    })

    let getDOLog = await models.DO_Log.findAll({ raw: true })


    let MOIdFromDOLog = []
    for (let i = 0; i < getDOLog.length; i++) {
        if (!MOIdFromDOLog.includes(getDOLog[i]["MOId"])) {
            MOIdFromDOLog.push(getDOLog[i]["MOId"])
        }
    }

    let moList = []
    for (let i = 0; i < MOIdFromDOLog.length; i++) {
        let mo = await models.MO.findOne({
            where: { MOId: MOIdFromDOLog[i] },
            raw: true
        })
        moList.push(mo)
    }


    let allMO = await models.MO.findAll({ raw: true })
    let nextMo = ""

    let pattern = /\w+_/g;
    const tag = getMO["MONo"];
    const arr = [...tag.matchAll(pattern)];
    let process = ""
    try {
        process = arr[0][0]
    } catch (e) {
        console.log(e)
    }
    process = process.substring(0, process.length - 1)

    let matchMO = []
    for (let i = 0; i < allMO.length; i++) {
        if (allMO[i]["MONo"].includes(process)) {
            matchMO.push(allMO[i])
        }
    }

    let availableMO = []
    for (let i = 0; i < moList.length; i++) {
        for (let j = 0; j < matchMO.length; j++) {
            if (moList[i]["MONo"] !== matchMO[j]["MONo"]) {
                availableMO.push(matchMO[j]["MONo"])
            }
        }
    }

    let uniqueMO = []
    for (let i = 0; i < availableMO.length; i++) {
        if (!uniqueMO.includes(availableMO[i])) {
            uniqueMO.push(availableMO[i])
        }
    }

    if (uniqueMO.length !== 0) {
        nextMo = uniqueMO[0]
        let getNextMO = await models.MO.findAll({
            where: { MONo: nextMo },
            include: [{
                model: models.DO,
                required: true
            }],
            raw: true
        })

        let getAllReceive = await models.ReceiveMaterialPart.findAll(
            {
                raw: true,
                where: { MOId: getDOId["MOId"] },
                order: [['createdAt', 'DESC']],
            }
        )

        let latestReceive = []
        let latestReceiveArr = []
        for (let i = 0; i < getAllReceive.length; i++) {
            if (!latestReceive.includes(getAllReceive[i]["ChildPartListId"])) {
                latestReceive.push(getAllReceive[i]["ChildPartListId"])
                latestReceiveArr.push(getAllReceive[i])
            }
        }

        let sumTotalQty = 0
        for (let i = 0; i < getNextMO.length; i++) {
            sumTotalQty += getNextMO[i]['DOs.TotalQty']
        }
  
        let receiveNew = []
        for (let i = 0; i < uniqueQtyArr.length; i++) {
            let childList = []
            for (let j = 0; j < getAllReceive.length; j++) {
                if (uniqueQtyArr[i]["ChildPartListId"] === getAllReceive[j]["ChildPartListId"]) {
                    childList.push(getAllReceive[j])
                }
            }

            let qtyLeftCheck = 0
            for (let j = 0; j < childList.length; j++) {

                if (qtyLeftCheck > uniqueQtyArr[i]["QtyLeft"]) {
                    j = childList.length
                } else {
                    // if(uniqueQtyArr[i]["QtyLeft"] < sumTotalQty){
                        qtyLeftCheck += childList[j]["Qty"]
                        receiveNew.push(Object.assign(childList[j], { NextMO: getNextMO[0]["MOId"] }, uniqueQtyArr[i]))
                    // }
                    // }else{
                    //     qtyLeftCheck += childList[j]["Qty"]
                    //     receiveNew.push(Object.assign(childList[j], { NextMO: getNextMO[0]["MOId"] }, uniqueQtyArr[i]))
                    //     j = childList.length
                    // }
                }
            }
        }

        let filterReceive = []
        for (let i = 0; i < receiveNew.length; i++) {
            filterReceive.push(
                {
                    ChildPartListId: receiveNew[i]["ChildPartListId"],
                    ChildPartLot: receiveNew[i]["ChildPartLot"],
                    MOId: receiveNew[i]["NextMO"],
                    Qty: receiveNew[i]["Qty"]
                })
        }

        let partNo = []
        for (let i = 0; i < getNextMO.length; i++) {
            let partListUsage = await models.PartListUsage.findAll({
                where: { PartNoId: getNextMO[i]['DOs.PartNoId'] },
                raw: true
            })
            partNo.push(partListUsage)
        }
        let partListUsage = []

        for (let i = 0; i < partNo.length; i++) {
            Object.assign(partListUsage, partNo[i])
        }
        let uniqueChild = []
        for (let i = 0; i < partListUsage.length; i++) {
            if (!uniqueChild.includes(partListUsage[i]["ChildPartListId"])) {
                uniqueChild.push(partListUsage[i]["ChildPartListId"])
            }
        }
        let result = []
        for (let i = 0; i < uniqueChild.length; i++) {
            for (let j = 0; j < filterReceive.length; j++) {
                if (filterReceive[j]["ChildPartListId"] === uniqueChild[i]) {
                    result.push(filterReceive[j])
                }
            }
        }
        
        console.log(result)
    }

    res.send("Test")

})
module.exports = router;
