const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

//insert childpartlist, partlist, machine, masterpostatus,employee
router.post("/insertChildPartList", async (req, res) => {
    let data = req.body;

    try {
        await models.ChildPartList.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertPartList", async (req, res) => {
    let data = req.body;
    try {
        await models.PartList.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});
router.post("/insertGroupPartList", async (req, res) => {
    let data = req.body;
    try {
        await models.GroupPartList.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertMachine", async (req, res) => {
    let data = req.body;
    try {
        await models.Machine.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertMasterPOStatus", async (req, res) => {
    let data = req.body;
    try {
        await models.MasterPOStatus.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertEmployee", async (req, res) => {
    let data = req.body;

    try {
        let getEmployeeRoleName = await models.EmployeeRole.findOne({
            where: { RoleName: data.RoleName },
            raw: true,
        });

        await models.Employee.create({
            EmployeeId: data.EmployeeId,
            EmployeeName: data.EmployeeName,
            RoleId: getEmployeeRoleName["RoleId"],
        });

    } catch (error) { 
        console.log(error);
        res.send("No Data");
        return;
    }
    res.send("OK");
});

router.post("/insertEmployeeRole", async (req, res) => {
    let data = req.body;
    try {
        await models.EmployeeRole.create(data);
    } catch (error) {
        res.send("No Data");
        return;
    }
    res.send("OK");
});


module.exports = router;