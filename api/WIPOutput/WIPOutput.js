const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");



router.post("/popageclose", async (req, res) => {

    // console.log(req.body)
    // console.log(req.body.showMC)
    // console.log(req.body.showPONo)

    let scannedMC = req.body.showMC
    let scannedPONumbers = req.body.showPONo

    try {
        for (let i = 0; i < scannedPONumbers.length; i++) {
            if (scannedPONumbers[i].PONo.length === 0) {
                res.send("INVALID_SCAN_DO_PO");
                return;
            }
        }

        for (let i = 0; i < scannedPONumbers.length; i++) {
            if (scannedPONumbers[i].checked) {
                let findPO = await models.PO.findOne({
                    where: { PONo: scannedPONumbers[i].PONo },
                    raw: true
                })

                let getDO = await models.DO.findOne({
                    where: { DOId: findPO["DOId"] },
                    raw: true
                })

                let findMachine = await models.Machine.findOne({
                    where: { MachineName: scannedMC },
                    raw: true
                })

                let getPartList = await models.PartListUsage.findAll({//check if receivemat is completed
                    where: { PartNoId: getDO["PartNoId"] },
                    include: [{
                        model: models.ChildPartList,
                        required: true
                    }],
                    raw: true
                })

                let getQtyCheck = await models.QtyCheck.findAll({//check if receivemat is completed
                    where: { DOId: findPO["DOId"] },
                    raw: true
                })

                let getUniqueChildList = []

                for (let i = 0; i < getQtyCheck.length; i++) {
                    if (!getUniqueChildList.includes(getQtyCheck[i]["ChildPartListId"])) {
                        getUniqueChildList.push(getQtyCheck[i]["ChildPartListId"])
                    }
                }

                let getProcess = findMachine["Machine"] + "-" + findMachine["MachineType"]

                let filteredPartList = []
                for (let i = 0; i < getPartList.length; i++) {
                    if (getPartList[i]['ChildPartList.ProcessName'] === getProcess) {
                        filteredPartList.push(getPartList[i])
                    }
                }

                let foundCount = 0
                for (let i = 0; i < filteredPartList.length; i++) {//find if process complete in partlist in qtycheck
                    for (let j = 0; j < getQtyCheck.length; j++) {

                        if (filteredPartList[i]['ChildPartList.ChildPartListId'] === getQtyCheck[j]["ChildPartListId"]) {
                            foundCount++
                            j = getQtyCheck.length
                        }
                    }
                }

                let getProductionData = await models.ProductionData.findAll({
                    where: { MachineId: findMachine["MachineId"] },
                    raw: true
                })

                let uniqueProductionDataChildPart = []
                for (let i = 0; i < getProductionData.length; i++) {
                    if (!uniqueProductionDataChildPart.includes(getProductionData[i]["ChildPartListId"])) {
                        uniqueProductionDataChildPart.push(getProductionData[i]["ChildPartListId"])
                    }
                }

                let foundProductionCount = 0
                for (let i = 0; i < filteredPartList.length; i++) {

                    for (let j = 0; j < uniqueProductionDataChildPart.length; j++) {
                        if (filteredPartList[i]['ChildPartList.ChildPartListId'] === uniqueProductionDataChildPart[j]) {
                            foundProductionCount++
                            j = uniqueProductionDataChildPart.length
                        }
                    }

                }
                console.log(foundCount + " " + foundProductionCount)
                if (foundCount !== filteredPartList.length || foundProductionCount !== filteredPartList.length) {
                    res.send("SCANNED_MATERIALS_ARE_INCOMPLETE");
                    return
                }


                let findAllPOInProductionData = await models.ProductionData.findAll({
                    where: { POId: findPO["POId"] },
                    raw: true
                })

                let uniqueMachineId = []
                for (let i = 0; i < findAllPOInProductionData.length; i++) {
                    if (!uniqueMachineId.includes(findAllPOInProductionData[i]["MachineId"])) {
                        uniqueMachineId.push(findAllPOInProductionData[i]["MachineId"])
                    }
                }


                if (!uniqueMachineId.includes(findMachine["MachineId"])) {
                    res.send("CARD_INCORRECTLY_INPUT");
                    return
                }

                let getCardLogOne = await models.Card_Log.findOne({
                    where: { POId: findPO["POId"], MachineId: findMachine["MachineId"] },
                    raw: true
                })

                //check if po existed in card log
                // let getCardLog = await models.Card_Log.findAll({
                //     where: { POId: findPO["POId"] },
                //     raw: true
                // })
                if (getCardLogOne !== null) {
                    res.send("CARD_HAS_BEEN_SCANNED");
                    return
                }

                // console.log(getCardLog)



                await models.POCloseLog.create({
                    PONo: findPO["PONo"],
                    MachineName: findMachine["MachineName"]
                })

                let findChildPartListId = await models.ProductionData.findAll({
                    where: [{ POId: findPO["POId"] }],
                    order: [['createdAt', 'DESC']],
                    raw: true
                })

                if (findChildPartListId.length === 0) {//**********alert to user when scanned out************//
                    res.send("No ChildPart found on ProductionData"); //can't find childpart because user forget to input material to line
                    return
                }

                let stockExtractResult = await StockExtraction(scannedPONumbers[i].PONo, findChildPartListId, findMachine)
                console.log(stockExtractResult)
                if (stockExtractResult !== "OK") {
                    res.send(String(stockExtractResult))
                    return
                }

                await models.POCloseLog.create({
                    PONo: findPO["PONo"],
                    MachineName: findMachine["MachineName"]
                })
                // console.log(findPO)

                // let findProduction = await models.ProductionData.findAll({
                //     where: {MachineId: findMachine["MachineId"]},
                //     raw:true
                // })


                await models.Card_Log.create({
                    DOId: findPO["DOId"],
                    CardNo: findPO["CardNo"],
                    POId: findPO["POId"],
                    MachineId: findMachine["MachineId"]
                })



                let machine = String(scannedMC).replace(/-\d+/g, "")

                let findMasterPOStatusClose = await models.MasterPOStatus.findOne({
                    where: { POStatus: machine + "_close" },
                    raw: true
                })

                // console.log(getMasterPOStatusClose)
                await models.POStatus.create({
                    MasterPOStatusId: findMasterPOStatusClose["MasterPOStatusId"],
                    POId: findPO["POId"],
                    MfgDate: new Date(Date.now()).toISOString(),
                })
                UpdateDOStatus(findPO)


            }

        }















    } catch (error) {
        console.log(error)
        res.send("No Data");
        return;
    }

    res.send("OK");
});

async function StockExtraction(PO, childPartListModel, Machine) {
    let thresholdStock = 0
    try {
        let scannedPONo = PO;
        // console.log(scannedPONo)
        // console.log(childPartListModel)
        let findPartList = await models.PO.findOne({
            where: { PONo: scannedPONo },
            include: [{
                model: models.DO,
                required: true,
                include: [{
                    model: models.PartList,
                    required: true,
                }]
            }],
            raw: true
        })

        let uniqueChildPartArr = []
        for (let i = 0; i < childPartListModel.length; i++) {

            if (!uniqueChildPartArr.includes(childPartListModel[i]["ChildPartListId"])) {
                uniqueChildPartArr.push(childPartListModel[i]["ChildPartListId"])
            }
        }

        if (findPartList !== null) {
            for (let i = 0; i < uniqueChildPartArr.length; i++) {//check if latestQtyIsEnoughForExtract
                let getChild = await models.ChildPartList.findOne({
                    where: { ChildPartListId: uniqueChildPartArr[i] },
                    raw: true
                })
            
                let getCommonChild = await models.ChildPartList.findOne({
                    where: { ChildPartNo: getChild["ChildPartNo"] },
                    raw: true
                })
                // console.log(getCommonChild)

                let getLatestQtyLeft = await models.QtyLeft.findOne({
                    where: { ChildPartListId: getCommonChild["ChildPartListId"] },
                    order: [['createdAt', 'DESC']],
                })
                // console.log(getLatestQtyLeft)
                let findPartListUsage = await models.PartListUsage.findOne({
                    where: { ChildPartListId: uniqueChildPartArr[i] },
                    raw: true
                })
                let usage = findPartList['DO.Balance'] * findPartListUsage["Usage"]
                if(getLatestQtyLeft !== null){
                    if (getLatestQtyLeft["QtyLeft"] <= usage * thresholdStock) {
                        return "StockIsNotEnough"
                    }
                }
         

            }
            let machineProcess = Machine["Machine"] + "-" + Machine["MachineType"]

            for (let i = 0; i < uniqueChildPartArr.length; i++) {

                let getChild = await models.ChildPartList.findOne({
                    where: { ChildPartListId: uniqueChildPartArr[i] },
                    raw: true
                })
                let getCommonChild = await models.ChildPartList.findOne({
                    where: { ChildPartNo: getChild["ChildPartNo"] },
                    raw: true
                })

                let getLatestQtyLeft = await models.QtyLeft.findOne({
                    where: { ChildPartListId: getCommonChild["ChildPartListId"] },
                    order: [['createdAt', 'DESC']],
                })
                let findPartListUsage = await models.PartListUsage.findOne({
                    where: { ChildPartListId: uniqueChildPartArr[i] },
                    raw: true
                })
                let usage = findPartList['DO.Balance'] * findPartListUsage["Usage"]


                if (getLatestQtyLeft !== null && getChild["ProcessName"] === machineProcess) {

                    let findQtyCheck = await models.QtyCheck.findOne({
                        where: {
                            ChildPartListId: Number(uniqueChildPartArr[i]),
                        },
                        order: [['createdAt', 'DESC']],
                        raw: true
                    })
                    if (findQtyCheck !== null) {
                        await models.QtyLeft.create({
                            QtyLeft: getLatestQtyLeft["QtyLeft"] - usage,
                            ChildPartListId: getLatestQtyLeft["ChildPartListId"]
                        })
                        await models.QtyCheck.create({
                            QtyLeft: getLatestQtyLeft["QtyLeft"] - usage,
                            ChildPartListId: uniqueChildPartArr[i],
                            DOId: findPartList["DO.DOId"],
                            Order: findQtyCheck["Order"],
                            ChildPartNo: findQtyCheck["ChildPartNo"],
                            ProcessName: findQtyCheck["ProcessName"],
                        })
                    }


                }

            }
        }

    } catch (e) {
        console.log(e)
        return "Error"
    }


    return "OK"
}

async function UpdateDOStatus(POModel) {

    let findAllDoCountInCardLog = await models.Card_Log.findAll({
        where: { DOId: POModel["DOId"] }
    })

    let findAllPOInProductionData = await models.ProductionData.findAll({
        where: { POId: POModel["POId"] },
        raw: true
    })
    let uniqueMachineId = []
    for (let i = 0; i < findAllPOInProductionData.length; i++) {
        if (!uniqueMachineId.includes(findAllPOInProductionData[i]["MachineId"])) {
            uniqueMachineId.push(findAllPOInProductionData[i]["MachineId"])
        }
    }



    findAllDoCountInCardLog = findAllDoCountInCardLog.length



    // console.log(findAllDoCountInCardLog)

    let getDOId = await models.DO.findOne({
        where: { DOId: POModel["DOId"] },
        raw: true
    })

    if (findAllDoCountInCardLog === getDOId["TotalCard"] * uniqueMachineId.length) {

        console.log("close that do")

        let getDOId = await models.DO.findOne({
            where: { DOId: POModel["DOId"] },
            raw: true
        })

        await models.DO.update(
            {
                DOStatusId: 2,
            },
            {
                where: {
                    DOId: POModel["DOId"]
                }
            }).then(async () => {
                let finishedDOStatus = {
                    DOId: getDOId["DOId"],
                    DONo: getDOId["DONo"],
                    Supp: getDOId["Supp"],
                    SuppName: getDOId["SuppName"],
                    CMSP: getDOId["CMSP"],
                    Prcs: getDOId["Prcs"],
                    PartName: getDOId["PartName"],
                    DueDate: getDOId["DueDate"],
                    DueQty: getDOId["DueQty"],
                    PrdtQty: getDOId["PrdtQty"],
                    Balance: getDOId["Balance"],
                    Unit: getDOId["Unit"],
                    ODDays: getDOId["ODDays"],
                    TotalCard: getDOId["TotalCard"],
                    TotalQty: getDOId["TotalQty"],
                    DeliveryDueDate: getDOId["DeliveryDueDate"],
                    CustomersPN: getDOId["CustomersPN"],
                    PartNoId: getDOId["PartNoId"],
                    DOStatusId: 2,
                    createdAt: getDOId["createdAt"],
                    updatedAt: getDOId["updatedAt"],
                    MOId: getDOId["MOId"]
                }
                let jsonArr = [getDOId, finishedDOStatus]
                await models.DO_Log.bulkCreate(jsonArr)

                let latestQty = await models.QtyCheck.findAll({
                    where: { DOId: getDOId["DOId"] },
                    order: [['createdAt', 'DESC']],
                    raw: true
                })
                let uniqueQty = []
                let uniqueQtyArr = []
                for (let i = 0; i < latestQty.length; i++) {
                    if (!uniqueQty.includes(latestQty[i]['ChildPartListId'])) {
                        uniqueQty.push(latestQty[i]['ChildPartListId'])
                        uniqueQtyArr.push(latestQty[i])
                    }
                }

                let getMO = await models.MO.findOne({
                    where: { MOId: getDOId["MOId"] },
                    raw: true
                })
                let getDOLog = await models.DO_Log.findAll({ raw: true })


                let MOIdFromDOLog = []
                for (let i = 0; i < getDOLog.length; i++) {
                    if (!MOIdFromDOLog.includes(getDOLog[i]["MOId"])) {
                        MOIdFromDOLog.push(getDOLog[i]["MOId"])
                    }
                }

                let moList = []
                for (let i = 0; i < MOIdFromDOLog.length; i++) {
                    let mo = await models.MO.findOne({
                        where: { MOId: MOIdFromDOLog[i] },
                        raw: true
                    })
                    moList.push(mo)
                }

                let allMO = await models.MO.findAll({ raw: true })
                let nextMo = ""

                let pattern = /\w+_/g;
                const tag = getMO["MONo"];
                const arr = [...tag.matchAll(pattern)];
                let process = ""
                try {
                    process = arr[0][0]
                } catch (e) {
                    console.log(e)
                }
                process = process.substring(0, process.length - 1)

                let matchMO = []
                for (let i = 0; i < allMO.length; i++) {
                    if (allMO[i]["MONo"].includes(process)) {
                        matchMO.push(allMO[i])
                    }
                }

                let availableMO = []
                for (let i = 0; i < moList.length; i++) {
                    for (let j = 0; j < matchMO.length; j++) {
                        if (moList[i]["MONo"] !== matchMO[j]["MONo"]) {
                            availableMO.push(matchMO[j]["MONo"])
                        }
                    }
                }

                let uniqueMO = []
                for (let i = 0; i < availableMO.length; i++) {
                    if (!uniqueMO.includes(availableMO[i])) {
                        uniqueMO.push(availableMO[i])
                    }
                }
                if (uniqueMO.length !== 0) {
                    nextMo = uniqueMO[0]
                    let getNextMO = await models.MO.findAll({
                        where: { MONo: nextMo },
                        include: [{
                            model: models.DO,
                            required: true
                        }],
                        raw: true
                    })

                    let getAllReceive = await models.ReceiveMaterialPart.findAll(
                        {
                            raw: true,
                            where: { MOId: getDOId["MOId"] },
                            order: [['createdAt', 'DESC']],
                        }
                    )

                    let latestReceive = []
                    let latestReceiveArr = []
                    for (let i = 0; i < getAllReceive.length; i++) {
                        if (!latestReceive.includes(getAllReceive[i]["ChildPartListId"])) {
                            latestReceive.push(getAllReceive[i]["ChildPartListId"])
                            latestReceiveArr.push(getAllReceive[i])
                        }
                    }

                    let sumTotalQty = 0
                    for (let i = 0; i < getNextMO.length; i++) {
                        sumTotalQty += getNextMO[i]['DOs.TotalQty']
                    }

                    let receiveNew = []
                    for (let i = 0; i < uniqueQtyArr.length; i++) {
                        let childList = []
                        for (let j = 0; j < getAllReceive.length; j++) {
                            if (uniqueQtyArr[i]["ChildPartListId"] === getAllReceive[j]["ChildPartListId"]) {
                                childList.push(getAllReceive[j])
                            }
                        }

                        let qtyLeftCheck = 0
                        for (let j = 0; j < childList.length; j++) {

                            if (qtyLeftCheck >= uniqueQtyArr[i]["QtyLeft"]) {
                                j = childList.length
                            } else {
                                // if(uniqueQtyArr[i]["QtyLeft"] < sumTotalQty){
                                qtyLeftCheck += childList[j]["Qty"]
                                receiveNew.push(Object.assign(childList[j], { NextMO: getNextMO[0]["MOId"] }, uniqueQtyArr[i]))
                                // }
                                // }else{
                                //     qtyLeftCheck += childList[j]["Qty"]
                                //     receiveNew.push(Object.assign(childList[j], { NextMO: getNextMO[0]["MOId"] }, uniqueQtyArr[i]))
                                //     j = childList.length
                                // }
                            }
                        }
                    }

                    let filterReceive = []
                    for (let i = 0; i < receiveNew.length; i++) {
                        filterReceive.push(
                            {
                                ChildPartListId: receiveNew[i]["ChildPartListId"],
                                ChildPartLot: receiveNew[i]["ChildPartLot"],
                                MOId: receiveNew[i]["NextMO"],
                                Qty: receiveNew[i]["Qty"]
                            })
                    }

                    let partNo = []
                    for (let i = 0; i < getNextMO.length; i++) {
                        let partListUsage = await models.PartListUsage.findAll({
                            where: { PartNoId: getNextMO[i]['DOs.PartNoId'] },
                            raw: true
                        })
                        partNo.push(partListUsage)
                    }
                    let partListUsage = []

                    for (let i = 0; i < partNo.length; i++) {
                        Object.assign(partListUsage, partNo[i])
                    }
                    let uniqueChild = []
                    for (let i = 0; i < partListUsage.length; i++) {
                        if (!uniqueChild.includes(partListUsage[i]["ChildPartListId"])) {
                            uniqueChild.push(partListUsage[i]["ChildPartListId"])
                        }
                    }
                    let result = []
                    for (let i = 0; i < uniqueChild.length; i++) {
                        for (let j = 0; j < filterReceive.length; j++) {
                            if (filterReceive[j]["ChildPartListId"] === uniqueChild[i]) {
                                result.push(filterReceive[j])
                            }
                        }
                    }

                    await models.ReceiveMaterialPart.bulkCreate(result)
                }
            })

    }
}



module.exports = router;