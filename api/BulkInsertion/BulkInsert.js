const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.post("/insertBulkMO", async (req, res) => {
 
    try{
        await models.MO.bulkCreate(req.body)
        res.send("OK")
    }catch(e){
        res.send(e)
    }
  

})

router.post("/insertBulkDO", async (req, res) => {

    for (let i = 0; i < req.body.length; i++) {
        const {
            DONo,
            Supp,
            SuppName,
            CMSP,
            Prcs,
            PartName,
            DueDate,
            DueQty,
            PrdtQty,
            Balance,
            Unit,
            ODDays,
            TotalCard,
            // TotalQty,
            DeliveryDueDate,
            CustomersPN,
            PartNoId,
            DOStatusId,
            MOId,
        } = req.body[i];

        let insertion = await models.DO.create({
            DONo: DONo,
            Supp: Supp,
            SuppName: SuppName,
            CMSP: Number(CMSP),
            Prcs: Number(Prcs),
            PartName: PartName,
            DueDate: tools.formatDate(DueDate),
            DueQty: Number(DueQty),
            PrdtQty: Number(PrdtQty),
            Balance: Number(Balance),
            Unit: Unit,
            ODDays: Number(ODDays),
            TotalCard: Number(TotalCard),
            TotalQty: Number(DueQty) * Number(TotalCard),
            DeliveryDueDate: tools.formatDate(DeliveryDueDate),
            CustomersPN: CustomersPN,
            PartNoId: Number(PartNoId),
            DOStatusId: Number(DOStatusId),
            MOId: MOId,
        });
    }

    res.json({
        api_result: "OK",
        message: "Success.",
    });

});


router.post("/insertBulkGroupPartList", async (req, res) => {
    await models.GroupPartList.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkPartList", async (req, res) => {
    try {
        for (let i = 0; i < req.body.length; i++) {
            let str = ""
            str = req.body[i].PartNo.replace(/(-DOM|-EXP)/g, "")
            let groupPartNoId = await models.GroupPartList.findOne({
                where: { PartNo: str },
                raw: true
            })
            req.body[i]["GroupPartNoId"] = groupPartNoId.GroupPartNoId
            await models.PartList.create(req.body[i])

        }
    } catch (e) {
        console.log(e)
    }

    res.send("OK")
})

router.post("/insertBulkMasterDOStatus", async (req, res) => {
    await models.MasterDOStatus.bulkCreate(req.body)
    res.send("OK")
})
router.post("/insertBulkPO", async (req, res) => {
    await models.PO.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkChildPartList", async (req, res) => {
    await models.ChildPartList.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkEmployee", async (req, res) => {
    await models.Employee.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkEmployeeRole", async (req, res) => {
    await models.EmployeeRole.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkMasterUsageStatus", async (req, res) => {
    await models.MasterUsageStatus.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkMachine", async (req, res) => {
    await models.Machine.bulkCreate(req.body)
    res.send("OK")
})
router.post("/insertBulkPartListUsage", async (req, res) => {
    await models.PartListUsage.bulkCreate(req.body)
    res.send("OK")
})

router.post("/insertBulkMasterPOStatus", async (req, res) => {
    await models.MasterPOStatus.bulkCreate(req.body)
    res.send("OK")
})

module.exports = router;