const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");
const e = require("express");

var state = {
    savedMOValues: [],
    MOValue: "",
    scanText: "",
    storedValidMat: [
        // { id: 1, values: [{ partNo: "testpart" }, { lotNo: "lot" }] },
        // { id: 2, values: [{ partNo: "testpart2" }, { lotNo: "lot" }] },
    ],
    row: 0,
};

var sumReceiveMaterialPartList = [];

router.get("/getMatchingIdState", async (req, res) => {
    res.send(state);
});

router.post("/checkMatchingId", async (req, res) => {
    state = req.body;

    let partNoPattern = /\w{0,6}-\D{3}|\w{0,6}-(.+)-\D{3}/g;
    const tag = req.body.scanText;
    const arr = [...tag.matchAll(partNoPattern)];

    let partNo = "";
    try {
        partNo = arr[0][0];
    } catch (error) { }

    let lotNo = "";
    let qty;
    let polotlabel = "";

    let qtyPatternUshinJP = /\d{20}\s\d+\s\d+/g;
    let check = [...tag.matchAll(qtyPatternUshinJP)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is now ushinJP
        let deleteLines = [...data.matchAll(/\d{12}\s/g)];
        let deletedLine = String(deleteLines[0][0]).trim();
        let subqty = deletedLine.substring(0, 7);
        let pureQty = subqty.replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        lotNo = poNo;
        polotlabel = lotNo;
    } catch (error) {
        //console.log(check);
    }
    let qtyWebEdiPattern = /(\w\d-)\d{17}|\w+\d\s\s\d{15}/g;
    check = [...tag.matchAll(qtyWebEdiPattern)];

    try {
        let data = String(check[0][0]).trim();
        //tag type is web edi
        let pureQty = data.substring(5, 12).replace(/^0+/, "");
        let poNoPattern = /[A-Za-z]\d{7}/g;
        let match = [...tag.matchAll(poNoPattern)];
        let poNo = match[0][0];
        qty = pureQty;
        polotlabel = poNo;
    } catch (error) {
    }
    let qtyPatternReprint = /(\w\d-)\d{21}|\s\s[A-Za-z]\d{21}/g;
    check = [...tag.matchAll(qtyPatternReprint)];
    try {
        let data = String(check[0][0]);
        //tag type is now reprint
        let subqty = data.substring(5, 12);
        console.log(subqty);
        let pureQty = subqty.replace(/^0+/, "");
        let lotNoPattern = /\s\s\s\w+\s/g;
        let match = [...tag.matchAll(lotNoPattern)];
        let lotNo = match[0][0].trim().substring(4, 15);
        qty = pureQty;
        polotlabel = lotNo;
    } catch (error) {
    }
    let qtyProductionInHousePattern = /\s{9}\w{18}/g;
    check = [...tag.matchAll(qtyProductionInHousePattern)];
    try {
        let data = String(check[0][0]).trim();
        let subqty = data.substring(3, 10).replace(/^0+/, "");
        let labelNo = data.substring(10, 17);
        qty = subqty;
        polotlabel = labelNo;
    } catch (error) { }

    // console.log(partNo + " " + lotNo + " " + qty + " " + polotlabel)
    try {
        let getChildPartListId = await models.ChildPartList.findAll({
            where: { ChildPartNo: partNo },
            raw: true
        });

        let childPartListId = getChildPartListId[0]["ChildPartListId"];

        let mo = await models.MO.findAll({
            where: { MONo: state.MOValue },
            raw: true
        });


        let getAllDO = await models.DO.findAll({
            where: { MOId: mo[0]["MOId"] },
            raw: true
        });


        let allDO = getAllDO;
        for (let i = 0; i < allDO.length; i++) {
            sumReceiveMaterialPartList.push({
                MOId: mo[0]["MOId"],
                ChildPartListId: childPartListId,
                ChildPartLot: polotlabel,
                row: state.row
            });
        }

    } catch (error) {
        state.scanText = "";
        res.send(state);
        return;
    }
    let qtyUshinNewPattern = /\s\d{15}\s+\w+\s/g
    check = [...tag.matchAll(qtyUshinNewPattern)];
    try {
        let data = String(check[0][0]).trim();
        let subqty = data.substring(8, 15).replace(/^0+/, "");
        let labelNo = data.substring(20, 28);
        qty = subqty;
        polotlabel = labelNo;
    } catch (e) {
    }

    state.row++;
    state.storedValidMat.push({
        // id: state.row,
        no: state.row,
        partNo: partNo,
        polotlabel: polotlabel,
        qty: qty

    });
    state.scanText = "";
    res.send(state);
});

router.post("/submitAllDataNew", async (req, res) => {

    try {

        let findMO = await models.MO.findOne({
            where: { MONo: state.MOValue },
            raw: true
        })

        let findDO = await models.DO.findAll({
            where: { MOId: findMO["MOId"] },
            raw: true
        })
        if (findDO.length === 0) {
            res.send("No Data")
            return
        }

        for (let i = 0; i < findDO.length; i++) {
            let getPartListUsage = await models.PartListUsage.findAll({
                where: { PartNoId: findDO[i]["PartNoId"] },
                raw: true
            })
            if (getPartListUsage.length === 0) {
                sumReceiveMaterialPartList = [];
                state = {
                    savedMOValues: state.savedMOValues,
                    MOValue: "",
                    scanText: "",
                    storedValidMat: [],
                    row: 0,
                    status: "PartListUsageNotFoundInDatabase",
                };
                // console.log(state)
                res.send(state);
                return
            }
        }

        let totalQtySum = 0
        for (let i = 0; i < findDO.length; i++) {
            totalQtySum += findDO[i]["TotalQty"]
        }
        // console.log(totalQtySum)

        let uniqueChildPartNo = []
        for (let i = 0; i < state.storedValidMat.length; i++) {
            if (!uniqueChildPartNo.includes(state.storedValidMat[i]["partNo"])) {
                uniqueChildPartNo.push(state.storedValidMat[i]["partNo"])
            }
        }
        let sumChildPartQty = []//sum of each child part
        for (let i = 0; i < uniqueChildPartNo.length; i++) {
            let sum = 0
            for (let j = 0; j < state.storedValidMat.length; j++) {
                if (state.storedValidMat[j]["partNo"] === uniqueChildPartNo[i]) {
                    sum += parseInt(state.storedValidMat[j]["qty"])
                }
            }
            sumChildPartQty.push({ partNo: uniqueChildPartNo[i], sumQty: sum })
        }

        let storedModelQtyLeft = []
        let storedReceiveMaterialPart = []

        for (let i = 0; i < state.storedValidMat.length; i++) {

            let findChildPart = await models.ChildPartList.findOne({
                where: { ChildPartNo: state.storedValidMat[i]["partNo"] },
                raw: true
            })

            if (state.storedValidMat[i]["polotlabel"] !== "" && state.storedValidMat[i]["qty"] !== "") {

                let getLatestQtyLeft = await models.QtyLeft.findOne({
                    where: { ChildPartListId: findChildPart["ChildPartListId"] },
                    order: [['createdAt', 'DESC']],
                    raw: true
                })

                await models.ReceiveMaterialPart.create({
                    ChildPartListId: findChildPart["ChildPartListId"],
                    ChildPartLot: state.storedValidMat[i]["polotlabel"],
                    MOId: findMO["MOId"]
                })
                storedReceiveMaterialPart.push({
                    ChildPartListId: findChildPart["ChildPartListId"],
                    ChildPartLot: state.storedValidMat[i]["polotlabel"],
                    MOId: findMO["MOId"],
                })
                if (getLatestQtyLeft === null) {
                    await models.QtyLeft.create({
                        QtyLeft: parseInt(state.storedValidMat[i]["qty"]),
                        ChildPartListId: findChildPart["ChildPartListId"]
                    })
                    storedModelQtyLeft.push({
                        QtyLeft: parseInt(state.storedValidMat[i]["qty"]),
                        ChildPartListId: findChildPart["ChildPartListId"],
                    })
                } else {
                    await models.QtyLeft.create({
                        QtyLeft: getLatestQtyLeft["QtyLeft"] + parseInt(state.storedValidMat[i]["qty"]),
                        ChildPartListId: findChildPart["ChildPartListId"]
                    })
                    storedModelQtyLeft.push({
                        QtyLeft: getLatestQtyLeft["QtyLeft"] + parseInt(state.storedValidMat[i]["qty"]),
                        ChildPartListId: findChildPart["ChildPartListId"],
                    })
                }

            }

        }
        //check if material is enough
        let getAllQtyLeft = await models.QtyLeft.findAll({ raw: true })
        let uniqueChildId = []
        let uniqueChildIdArr = []
        for (let i = 0; i < getAllQtyLeft.length; i++) {
            if (!uniqueChildId.includes(getAllQtyLeft[i]["ChildPartListId"])) {
                uniqueChildId.push(getAllQtyLeft[i]["ChildPartListId"])
                uniqueChildIdArr.push({ ChildPartListId: getAllQtyLeft[i]["ChildPartListId"] })
            }
        }
        let uniqueStoredModelQtyLeftChild = []
        let uniqueStoredModelQtyLeftChildArr = []
        for (let i = 0; i < storedModelQtyLeft.length; i++) {
            if (!uniqueStoredModelQtyLeftChild.includes(storedModelQtyLeft[i]["ChildPartListId"])) {
                uniqueStoredModelQtyLeftChild.push(storedModelQtyLeft[i]["ChildPartListId"])
                uniqueStoredModelQtyLeftChildArr.push(storedModelQtyLeft[i])
            }
        }

        let scannedChilds = []
        for (let i = 0; i < uniqueStoredModelQtyLeftChildArr.length; i++) {
            for (let j = 0; j < uniqueChildIdArr.length; j++) {
                if (uniqueStoredModelQtyLeftChildArr[i]["ChildPartListId"] === uniqueChildIdArr[j]["ChildPartListId"]) {
                    scannedChilds.push(uniqueChildIdArr[j])
                }
            }
        }


        let exceedFound = false
        for (let i = 0; i < scannedChilds.length; i++) {
            let exceedCount = 0;
            let qtyCount = 0
            for (let j = 0; j < getAllQtyLeft.length; j++) {

                if (scannedChilds[i]["ChildPartListId"] === getAllQtyLeft[j]["ChildPartListId"] && getAllQtyLeft[j]["QtyLeft"] > totalQtySum) {
                    exceedCount++
                }
                if (scannedChilds[i]["ChildPartListId"] === getAllQtyLeft[j]["ChildPartListId"]) {
                    qtyCount++
                }
                if (exceedCount > 1) {
                    exceedFound = true
                }
            }
            scannedChilds[i]["qtyCount"] = qtyCount
        }
        //this part is to check the case where qtyleft/receivemat has been inserted multiple times 
        //where qtyleft greater than mo totalqty if it is set boolean to true then error is shown (delete inserted list as well)
        let somethingWrong = false;
        // for (let i = 0; i < uniqueChildIdArr.length; i++) {
        //     if (uniqueChildIdArr[i]["qtyCount"] > 1) {
        //         for (let j = 0; j < state.storedValidMat.length; j++) {
        //             if (uniqueChildIdArr[i]["ChildPartListId"] === storedReceiveMaterialPart[j]["ChildPartListId"] && storedModelQtyLeft[j]["QtyLeft"] > totalQtySum) {
        //                 somethingWrong = true
        //                 j = state.storedValidMat.length
        //             }

        //         }
        //     }
        // }

        if (exceedFound || somethingWrong) {
            for (let i = 0; i < state.storedValidMat.length; i++) {

                let findReceive = await models.ReceiveMaterialPart.findOne({
                    where:
                    {
                        ChildPartListId: storedReceiveMaterialPart[i]["ChildPartListId"],
                        ChildPartLot: storedReceiveMaterialPart[i]["ChildPartLot"],
                        MOId: storedReceiveMaterialPart[i]["MOId"]
                    }
                })

                let findQty = await models.QtyLeft.findOne({
                    where:
                    {
                        QtyLeft: storedModelQtyLeft[i]["QtyLeft"],
                        ChildPartListId: storedModelQtyLeft[i]["ChildPartListId"]
                    }
                })
                findReceive.destroy()
                findQty.destroy()

            }
        }

        if (exceedFound || somethingWrong) {
            state = {
                savedMOValues: state.savedMOValues,
                MOValue: "",
                scanText: "",
                storedValidMat: [],
                row: 0,
                status: "ExceededMatFound",
            };
            sumReceiveMaterialPartList = [];
            res.send(state);
            return
        }
        for (let i = 0; i < findDO.length; i++) {
            let getPartListUsage = await models.PartListUsage.findAll({
                where: { PartNoId: findDO[i]["PartNoId"] },
                raw: true
            })

            let getReceieveMat = await models.ReceiveMaterialPart.findAll({
                where: { MOId: findMO["MOId"] },
                raw: true
            })
            // console.log(getPartListUsage.length)
            // console.log(getReceieveMat.length)

            if (getPartListUsage.length === getReceieveMat.length) {
                //find mo then prep for saving to log
                const index = state.savedMOValues.indexOf(state.MOValue)
                if (index > -1) {
                    state.savedMOValues.splice(index, 1)
                }
                //save mo to log
                await models.MO_Log.create({
                    MOId: findMO["MOId"],
                    MONo: findMO["MONo"]
                })
            }

        }

    } catch (e) {
        console.log(e)
        res.send("No Data")

        sumReceiveMaterialPartList = [];
        state = {
            savedMOValues: state.savedMOValues,
            MOValue: "",
            scanText: "",
            storedValidMat: [],
            row: 0,
            status: "Error",
        };
        // console.log(state)
        res.send(state);
        return
    }
    sumReceiveMaterialPartList = [];
    state = {
        savedMOValues: state.savedMOValues,
        MOValue: "",
        scanText: "",
        storedValidMat: [],
        row: 0,
        status: "OK",
    };
    // console.log(state)
    res.send(state);

})

router.get("/clearData", async (req, res) => {
    sumReceiveMaterialPartList = [];
    state = {
        savedMOValues: [],
        MOValue: "",
        scanText: "",
        storedValidMat: [],
        row: 0,
    };
    res.send(state);
});

router.post("/populateMOSplit", async (req, res) => {

    try {
        const splitArr = String(req.body.data).split(",")
        
        let moNotUsedList = []

        for (let i = 0; i < splitArr.length; i++) {
            let findUsedMO = await models.MO_Log.findOne({
                where: { MONo: splitArr[i] },
                raw: true
            })
            if (findUsedMO === null) {

                moNotUsedList.push(splitArr[i])

            }
        }

        state.savedMOValues = moNotUsedList

    } catch (e) {
        console.log(e)
    }
    res.send(state)

})

router.get("/deleteLastScannedMat", async (req, res) => {

    let getLastNo
    if (state.storedValidMat.length !== 0) {
        getLastNo = state.storedValidMat[state.storedValidMat.length - 1]["no"]
    }

    let listOfNoLastOne = []
    for (let i = 0; i < state.storedValidMat.length - 1; i++) {
        listOfNoLastOne.push(state.storedValidMat[i])
    }

    let newList = []
    for (let i = 0; i < sumReceiveMaterialPartList.length; i++) {
        if (Number(sumReceiveMaterialPartList[i]["row"] + 1) !== getLastNo) {
            newList.push(sumReceiveMaterialPartList[i])
        }
    }
    state.row = state.row - 1
    state.storedValidMat = listOfNoLastOne
    sumReceiveMaterialPartList = newList

    res.send(state)
})

async function GetUnfinishedMatData(totalDOQtyCount) {
    //check if materials are completed
    let getAllReceiveMat = await models.ReceiveMaterialPart.findAll({
        raw: true
    })
    let uniqueReceiveMatList = []

    for (let i = 0; i < getAllReceiveMat.length; i++) {
        if (!uniqueReceiveMatList.includes(getAllReceiveMat[i]["ChildPartListId"])) {
            uniqueReceiveMatList.push(getAllReceiveMat[i]["ChildPartListId"])
        }
    }
    let sumCheckChildPart = []
    for (let i = 0; i < uniqueReceiveMatList.length; i++) {
        let sumBoxCount = 0
        for (let j = i; j < getAllReceiveMat.length; j++) {
            if (uniqueReceiveMatList[i] === getAllReceiveMat[j]["ChildPartListId"]) {
                let getChildPart = await models.ChildPartList.findOne({
                    where: { ChildPartListId: getAllReceiveMat[j]["ChildPartListId"] },
                    raw: true
                })
                sumBoxCount += getChildPart["UsagePerBox"]
            }
        }
        sumCheckChildPart.push({ MaxQty: totalDOQtyCount, CurrentQty: sumBoxCount, ChildPartListId: getAllReceiveMat[i]["ChildPartListId"] })

    }
    let listOfUnfinishedMat = []
    for (let i = 0; i < sumCheckChildPart.length; i++) {
        if (sumCheckChildPart[i]["CurrentQty"] < sumCheckChildPart[i]["MaxQty"]) {
            listOfUnfinishedMat.push(sumCheckChildPart[i])
        }
    }
    console.log(listOfUnfinishedMat)
    return listOfUnfinishedMat

    // if (listOfUnfinishedMat.length !== 0) {//if mat still not finish don't lock mo

    // }
}

module.exports = router;