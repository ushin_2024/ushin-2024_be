const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.post("/checkDo", async (req, res) => {
    let DO_PO_TABLE = [];
    let MC_TABLE = [];
    let LOT_TABLE = [];

    try {
        if (req.body.data["scannedDo"].length === 0 || req.body.data["scannedDo"].length > 112) {
            res.send("No Data");
            return;
        }
        let DO_PATTERN = /N\w+-\w+-\w+|N\w+-\w+|N\w+/g;
        const tag = req.body.data["scannedDo"];
        const arr = [...tag.matchAll(DO_PATTERN)];
        let DO = arr[0][0]

        let PO_PATTERN = /D.*/g;
        const arrPO = [...tag.matchAll(PO_PATTERN)];
        let PO = arrPO[0][0]


        let DOPO
        try {
            DOPO = { DONo: DO, PONo: PO };
        } catch (e) {
            console.log(e);
            res.send({ DO_PO_TABLE: [], MC_TABLE: [], LOT_TABLE: [] });
            return
        }


        let getJoinedDOPO = await models.DO.findAll({
            // attributes: ['DONo'],
            where: { DONo: DOPO["DONo"] },
            include: [
                {
                    model: models.PO,
                    required: true,
                    // attributes: ['PONo'],
                    raw: true,
                },
                {
                    model: models.PartList,
                    required: true,
                },
                {
                    model: models.MO,
                    required: true,
                },
            ],
            raw: true,
        });

        let getAvailableChildParts = await models.DO.findAll({
            where: { DONo: DOPO["DONo"] },
            include: [
                {
                    model: models.PO,
                    required: true,
                    include: [{
                        model: models.ProductionData,
                        required: true,
                        include: [{
                            model: models.ChildPartList,
                            required: true,

                        }, {
                            model: models.Machine,
                            required: true
                        }]
                    }]
                }
            ],
            raw: true
        })

        let getDO = await models.DO.findOne({
            where: { DONo: DOPO["DONo"] },
            raw: true
        })
        let getPO = await models.PO.findAll({
            where: { DOId: getDO["DOId"] },
            raw: true
        })


        let partListUsageChild = []

        let sameCount = 0
        for (let i = 0; i < getAvailableChildParts.length; i++) {


            for (let j = 0; j < getPO.length; j++) {
                if (getPO[j]["POId"] == getAvailableChildParts[i]['POs.ProductionData.POId']) {
                    sameCount++
                    j = getPO.length
                }
            }
            if (sameCount == getPO.length) {
                partListUsageChild.push(getAvailableChildParts[i])
                sameCount = 0
            }
            // console.log(sameCount)
        }
        // console.log(getAvailableChildParts)


        // let findAllReceiveMat = await models.ReceiveMaterialPart.findAll({ raw: true })
        // partListUsageChild.sort(function (a, b) {
        //     return a['POs.ProductionData.ChildPartListId'] - b['POs.ProductionData.ChildPartListId'];
        // });

        // for (let i = 0; i < partListUsageChild.length; i++) {
        //     let getChild = await models.ChildPartList.findOne({
        //         where: { ChildPartNo: partListUsageChild[i]['POs.ProductionData.ChildPartList.ChildPartNo'] },
        //         raw: true
        //     })
        //     partListUsageChild[i]["SimiliarChild"] = getChild["ChildPartListId"]
        //     partListUsageChild[i]["Used"] = false
        // }

        // for (let i = 0; i < findAllReceiveMat.length; i++) {

        //     for (let j = 0; j < partListUsageChild.length; j++) {
        //         if (partListUsageChild[j]["SimiliarChild"] === findAllReceiveMat[i]["ChildPartListId"] && partListUsageChild[j]["Used"] === false) {
        //             findAllReceiveMat[i]["UsagePerBox"] = partListUsageChild[j]['POs.ProductionData.ChildPartList.UsagePerBox']
        //             findAllReceiveMat[i]["ChildPartNo"] = partListUsageChild[j]['POs.ProductionData.ChildPartList.ChildPartNo']
        //             findAllReceiveMat[i]["ChildPartName"] = partListUsageChild[j]['POs.ProductionData.ChildPartList.ChildPartName']
        //             findAllReceiveMat[i]["ProcessName"] = partListUsageChild[j]['POs.ProductionData.ChildPartList.ProcessName']
        //             let getMo = await models.MO.findOne({ where: { MOId: findAllReceiveMat[i]["MOId"] }, raw: true })
        //             findAllReceiveMat[i]["MONo"] = getMo["MONo"]
        //             partListUsageChild[j]["Used"] = true
        //             j = partListUsageChild.length
        //         }
        //     }
        // }
        // let filterData = []
        // for (let i = 0; i < findAllReceiveMat.length; i++) {
        //     try {
        //         if (findAllReceiveMat[i]["ChildPartNo"] !== undefined) {
        //             filterData.push(findAllReceiveMat[i])
        //         }
        //     } catch (e) {

        //     }
        // }

        let findAllReceive = await models.ReceiveMaterialPart.findAll({
            where: { MOId: getDO["MOId"] },
            include: [{
                model: models.MO,
                required: true
            }],
            raw: true
        })
        for (let i = 0; i < findAllReceive.length; i++) {
            let child = await models.ChildPartList.findOne({
                where: { ChildPartListId: findAllReceive[i]["ChildPartListId"] },
                raw: true
            })
            if (child !== null) {
                findAllReceive[i]["ChildPartList.ChildPartNo"] = child["ChildPartNo"]
                findAllReceive[i]["ChildPartList.ChildPartName"] = child["ChildPartName"]
                findAllReceive[i]["ChildPartList.ProcessName"] = child["ProcessName"]
                findAllReceive[i]["ChildPartList.UsagePerBox"] = child["UsagePerBox"]
                findAllReceive[i]["ChildPartList.MaterialYieldPercent"] = child["MaterialYieldPercent"]
            }


        }

        // console.log(findAllReceive)
        // console.log(partListUsageChild)

        let distinctMachine = []
        let distinctMachineArr = []//filtered dup machine
        for (let i = 0; i < getAvailableChildParts.length; i++) {
            // console.log()
            if (!distinctMachine.includes(getAvailableChildParts[i]['POs.PONo'] + "," + getAvailableChildParts[i]['POs.ProductionData.ChildPartListId'] + "," + getAvailableChildParts[i]['POs.ProductionData.ChildPartList.ProcessName'] + "," + getAvailableChildParts[i]['POs.ProductionData.Machine.MachineName'])) {
                distinctMachine.push(getAvailableChildParts[i]['POs.PONo'] + "," + getAvailableChildParts[i]['POs.ProductionData.ChildPartListId'] + "," + getAvailableChildParts[i]['POs.ProductionData.ChildPartList.ProcessName'] + "," + getAvailableChildParts[i]['POs.ProductionData.Machine.MachineName'])
                distinctMachineArr.push(getAvailableChildParts[i])
            }
        }

        // console.log(getAvailableChildParts)


        DO_PO_TABLE = getJoinedDOPO;
        MC_TABLE = findAllReceive;
        LOT_TABLE = distinctMachineArr;


        MC_TABLE.sort(function (a, b) {
            return a.ChildPartListId - b.ChildPartListId;
        });
        LOT_TABLE.sort(function (a, b) {
            return a['POs.ProductionData.ChildPartListId'] - b['POs.ProductionData.ChildPartListId']
        })
        // console.log(distinctMachineArr)
        console.log(MC_TABLE)

        let mc_table = []
        for (let i = 0; i < MC_TABLE.length; i++) {
            mc_table.push({
                'MO.MONo': MC_TABLE[i]['MO.MONo'],
                'POs.ProductionData.ChildPartList.ChildPartListId': MC_TABLE[i]['POs.ProductionData.ChildPartList.ChildPartListId'],
                'POs.ProductionData.ChildPartList.ChildPartNo': MC_TABLE[i]['POs.ProductionData.ChildPartList.ChildPartNo'],
                'ReceiveMaterialPart.ChildPartLot': MC_TABLE[i]['ReceiveMaterialPart.ChildPartLot'],
                'POs.ProductionData.ChildPartList.ProcessName': MC_TABLE[i]['POs.ProductionData.ChildPartList.ProcessName'],
                'POs.ProductionData.ChildPartList.UsagePerBox': MC_TABLE[i]['POs.ProductionData.ChildPartList.UsagePerBox']

            })
        }
        let machine_table = []
        for (let i = 0; i < LOT_TABLE.length; i++) {
            machine_table.push(
                {
                    'POs.PONo': LOT_TABLE[i]['POs.PONo'],
                    'POs.ProductionData.Machine.MachineName': LOT_TABLE[i]['POs.ProductionData.Machine.MachineName'],
                    'POs.ProductionData.ChildPartList.ChildPartListId': LOT_TABLE[i]['POs.ProductionData.ChildPartList.ChildPartListId'],
                    'POs.ProductionData.EmployeeId': LOT_TABLE[i]['POs.ProductionData.EmployeeId']
                }
            )
        }

        let combinedMachineLot = []
        // console.log(mc_table)
        // console.log(machine_table)
        // console.log(distinctMachineArr)
        for (let i = 0; i < mc_table.length; i++) {
            for (let j = 0; j < machine_table.length; j++) {
                if (mc_table[i]['POs.ProductionData.ChildPartList.ChildPartListId'] === machine_table[j]['POs.ProductionData.ChildPartList.ChildPartListId']) {
                    combinedMachineLot.push(
                        {
                            MONo: mc_table[i]['MO.MONo'],
                            ChildPartListId: machine_table[j]['POs.ProductionData.ChildPartList.ChildPartListId'],
                            ChildPartNo: mc_table[i]['POs.ProductionData.ChildPartList.ChildPartNo'],
                            ChildPartLot: mc_table[i]['ReceiveMaterialPart.ChildPartLot'],
                            ProcessName: mc_table[i]['POs.ProductionData.ChildPartList.ProcessName'],
                            UsagePerBox: mc_table[i]['POs.ProductionData.ChildPartList.UsagePerBox'],
                            PONo: machine_table[j]['POs.PONo'],
                            MachineName: machine_table[j]['POs.ProductionData.Machine.MachineName'],
                            EmployeeId: machine_table[j]['POs.ProductionData.EmployeeId'],
                        }
                    )
                }
            }
        }

        for (let i = 0; i < DO_PO_TABLE.length; i++) {

            let arr = []
            for (let j = 0; j < combinedMachineLot.length; j++) {
                if (combinedMachineLot[j]["PONo"] === DO_PO_TABLE[i]['POs.PONo'])
                    arr.push(combinedMachineLot[j])
            }
            DO_PO_TABLE[i]["CombinedTable"] = arr
        }

        //   console.log(DO_PO_TABLE)

        // console.log(LOT_TABLE)
    } catch (error) {
        console.log(error);
        res.send({ DO_PO_TABLE: [], MC_TABLE: [], LOT_TABLE: [] });
        return;
    }
    res.send({
        DO_PO_TABLE: DO_PO_TABLE,
        MC_TABLE: MC_TABLE,
        LOT_TABLE: LOT_TABLE,
    });
});


module.exports = router;
