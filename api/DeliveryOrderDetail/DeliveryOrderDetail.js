const { json } = require("body-parser");
const express = require("express");
const router = express.Router();
// const dbOperation = require("../dbFiles/dbOperation");
const models = require("../../models");
const moment = require("moment");
const tools = require("../../tools/tools");
const { where } = require("sequelize");
const Sequelize = require("sequelize");
const { sequelize } = require("../../models");

router.get("/get_DO", async (req, res) => {
    let DOdata = await models.DO.findAll({
        raw: true,
        include: [
            {
                model: models.PartList,
                required: true,
                raw: true,
            },
            {
                model: models.MasterDOStatus,
                required: true,
                raw: true,
            },
        ],
    });
    console.log(DOdata);
    res.send(DOdata);
});

module.exports = router;