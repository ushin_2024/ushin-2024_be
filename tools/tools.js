const formatDate = (content) => {
  const dateArr = String(content).split("/");
  let date = dateArr[2] + "-" + dateArr[1] + "-" + dateArr[0];
  let d = new Date(date);

  d = d.toISOString().split("T").join(" ").split("Z").join("");
  
  return d;
};




module.exports = {
  formatDate,
};
