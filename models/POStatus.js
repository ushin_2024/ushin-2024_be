module.exports = (sequelize, DataTypes) => {
  const POStatus = sequelize.define(
    "POStatus",
    {
      POStatusId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      MasterPOStatusId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      POId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      MfgDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return POStatus;
};
