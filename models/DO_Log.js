module.exports = (sequelize, DataTypes) => {
  const DO_Log = sequelize.define(
    "DO_Log",
    {
      DO_LogId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      DOId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      DONo: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      Supp: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      SuppName: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      CMSP: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      Prcs: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      PartName: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      DueDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      DueQty: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      PrdtQty: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      Balance: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      Unit: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      ODDays: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      TotalCard: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      TotalQty: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      DeliveryDueDate: {
        type: DataTypes.DATE,
        allowNull: true,
      },
      CustomersPN: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      PartNoId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      DOStatusId: {
        //fk in masterdostatus
        type: DataTypes.INTEGER,
        allowNull: false,
        // required: true,
      },
      MOId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      }
    },
    {
      freezeTableName: true,

    }
  );
  return DO_Log;
};
