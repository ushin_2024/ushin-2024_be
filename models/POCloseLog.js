module.exports = (sequelize, DataTypes) => {
    const POCloseLog = sequelize.define(
        "POCloseLog",
        {
            POCloseLogId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            PONo: {
                type: DataTypes.STRING(100),
                allowNull: true,
            },
            MachineName: {
                type: DataTypes.STRING(100),
                allowNull: true,
            }
        },
        {
            freezeTableName: true,
        }
    );
    return POCloseLog;
};
