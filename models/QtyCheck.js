module.exports = (sequelize, DataTypes) => {
    const QtyCheck = sequelize.define(
        "QtyCheck",
        {
            QtyCheckId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            QtyLeft: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            ChildPartListId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            DOId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            Order: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            ChildPartNo: {
              type: DataTypes.STRING(100),
              allowNull: true,
            },
            ProcessName: {
              type: DataTypes.STRING(100),
              allowNull: true,
            },
            
        },
        {
            freezeTableName: true,
        }
    );
    return QtyCheck;
};
