module.exports = (sequelize, DataTypes) => {
    const EmployeeLineMatch = sequelize.define(
      "EmployeeLineMatch",
      {
        EmployeeLineMatchId: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
            allowNull: false,
        },
        EmployeeId: {
          type: DataTypes.STRING(100),
          allowNull: true,
        },
        Machine: {
            type: DataTypes.STRING(100),
            allowNull: true,
        },
      },
      {
        freezeTableName: true,
  
      }
    );
    return EmployeeLineMatch;
  };
  