module.exports = (sequelize, DataTypes) => {
  const ProductionData = sequelize.define(
    "ProductionData",
    {
      ProductionDataId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      POId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      ChildPartListId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      EmployeeId: {
        type: DataTypes.STRING(100),
        allowNull: false,
      }
    },
    {
      freezeTableName: true,

    }
  );
  return ProductionData;
};
