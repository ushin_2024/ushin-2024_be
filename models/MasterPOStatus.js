module.exports = (sequelize, DataTypes) => {
  const MasterPOStatus = sequelize.define(
    "MasterPOStatus",
    {
      MasterPOStatusId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      POStatus: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return MasterPOStatus;
};
