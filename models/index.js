"use strict";

const fs = require("fs");
const path = require("path");
const Sequelize = require("sequelize");
const process = require("process");
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || "development";
const config = require(__dirname + "/../config/config.json")[env];
const db = {};

let sequelize;
if (config.use_env_variable) {
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(
    config.database,
    config.username,
    config.password,
    config
  );
}

fs.readdirSync(__dirname)
  .filter((file) => {
    return (
      file.indexOf(".") !== 0 &&
      file !== basename &&
      file.slice(-3) === ".js" &&
      file.indexOf(".test.js") === -1
    );
  })
  .forEach((file) => {
    const model = require(path.join(__dirname, file))(
      sequelize,
      Sequelize.DataTypes
    );
    db[model.name] = model;
  });

Object.keys(db).forEach((modelName) => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

sequelize
  .authenticate()
  .then(() => {
    console.log("Connection has been established successfully.");
  })
  .catch((err) => {
    console.log("Unable to connect to the database: ", err);
  });

db.sequelize = sequelize;
db.Sequelize = Sequelize;

//DO Associations
db.DO.belongsTo(db.MO, {//DOMO
  foreignKey: "MOId",
})

db.DO.belongsTo(db.MasterDOStatus, {//DOMasterDOStatus
  foreignKey: "DOStatusId"
})

db.DO.belongsTo(db.PartList, {//DOPartList
  foreignKey: "PartNoId"
})
db.PartList.hasMany(db.DO, {//DOPartList
  foreignKey: "PartNoId"
})
db.MO.hasMany(db.DO, {//DOMO
  foreignKey: "MOId",
});
db.MasterDOStatus.hasMany(db.DO, {//DOMasterDOStatus
  foreignKey: "DOStatusId"
})

//PO Associations
db.PO.belongsTo(db.DO, {//PODO
  foreignKey: "DOId"
})
db.DO.hasMany(db.PO, {//PODO
  foreignKey: "DOId"
})

//POStatus Associations
db.POStatus.belongsTo(db.MasterPOStatus, {//POStatusMasterPOStatus
  foreignKey: "MasterPOStatusId"
})
db.MasterPOStatus.hasMany(db.POStatus, {//POStatusMasterPOStatus
  foreignKey: "MasterPOStatusId"
})

db.POStatus.belongsTo(db.PO, {
  foreignKey: "POId"
})

db.PO.hasMany(db.POStatus, {
  foreignKey: "POId"
})

//Production Data Associations
db.ProductionData.belongsTo(db.Machine, {//ProductionDataMachine
  foreignKey: "MachineId"
})
db.Machine.hasMany(db.ProductionData, {//ProductionDataMachine
  foreignKey: "MachineId"
})
db.ProductionData.belongsTo(db.Employee, {//ProductionDataEmployee
  foreignKey: "EmployeeId"
})

db.Employee.hasMany(db.ProductionData, {//ProductionDataEmployee
  foreignKey: "EmployeeId"
})
db.ProductionData.belongsTo(db.ChildPartList, {//ProductionDataChildPartList
  foreignKey: "ChildPartListId"
})
db.ChildPartList.hasMany(db.ProductionData, {//ProductionDataChildPartList
  foreignKey: "ChildPartListId"
})

db.ProductionData.belongsTo(db.PO, {//ProductionDataPO
  foreignKey: "POId"
})
db.PO.hasMany(db.ProductionData, {//ProductionDataPO
  foreignKey: "POId"
})

//Employee Associations
db.Employee.belongsTo(db.EmployeeRole, {//EmployeeEmployeeRole
  foreignKey: "RoleId"
})
db.EmployeeRole.hasOne(db.Employee, {//EmployeeEmployeeRole
  foreignKey: "RoleId"
})

//ChildPartList Associations
// db.ChildPartList.belongsTo(db.GroupPartList, {//ChildPartListGroupPartList
//   foreignKey: "GroupPartNoId"
// })
// db.GroupPartList.hasOne(db.ChildPartList, {//ChildPartListGroupPartList
//   foreignKey: "GroupPartNoId"
// })

//PartListUsage Associations
db.PartListUsage.belongsTo(db.ChildPartList, {//PartListUsageChildPartList
  foreignKey: "ChildPartListId"
})
db.ChildPartList.hasMany(db.PartListUsage, {//PartListUsageChildPartList
  foreignKey: "ChildPartListId"
})

db.PartListUsage.belongsTo(db.PartList, {//PartListUsagePartList
  foreignKey: "PartNoId"
})
db.PartList.hasMany(db.PartListUsage, {//PartListUsagePartList
  foreignKey: "PartNoId"
})

//PartList Associations
// db.PartList.belongsTo(db.GroupPartList, {//PartListGroupPartList
//   foreignKey: "GroupPartNoId"
// })
// db.GroupPartList.hasOne(db.PartList, {//PartListGroupPartList
//   foreignKey: "GroupPartNoId"
// })


//QtyLeft Associations
// db.QtyLeft.belongsTo(db.MO, {//QtyLeftMO
//   foreignKey: "MOId"
// })
// db.MO.hasMany(db.QtyLeft, {//QtyLeftMO
//   foreignKey: "MOId"
// })
db.QtyLeft.belongsTo(db.ChildPartList, {//QtyLeftChildPartList
  foreignKey: "ChildPartListId"
})
db.ChildPartList.hasMany(db.QtyLeft, {//QtyLeftChildPartList
  foreignKey: "ChildPartListId"
})

//TargetUsage Associations
db.TargetUsage.belongsTo(db.MO, {//TargetUsageMO
  foreignKey: "MOId"
})
db.MO.hasOne(db.TargetUsage, {//TargetUsageMO
  foreignKey: "MOId"
})

db.TargetUsage.belongsTo(db.PartListUsage, {//TargetUsagePartListUsage
  foreignKey: "PartListUsageId"
})
db.PartListUsage.hasOne(db.TargetUsage, {//TargetUsagePartListUsage
  foreignKey: "PartListUsageId"
})

db.TargetUsage.belongsTo(db.ActualUsage, {//TargetUsageActualUsage
  foreignKey: "ActualUsageId"
})
db.ActualUsage.hasOne(db.TargetUsage, {//TargetUsageActualUsage
  foreignKey: "ActualUsageId"
})

//ActualUsage Associations
db.ActualUsage.belongsTo(db.MasterUsageStatus, {//ActualUsageMasterUsageStatus
  foreignKey: "UsageStatusId"
})
db.MasterUsageStatus.hasOne(db.ActualUsage, {//ActualUsageMasterUsageStatus
  foreignKey: "UsageStatusId"
})

//ReceiveMaterialPart Associations
db.ReceiveMaterialPart.belongsTo(db.MO, {//ReceiveMaterialPartMO
  foreignKey: "MOId"
})
db.MO.hasMany(db.ReceiveMaterialPart, {//ReceiveMaterialPartMO
  foreignKey: "MOId"
})
db.ReceiveMaterialPart.belongsTo(db.ChildPartList, {//ReceiveMaterialPartChildPartList
  foreignKey: "ChildPartListId"
})
db.ChildPartList.hasMany(db.ReceiveMaterialPart, {//ReceiveMaterialPartChildPartList
  foreignKey: "ChildPartListId"
})

//QtyPartListRealUsage Associations
db.PartListRealUsage.belongsTo(db.ChildPartList, {
  foreignKey: "ChildPartListId"
})
db.ChildPartList.hasMany(db.PartListRealUsage, {
  foreignKey: "ChildPartListId"
})

//QtyCheck Associations
db.QtyCheck.belongsTo(db.ChildPartList, {
  foreignKey: "ChildPartListId"
})
db.ChildPartList.hasMany(db.QtyCheck, {
  foreignKey: "ChildPartListId"
})

db.QtyCheck.belongsTo(db.DO, {
  foreignKey: "DOId"
})

db.DO.hasMany(db.QtyCheck, {
  foreignKey: "DOId"
})

module.exports = db;
