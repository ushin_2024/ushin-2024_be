module.exports = (sequelize, DataTypes) => {
  const Employee = sequelize.define(
    "Employee",
    {
      EmployeeId: {
        type: DataTypes.STRING(100),
        primaryKey: true,
        allowNull: false,
      },
      EmployeeName: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      RoleId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return Employee;
};
