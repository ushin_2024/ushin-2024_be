module.exports = (sequelize, DataTypes) => {
  const PartListUsage = sequelize.define(
    "PartListUsage",
    {
      PartListUsageId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      PartNoId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      ChildPartListId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      Usage: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return PartListUsage;
};
