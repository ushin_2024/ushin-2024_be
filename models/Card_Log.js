module.exports = (sequelize, DataTypes) => {
    const Card_Log = sequelize.define(
        "Card_Log",
        {
            Card_LogId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                autoIncrement: true,
                allowNull: false,
            },
            DOId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            CardNo: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            POId: {
                type: DataTypes.INTEGER,
                allowNull: false
            },
            MachineId: {
                type: DataTypes.INTEGER,
                allowNull: false
            },

        },
        {
            freezeTableName: true,

        }
    );
    return Card_Log;
};
