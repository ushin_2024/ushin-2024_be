module.exports = (sequelize, DataTypes) => {
  const TargetUsage = sequelize.define(
    "TargetUsage",
    {
      TargetUsageId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        autoIncrement: true,
        allowNull: false,
      },
      MOId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      PartListUsageId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      TargetUsage: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      TargetPerBox: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      ActualUsageId:{
        type: DataTypes.INTEGER,
        allowNull: false,
      }
    },
    {
      freezeTableName: true,

    }
  );
  return TargetUsage;
};
