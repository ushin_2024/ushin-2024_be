module.exports = (sequelize, DataTypes) => {
  const MasterUsageStatus = sequelize.define("MasterUsageStatus", {
    UsageStatusId: {
      type: DataTypes.INTEGER,
      primaryKey: true,
      allowNull: false,
      autoIncrement: true,
    },
    UsageStatus: {
      type: DataTypes.STRING(100),
      allowNull: true,
    },
  },{
    freezeTableName: true,

  });
  return MasterUsageStatus;
};
