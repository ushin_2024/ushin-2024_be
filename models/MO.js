module.exports = (sequelize, DataTypes) => {
  const MO = sequelize.define(
    "MO",
    {
      MOId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      MONo: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,
    }
  );
  return MO;
};
