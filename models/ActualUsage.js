module.exports = (sequelize, DataTypes) => {
  const ActualUsage = sequelize.define(
    "ActualUsage",
    {
      ActualUsageId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      ActualUsage: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      ActualPerBox: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      UsageStatusId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return ActualUsage;
};
