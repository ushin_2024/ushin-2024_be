module.exports = (sequelize, DataTypes) => {
    const StockLostReason = sequelize.define(
        "StockLostReason",
        {
            StockLostReasonId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            ChildPartNo: {
                type: DataTypes.STRING(100),
                allowNull: true,
            },
            ChildPartLot: {
                type: DataTypes.STRING(100),
                allowNull: true,
            },
            Reason: {
                type: DataTypes.STRING(100),
                allowNull: true,
            },
        },
        {
            freezeTableName: true,
        }
    );
    return StockLostReason;
};
