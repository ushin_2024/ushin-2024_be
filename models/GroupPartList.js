module.exports = (sequelize, DataTypes) => {
  const GroupPartList = sequelize.define(
    "GroupPartList",
    {
      GroupPartNoId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      PartNo: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return GroupPartList;
};
