module.exports = (sequelize, DataTypes) => {
  const ChildPartList = sequelize.define(
    "ChildPartList",
    {
      ChildPartListId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      ChildPartNo: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      ChildPartName: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      ProcessName: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      UsagePerBox: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      MaterialYieldPercent: {
        type: DataTypes.INTEGER,
        allowNull: false,
      }, 

    },
    {
      freezeTableName: true,

    }
  );
  return ChildPartList
};
