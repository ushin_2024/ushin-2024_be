module.exports = (sequelize, DataTypes) => {
  const PartList = sequelize.define(
    "PartList",
    {
      PartNoId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      PartNo: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return PartList;
};
