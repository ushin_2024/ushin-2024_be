module.exports = (sequelize, DataTypes) => {
  const ReceiveMaterialPart = sequelize.define(
    "ReceiveMaterialPart",
    {
      ReceiveMaterialPartId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      ChildPartListId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      ChildPartLot: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      MOId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      Qty: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return ReceiveMaterialPart;
};
