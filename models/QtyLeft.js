module.exports = (sequelize, DataTypes) => {
    const QtyLeft = sequelize.define(
        "QtyLeft",
        {
            QtyLeftId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            QtyLeft: {
                type: DataTypes.INTEGER,
                allowNull: false,
            },
            ChildPartListId: {
                type: DataTypes.INTEGER,
                allowNull: false,
            }
        },
        {
            freezeTableName: true,
        }
    );
    return QtyLeft;
};
