module.exports = (sequelize, DataTypes) => {
  const MasterDOStatus = sequelize.define(
    "MasterDOStatus",
    {
      DOStatusId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      DOStatus: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return MasterDOStatus;
};
