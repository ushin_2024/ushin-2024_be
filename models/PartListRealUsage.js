module.exports = (sequelize, DataTypes) => {
    const PartListRealUsage = sequelize.define(
      "PartListRealUsage",
      {
        PartListRealUsageId: {
          type: DataTypes.INTEGER,
          primaryKey: true,
          allowNull: false,
          autoIncrement: true,
        },
        PartNoId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        ChildPartListId: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        Usage: {
          type: DataTypes.INTEGER,
          allowNull: false,
        },
        DOId: {
            type: DataTypes.INTEGER,
            allowNull: false,
        },
        Order: {
          type: DataTypes.INTEGER,
          allowNull: false,
      }
      },
      {
        freezeTableName: true,
  
      }
    );
    return PartListRealUsage;
  };
  