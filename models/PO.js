module.exports = (sequelize, DataTypes) => {
  const PO = sequelize.define(
    "PO",
    {
      POId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      PONo: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      CardNo: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
      DOId: {
        type: DataTypes.INTEGER,
        allowNull: false,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return PO;
};
