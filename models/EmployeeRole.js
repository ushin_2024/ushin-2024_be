module.exports = (sequelize, DataTypes) => {
  const EmployeeRole = sequelize.define(
    "EmployeeRole",
    {
      RoleId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      RoleName: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return EmployeeRole;
};
