module.exports = (sequelize, DataTypes) => {
    const MO_Log = sequelize.define(
        "MO_Log",
        {
            MOLogId: {
                type: DataTypes.INTEGER,
                primaryKey: true,
                allowNull: false,
                autoIncrement: true,
            },
            MOId: {
                type: DataTypes.INTEGER,
                allowNull: true,
            },
            MONo: {
                type: DataTypes.STRING(100),
                allowNull: true,
            }
        },
        {
            freezeTableName: true,
        }
    );
    return MO_Log;
};
