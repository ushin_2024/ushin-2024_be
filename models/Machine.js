const { database } = require("../dbFiles/dbConfig");

module.exports = (sequelize, DataTypes) => {
  const Machine = sequelize.define(
    "Machine",
    {
      MachineId: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
      },
      Machine: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      MachineType: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
      MachineName: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },      
      MachineNumber: {
        type: DataTypes.STRING(100),
        allowNull: true,
      },
    },
    {
      freezeTableName: true,

    }
  );
  return Machine;
};
