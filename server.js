const express = require("express");
const app = express();
const PORT = 2005;
const cors = require("cors");

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: false }));

app.use(cors());
// app.use("/api", require("./api/webapi"));
const getMaster = require("./api/GetMaster/GetMaster")
const insertMaster = require("./api/InsertMaster/InsertMaster")
const debug = require("./api/debug")
const bulkInsert = require("./api/BulkInsertion/BulkInsert")
const dashboard = require("./api/Dashboard/Dashboard")
const deliveryOrderDetail = require("./api/DeliveryOrderDetail/DeliveryOrderDetail")
const receiveMaterial = require("./api/ReceiveMaterial/ReceiveMaterial")
const shopper = require("./api/Shopper/Shopper")
const traceability = require("./api/Tracebility/Tracebility")
const wipOutput = require("./api/WIPOutput/WIPOutput")
const stockLost = require("./api/StockLost/StockLost")
const databaseReceive = require("./api/DatabaseReceive/DatabaseReceive")
const shopperStore = require("./api/ShopperStore/ShopperStore")
const employeeLineMatch = require("./api/EmployeeLineMatch/EmployeeLineMatch")

app.use("/api",//link API
  [
    getMaster,
    insertMaster,
    debug,
    bulkInsert,
    dashboard,
    deliveryOrderDetail,
    receiveMaterial,
    shopper,
    traceability,
    wipOutput,
    stockLost,
    databaseReceive,
    shopperStore,
    employeeLineMatch
  ]
)

const db = require("./models");

db.sequelize.sync().then((req) => {
  app.listen(PORT, () => {
    console.log(`Application is running on port ${PORT}`);
  });
});
